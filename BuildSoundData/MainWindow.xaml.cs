﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Numerics;
using Arction.Wpf.SemibindableCharting;
using System.IO;
using MathNet.Numerics.IntegralTransforms;

namespace BuildSoundData
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UserControl UserControlGraph;
        WriteAndRead WriteAndRead;

        public BufferedWaveProvider bwp;
        public WaveFileWriter writer;
        static double Fs = 44100;               // Частота дискретизвции 




        private int _samplingFrequency = 44100;
        private int m_iFFTWindowLength;
        private int m_iHighFreq;

        public MainWindow()
        {
            m_iFFTWindowLength = 1024 * 2;
            m_iHighFreq = 22050;
            _samplingFrequency = 44100;

            InitializeComponent();
            InitConnectionAor();
        }

        static double T = 1.0 / Fs;             // Шаг дискретизации
        static double Fn = Fs / 2;              // Частота Найквиста
        static int N = (int)Fs / 5;
        Complex max = 2400;                         //для корректировки уровня спектра
        Complex[] sig1;
        int iAverage = 0;//Длина сигнала (точек)    
        int countafterdot = 0;
        double volumeProcents;
        bool DotInFrq = false;
        bool zapolneno = false;
        public float step_frq_MHz = 10f / 1000000f;
        string path;
        string outputFilename;

        double qwe = 0;                             //точка сигнала на графике
        double sig_sum = 0;                         //среднее за 5 отсчетов
        double K = N / 4;
        int kol_previshenij_dlia_zapisi = 10;       //колво превышений за 1 набор БПФ
        int kol_previshenij_dlia_zapisi_time_count = 5; //колво превышений подряд
        int kol_previshenij_dlia_zapisi_time_count_i = 0;
        int kol_buf_ostanovka_zapisi = 300;                  //задержка конца записи
        int PorogZapisi = -60;

        double[] SignalX;           //для точек графика БПФ 
        double[] SignalY;           //для точек графика БПФ 
        double[] PorogObnar;           //для точек порог обнаружения
        double[] SignalYAverage;           //для точек графика БПФ        
        double[] SignalYResized;
        int iNumDataMx = 0;
        int iNumDataSigma = 0;
        int count_baund = 0;
        public int delitel;
        public int resolutionX = 1000;
        public int resolutionY = 1000;
        int kol_previshenij = 0;

        double timeStepMs = 20;
        double m_dTimeRangeLengthSec;
        double freqMin = 0;
        double freqMax = Fs / 2;
        double dFFTtimeWinOffset = (double)(512 / 2) / Fs;
        double timeRangeLengthSec = 5;
        double m_dStepTime;
        double m_dCurrentTime = 0;
        int m_iSizeTimeSlots;
        int m_iSizeResolution;
        double[][] m_aFastData;
        public Int64 Friquency;
        

        private void InitConnectionAor()
        {
            WriteAndRead = new WriteAndRead();
            WriteAndRead.ConnectAudio();

            sig1 = new Complex[44100];
            SignalX = new double[2205];
            SignalY = new double[2205];
            PorogObnar = new double[2205];
            SignalYAverage = new double[2205];


            InitializeIntensity();
            Graph_AOR.cbWrite.IsChecked = true;
            intensityGraph1Resize();
        }

        private void InitializeIntensity()
        {
            int resolution = m_iFFTWindowLength;
            if (m_iHighFreq <= _samplingFrequency / 2)
                resolution = (int)Math.Round((double)m_iHighFreq / (double)(_samplingFrequency / 2) * m_iFFTWindowLength);
            Graph_AOR.spectrogram.Clear();
            Graph_AOR.intensityGraph.BeginUpdate();
            Graph_AOR.spectrogram.Clear();

            double dAxisTimeScaleMin = timeStepMs / 1000.0 - timeRangeLengthSec;
            double dAxisTimeScaleMax = 0;
            m_dStepTime = timeStepMs / 1000.0;
            m_dTimeRangeLengthSec = timeRangeLengthSec;

            var view = Graph_AOR.intensityGraph.ViewXY;
            view.XAxes[0].SetRange(freqMin, freqMax);
            view.YAxes[0].SetRange(dAxisTimeScaleMin, dAxisTimeScaleMax);

            m_iSizeTimeSlots = (int)Math.Round(timeRangeLengthSec / (timeStepMs / 1000.0));
            m_iSizeResolution = resolution;

            m_aFastData = new double[m_iSizeTimeSlots][];
            for (int iTimeSlot = 0; iTimeSlot < m_iSizeTimeSlots; iTimeSlot++)
            {
                m_aFastData[iTimeSlot] = new double[m_iSizeResolution];
            }

            Graph_AOR.spectrogram.SetValuesData(m_aFastData, IntensityGridValuesDataOrder.RowsColumns);
            //Graph_AOR.spectrogram.Data = null;
            //Graph_AOR.spectrogram.ContourLineType = ContourLineTypeXY.None;
            //Graph_AOR.spectrogram.WireframeType = SurfaceWireframeType.None;
            Graph_AOR.spectrogram.PixelRendering = true;
            //Graph_AOR.spectrogram.MouseInteraction = false;

            m_dCurrentTime = dAxisTimeScaleMax;

            Graph_AOR.spectrogram.SetRangesXY(view.XAxes[0].Minimum, view.XAxes[0].Maximum,
                view.YAxes[0].Minimum, view.YAxes[0].Maximum);
            Graph_AOR.intensityGraph.EndUpdate();
        }


        private void FileDirectoryWrite()
        {
            path = String.Format(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                "\\wav\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00"));

            if (!Directory.Exists(path))
            {
                DirectoryInfo directory = Directory.CreateDirectory(path);
            }

            outputFilename = path + "\\" + Friquency.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" +
                DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00") + ".wav";
        }

        private void Graph_AOR_Loaded(object sender, RoutedEventArgs e)
        {
            UserControlGraph = new UserControl();
            Graph_AOR.InitializeComponent();
            LoadGraphControl();
        }

        private void LoadGraphControl()    // Подписался на события из Графконтрола
        {
            Graph_AOR.OnCbWriteCheccked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbWriteCheccked_OnCbWriteCheccked);
            Graph_AOR.OnCbWriteUnCheccked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbWriteUnCheccked_OnCbWriteUnCheccked);
            Graph_AOR.OnCbPlayCloseChecked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbPlayCloseChecked_OnCbPlayCloseChecked);
            Graph_AOR.OnCbPlayCloseUnChecked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(OnCbPlayCloseUnChecked_OnCbPlayCloseUnChecked);
            Graph_AOR.OnCbAverageChecked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbAverageChecked_OnCbAverageChecked);
            Graph_AOR.OnCbAverageUnChecced += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbAverageUnChecced_OnCbAverageUnChecced);
            Graph_AOR.OnCbAutoWriteChecked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbAutoWriteChecked_OnCbAutoWriteChecked);
            Graph_AOR.OnCbAutoWriteUnChecked += new WpfControlLibraryBut.UserControlGraph.RoutedEventHandler(CbAutoWriteUnChecked_OnCbAutoWriteUnChecked);
        }

        public void CbWriteCheccked_OnCbWriteCheccked()
        {

            if (Graph_AOR.cbWrite.IsChecked == true)
            {
                FileDirectoryWrite();
                writer = new WaveFileWriter(outputFilename, WriteAndRead.waveIn.WaveFormat);
                Graph_AOR.RecZapis.Visibility = Visibility.Visible;
            }
        }

        private void CbWriteUnCheccked_OnCbWriteUnCheccked()
        {
            if (writer != null)
            {
                writer.Close();
                writer = null;
                Graph_AOR.RecZapis.Visibility = Visibility.Collapsed;
            }
        }

        private void CbPlayCloseChecked_OnCbPlayCloseChecked()
        {
            try
            {
                if (Graph_AOR.cbPlayClose.IsChecked == true)
                {
                    WriteAndRead.waveIn.DataAvailable += waveIn_DataAvailable;
                    Graph_AOR.cbWrite.IsEnabled = true;
                }
            }
            catch { }
        }

        private void OnCbPlayCloseUnChecked_OnCbPlayCloseUnChecked()
        {
            try
            {
                if (Graph_AOR.cbPlayClose.IsChecked == false)
                {
                    WriteAndRead.waveIn.DataAvailable -= waveIn_DataAvailable;
                    Graph_AOR.cbWrite.IsEnabled = false;
                }
            }
            catch { }
        }

        private void CbAverageChecked_OnCbAverageChecked()
        {

        }

        private void CbAverageUnChecced_OnCbAverageUnChecced()
        {

        }

        private void CbAutoWriteChecked_OnCbAutoWriteChecked()
        {

        }

        private void CbAutoWriteUnChecked_OnCbAutoWriteUnChecked()
        {
            Graph_AOR.cbWrite.IsChecked = false;
        }

        public void waveIn_DataAvailable(object sender, WaveInEventArgs e)
        {

            if (writer != null) writer.WriteData(e.Buffer, 0, e.BytesRecorded);  //запись в файл
            byte[] buffer = e.Buffer;
            int bytesRecorded = e.BytesRecorded;
            Complex[] sig = new Complex[bytesRecorded / 2];

            for (int i = 0, j = 0; i < e.BytesRecorded; i += 2, j++)
            {
                short sample = (short)((buffer[i + 1] << 8) | buffer[i + 0]);
                sig[j] = sample / 32768f;
            }

            Fourier.Inverse(sig, FourierOptions.Matlab);
            foreach (Complex c in sig)
            {
                if (max.Magnitude < c.Magnitude)
                {
                    max = c;
                }
            }

            for (int j = 0; j < bytesRecorded / 2; j++)
            {
                sig1[j + iAverage * bytesRecorded] = sig[j];
            }
            iAverage++;
            if (iAverage == 5) { iAverage = 0; }

            DrawGraphs(sig, max);

        }

        private void DrawGraphs(Complex[] sig, Complex max1)
        {
            try
            {
                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke((delegate ()
                    {
                        DrawGraphs(sig, max1);
                    }));
                    return;
                }

                SignalYResized_Clear();
                qwe = 0;                             //точка сигнала на графике
                sig_sum = 0;                         //среднее за 5 отсчетов
                //double K = sig.Length / 2;
                for (int i = 0; i < K; i++)
                {
                    qwe = (Complex.Abs(sig[i]) * 1000000 * 100) / (N * 115 * max1.Magnitude);
                    qwe = 20 * Math.Log10(qwe);
                    try { SignalY[i] = qwe; }
                    catch { }

                    PorogObnar[i] = PorogZapisi;
                    if (iNumDataMx == count_baund) { iNumDataMx = 0; }
                    if (iNumDataSigma == count_baund) { iNumDataSigma = 0; }
                }

                for (int j = 0; j < sig.Length; j++)
                {
                    sig1[j + iAverage * sig.Length] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }
                for (int i = 0; i < K; i++)
                {
                    sig_sum = ((
                        Complex.Abs(sig1[i + sig.Length * 0]) +
                        Complex.Abs(sig1[i + sig.Length * 1]) +
                        Complex.Abs(sig1[i + sig.Length * 2]) +
                        Complex.Abs(sig1[i + sig.Length * 3]) +
                        Complex.Abs(sig1[i + sig.Length * 4])) * 1000000 * 100) / (5 * N * 115 * max1.Magnitude);
                    sig_sum = 20 * Math.Log10(sig_sum);
                    SignalYAverage[i] = sig_sum;
                    //SignalYAverage[i] = Complex.FromPolarCoordinates(Math.Sqrt(K * K + sig_sum * sig_sum), Math.Atan2(sig_sum, K));
                    //if (i > 10 && i < 1000 && sig_sum > int.Parse(textBox3.Text)) { kol_previshenij++; }    // условие записи по порогу


                    if (i > 10 && i < 1000 && sig_sum > PorogZapisi) { kol_previshenij++; }    // условие записи по порогу
                }
                int ii = 0;

                for (int i = 0; i < K; i++)
                {
                    ii = i / delitel;
                    if (ii < SignalYResized.Length)
                        if (SignalYResized[ii] < SignalY[i]) SignalYResized[ii] = SignalY[i];

                }


                UpdateGraphs();
            }
            catch { }
        }

        void UpdateGraphs()
        {

            if (SignalY != null)
            {
                var _view = Graph_AOR.FrqGraph.ViewXY;
                Graph_AOR.FrqGraph.BeginUpdate();


                _view.SampleDataSeries[0].SamplingFrequency = K / Fn;
                _view.SampleDataSeries[0].InvalidateData();
                _view.SampleDataSeries[0].SamplesDouble = SignalY;
                if (Graph_AOR.cbAverage.IsChecked == true)
                {
                    _view.SampleDataSeries[1].SamplingFrequency = 1 / Fn * K;
                    _view.SampleDataSeries[1].InvalidateData();
                    _view.SampleDataSeries[1].SamplesDouble = SignalYAverage;
                    _view.SampleDataSeries[1].LineVisible = true;
                }
                else
                {
                    _view.SampleDataSeries[1].LineVisible = false;
                }
                _view.SampleDataSeries[2].SamplingFrequency = 1 / Fn * K;
                _view.SampleDataSeries[2].SamplesDouble = PorogObnar;


                Graph_AOR.FrqGraph.EndUpdate();
                AutoWriteWave();

                ////////////////////////////////////////////////////////////ПОСТРОЕНИЕ СПЕКТРОГРАММЫ

                Graph_AOR.intensityGraph.BeginUpdate();

                var view = Graph_AOR.intensityGraph.ViewXY;

                int rowCount = 6;

                for (int row = 0; row < rowCount; row++)
                {
                    double[] yValues = SignalY;
                    //Only accept resolution count of data points 
                    Array.Resize(ref yValues, m_iSizeResolution);

                    //move the old time slots one step earlier
                    for (int iTimeSlot = 1; iTimeSlot < m_iSizeTimeSlots; iTimeSlot++)
                    {
                        m_aFastData[iTimeSlot - 1] = m_aFastData[iTimeSlot]; //change the reference  
                    }
                    m_aFastData[m_iSizeTimeSlots - 1] = yValues;
                }

                Graph_AOR.spectrogram.InvalidateValuesDataOnly();

                double dCurrentTimeMin = m_dCurrentTime - m_dTimeRangeLengthSec;
                double dTotalTimeShift = m_dStepTime * rowCount;

                view.YAxes[0].SetRange(dCurrentTimeMin + dTotalTimeShift, dCurrentTimeMin + dTotalTimeShift + m_dTimeRangeLengthSec);

                Graph_AOR.spectrogram.SetRangesXY(view.XAxes[0].Minimum, view.XAxes[0].Maximum,
                    view.YAxes[0].Minimum, view.YAxes[0].Maximum);

                m_dCurrentTime += dTotalTimeShift;

                Graph_AOR.intensityGraph.EndUpdate();

            }

        }

        void SignalYResized_Clear()
        {
            try
            {
                for (int j = 0; j < SignalYResized.Length; j++)
                {
                    SignalYResized[j] = -150;
                }
            }
            catch (Exception) { }
        }

        private void intensityGraph1Resize()
        {
            resolutionX = (int)Graph_AOR.intensityGraph.Width;
            resolutionY = (int)Graph_AOR.intensityGraph.Height;
            //delitel = 5;
            delitel = 2205 / resolutionX;
            if (delitel < 2) delitel = 2;
            if (delitel > 10) delitel = 10;
            //intensity = new double[2205 / delitel, WaterflowTime];
            SignalYResized = new double[2205 / delitel];
            SignalYResized_Clear();
            //Intensity_Clear();

        }

        private void DisposeFFTMonitors()
        {
            Graph_AOR.FrqGraph.ViewXY.AreaSeries[0].Clear();
        }

        private void AutoWriteWave()
        {
            if (Graph_AOR.cbAutoWrite.IsChecked == true)
            {
                if (kol_previshenij_dlia_zapisi_time_count_i < kol_buf_ostanovka_zapisi)
                {
                    if ((kol_previshenij > kol_previshenij_dlia_zapisi))
                    {
                        kol_previshenij_dlia_zapisi_time_count_i++;
                        if (kol_previshenij_dlia_zapisi_time_count_i > kol_previshenij_dlia_zapisi_time_count)
                        {
                            if (Graph_AOR.cbWrite.IsChecked == false)
                            {
                                Graph_AOR.cbWrite.IsChecked = true;                        //zapisyvat
                            }
                        }
                        else
                        {
                            Graph_AOR.cbWrite.IsChecked = false;
                        }
                    }

                    else
                    {
                        Graph_AOR.cbWrite.IsChecked = false;
                    }
                }
                else
                {
                    kol_previshenij_dlia_zapisi_time_count_i = 0;
                    Graph_AOR.cbWrite.IsChecked = false;
                }
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (writer != null)
            {
                writer.Close();
                writer = null;
                Graph_AOR.RecZapis.Visibility = Visibility.Collapsed;
            }
        }
    }
}
