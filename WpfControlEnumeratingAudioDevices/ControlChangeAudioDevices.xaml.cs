﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfControlEnumeratingAudioDevices
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class ControlChangeAudioDevices : UserControl
    {
        private WaveIn waveIn;

        private static double audioValueMax = 0;
        private static double audioValueLast = 0;
        private static int audioCount = 0;

        public ControlChangeAudioDevices()
        {
            InitializeComponent();

            waveIn = new WaveIn();
            cbDevices.Items.Clear();
            FindAudioDevices();
        }

        private void FindAudioDevices() 
        {
            if (WaveIn.DeviceCount != 0) 
            {
                var devicescount = WaveIn.DeviceCount;
                for (int i = 0; i < devicescount; i++)
                {
                    WaveInCapabilities inCapabilities = WaveIn.GetCapabilities(i);
                    cbDevices.Items.Add(inCapabilities.ProductName);
                }
                cbDevices.SelectedIndex = 0;
            }
            else { cbDevices.Items.Add("ERROR: no recording devices available"); }
        }

        private void cbDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            waveIn?.StopRecording();
            waveIn = new WaveIn();
            
            
            waveIn.DeviceNumber = cbDevices.SelectedIndex;
            waveIn.WaveFormat = new WaveFormat(44100, 16, 1);
            waveIn.DataAvailable += WaveIn_DataAvailable;

            waveIn.RecordingStopped += waveIn_RecordingStopped;
            waveIn.StartRecording();
            TaskManager();
        }

        private void WaveIn_DataAvailable(object sender, WaveInEventArgs e) 
        {
            float max = 0;

            // interpret as 16 bit audio
            for (int index = 0; index < e.BytesRecorded; index += 2)
            {
                short sample = (short)((e.Buffer[index + 1] << 8) |
                                        e.Buffer[index + 0]);
                var sample32 = sample / 32768f; // to floating point
                if (sample32 < 0) sample32 = -sample32; // absolute value 
                if (sample32 > max) max = sample32; // is this the max value?
            }

            // calculate what fraction this peak is of previous peaks
            if (max > audioValueMax)
            {
                audioValueMax = (double)max;
            }
            audioValueLast = max;
            audioCount += 1;
        }

        private void waveIn_RecordingStopped(object sender, EventArgs e)
        {
            waveIn?.Dispose();
            waveIn = null;
        }

        private bool isLvlRequest = true;
        CancellationTokenSource cts = new CancellationTokenSource();
        private int _CancellationTokenDelay = 100;
        public int CancellationTokenDelay
        {
            get { return _CancellationTokenDelay; }
            set
            {
                if (_CancellationTokenDelay != value)
                {
                    _CancellationTokenDelay = value;
                }
            }
        }
        private int _RequestLvlTimer = 10;
        public int RequestLvlTimer
        {
            get { return _RequestLvlTimer; }
            set
            {
                if (_RequestLvlTimer != value)
                {
                    _RequestLvlTimer = value;
                }
            }
        }

        private void TaskManager()
        {
            Task.Run(() => UpdateLvlRequest());
        }

        private async void UpdateLvlRequest()
        {
            try
            {
                while (isLvlRequest)
                {
                    cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;

                    DispatchIfNecessary(() => 
                    {
                        if (audioValueMax != 0)
                        {
                            double frac = audioValueLast / audioValueMax;
                            lvlMicrophone.Value = frac;
                        }
                        
                    });
                    
                    //lvlMicrophone.Width = (int)(frac * lvlMicrophone.Width);
                    //lvlMicrophone.Text = string.Format("{0:00.00}% peak [count: {0}]", frac * 100.0, audioCount);

                    var T = await Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; });
                    await Task.Delay(_RequestLvlTimer);
                }
            }
            catch { }
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
