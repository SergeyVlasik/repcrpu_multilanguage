﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using NAudio.Wave;

namespace AORConnection
{
    class WriteAndRead
    {
        public WaveIn waveIn;        
        
        private static int Fs = 44100;               // Частота дискретизвции 
        
        public void ConnectAudio()
        {
            //int devcount = WaveIn.DeviceCount;
            //Console.Out.WriteLine("Device Count: {0}.", devcount);
            waveIn = new WaveIn();

            try
            {
                waveIn.DeviceNumber = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            try
            {
                waveIn.WaveFormat = new WaveFormat((int)Fs, 1);                
                waveIn.StartRecording();
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }

        public void stopRecordButton_Click(object sender, EventArgs e)
        {
            waveIn.StopRecording();
        }
    }
}
