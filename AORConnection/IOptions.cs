﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AORConnection
{
    interface IOptions
    {
        Int64 Frequency { get; set; }
    }
}
