﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AORConnection
{
    public partial class MainWindow
    {
        void InitScanEvents() 
        {
            ControlAr6000Table.OnRequestReceiverStatus += ControlAr6000Table_OnRequestReceiverStatus;
            ControlAR6000.OnSendScanModeStatus += ControlAR6000_OnSendScanModeStatus;
            ControlAR6000.OnSendReceiveModeStatus += ControlAR6000_OnSendReceiveModeStatus;  //Отслеживание Режима Приёмника
        }

        private void ControlAr6000Table_OnRequestReceiverStatus(bool value)
        {
            ControlAR6000.GetScanStatus();
        }

        private void ControlAR6000_OnSendReceiveModeStatus(object sender, string data)
        {
            DispatchIfNecessary(() =>
            {
                listLogs.Items.Add(data);
                listLogs.Items.MoveCurrentToLast();
                listLogs.ScrollIntoView(listLogs.Items.CurrentItem);
            });
        }

        private void ControlAR6000_OnSendScanModeStatus(object sender, string data, Int64 frequency)
        {
            //Send Current Address Channel Memory
            ControlAr6000Table.DecodedScanMode(data);
            ControlAr6000Table.ScanFrequency = frequency;

            DispatchIfNecessary(() => 
            {
                listLogs.Items.Add(data);
                listLogs.Items.MoveCurrentToFirst();
                listLogs.ScrollIntoView(listLogs.Items.CurrentItem);
            });
        }

        private void ControlAr6000Table_OnRequestScanStatus(object sender, EventArgs e)
        {
            
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }
    }
}
