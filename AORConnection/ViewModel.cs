﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AORConnection
{
    public class ViewModel : INotifyPropertyChanged
    {
        public ViewModel()
        {
            Customers = new ObservableCollection<Customer>();
        }

        public void AddCustomer(string customerName)
        {
            Customer customer = new Customer { Name = customerName };
            this.Customers.Add(customer);
        }

        private ObservableCollection<Customer> customers;
        public ObservableCollection<Customer> Customers
        {
            get { return customers; }
            set
            {
                customers = value;
                OnPropertyChanged("Customers");
            }
        }

        public DateTime Dates { get; set; }
        public string Notes { get; set; }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler(this, new PropertyChangedEventArgs(name));
        }
    }
    public partial class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }

    }
}
