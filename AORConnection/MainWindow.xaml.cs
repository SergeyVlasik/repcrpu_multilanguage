﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.IO.Ports;
using NAudio.Wave;
using System.IO;
using MathNet.Numerics.IntegralTransforms;
using System.Numerics;
using NAudio.CoreAudioApi;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SignalProcessing;
using Arction.Wpf.SemibindableCharting.SeriesXY;

using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using WPFControlConnection;
using System.Globalization;
using AR6000;

namespace AORConnection
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UserControl UserControl;
        UserControl UserControlGraph;
        UserControl ReceiverTableControl;
        WpfControlLibraryBut.ReceiverTableControl ReceiverTableControl1;

        WriteAndRead WriteAndRead;
        ConnectionControl ConnectionControl;
        public BufferedWaveProvider bwp;
        public WaveFileWriter writer;
        static double Fs = 44100;               // Частота дискретизвции 
        

        public MainWindow()
        {  
            InitializeComponent();
            UserControl = new UserControl();
            InitConnectionAor();
            //InitLanguage();
            List<string> cmblang = new List<string>() {"ENG", "RU", "AZ" };
            OnCBChangedLanguage.ItemsSource = cmblang;
            ControlAR6000.OnDataToSave += ControlAR6000_OnDataToSave;
            ControlAR6000.OnLvlSignal += ControlAR6000_OnLvlSignal;

            ControlAR6000.OnScanInformation += ControlAR6000_OnScanInformation;
            ControlAr6000Table.OnSendScanDelayTime += ControlAr6000Table_OnSendScanDelayTime;
            ControlAr6000Table.OnSendScanPauseTime += ControlAr6000Table_OnSendScanPauseTime; 

            Connection_AOR.OnLvlSignal += Connection_AOR_OnLvlSignal;

            InitScanEvents();

            ControlAr6000Table.DeviceNumber = 1;

            
        }


        public Ar6000Manager AR6000DLL;
        public ClassLibrary_ARONE ClassLibrary_ARONE_DLL;

        
        string portAdress = "COM3";

        string dB = "дБ";
        string Hz = "Гц";
        string kHz = "кГц";
        string MHz = " МГц";
        string FrqToAOR = "";
        string dBm = "дБм";
        string UsilVKL = "Усил. ВКЛ";
        string vykl = "Выкл";
        string vkl = "Вкл";
        string Attt = "Атт";
        string Avto = "Авто";







        #region Переменные
        //------------------------------Свойства полей из AR6000DLL
        #endregion
        public Int64 Frequency
        {
            get;
            set;
        }
        public int SignalLevel
        {
            get { return AR6000DLL.AORstruct.SignalLevel; }
            set { AR6000DLL.AORstruct.SignalLevel = value; }
        }
        public int Mode
        {
            get { return AR6000DLL.AORstruct.Mode; }
            set { AR6000DLL.AORstruct.Mode = value; }
        }
        public int Bandwidth
        {
            get { return AR6000DLL.AORstruct.Bandwidth; }
            set { AR6000DLL.AORstruct.Bandwidth = value; }
        }
        public int Attenuator
        {
            get { return AR6000DLL.AORstruct.Attenuator; }
            set { }
        }
        public int HighPassFilter
        {
            get { return AR6000DLL.AORstruct.HighPassFilter; }
            set { }
        }
        public int LowPassFilter
        {
            get { return AR6000DLL.AORstruct.LowPassFilter; }
            set { }
        }
        public int AGC
        {
            get { return AR6000DLL.AORstruct.AGC; }
            set { }
        }
        public int NoiseSquelch
        {
            get { return AR6000DLL.AORstruct.NoiseSquelch; }
            set { }
        }
        public int NoiseSquelchOnOff
        {
            get { return AR6000DLL.AORstruct.NoiseSquelchOnOff; }
            set { }
        }
        public int AudioGain
        {
            get { return AR6000DLL.AORstruct.AudioGain; }
            set { }
        }
        //------------------------------Свойства полей из ArOne
        //public Int64 FrequencyOne
        //{
        //    get => ClassLibrary_ARONE.ARONE.frequency; 
        //    set { }
        //}
        
        public int IzOpen { get; set; }
        public bool isOpen { get; set; }

        //WpfControlLibraryBut.UserControl1 userControl1;
        //WpfControlLibraryBut.UserControlGraph userControlGraph;
        // ИНИЦИАЛИЗАЦИЯ
        public void InitConnectionAor()
        {
            WriteAndRead = new WriteAndRead();
            AR6000DLL = new Ar6000Manager();
            ConnectionControl = new ConnectionControl();
            Connection_AOR.OnComIsOpen += Connection_AOR_OnComIsOpen;
            ControlAR6000.OnComIsOpen += ControlAR6000_OnComIsOpen;
            //DataContext = new ViewModel();
            //Connection_AOR.OnChangedLanguageReceiver += Connection_AOR_OnChangedLanguageReceiver;
            //userControl1 = new WpfControlLibraryBut.UserControl1();
            //userControlGraph = new WpfControlLibraryBut.UserControlGraph();
            //userControlGraph.OnTakeFrq += UserControlGraph_OnTakeFrq;
            //userControlGraph.TakeFrq(userControl1.Frequency);

            //AR6000DLL.OnDecodedMemoryChannelDataRead += new AR6000Manager.ByteEventHandler(DecodedMemoryChannelDataRead);

            /*
            ConnectorVisible();
            SQL_Visible();
            
            try
            {                
                switch (vybor_DLL)
                {
                    case 0:
                        OpenARDll();
                        break;
                    case 1:
                        OpenArOneDll();
                        break;
                }
                
                Thread.Sleep(300);
                Ask_Arone();
                
                //НАСТРОЙКА ТАЙМЕРА
                DispatcherTimer dispatcherTimer1 = new DispatcherTimer();
                dispatcherTimer1.Tick += new EventHandler(DispatcherTimer1_Tick);
                dispatcherTimer1.Interval = new TimeSpan(0, 0, 1 / 10);
                dispatcherTimer1.Start();
                
            }
            catch { }*/
        }



        private void ControlAR6000_OnComIsOpen(object sender, WpfControlLibraryAR6000.ComIsOpenEventArgs e)
        {
            isOpen = e.IsOpen;
        }

        private void Connection_AOR_OnChangedLanguageReceiver(object sender, string ChangedLanguage)
        {
           
        }

        public void InitLanguage()
        {
            WpfControlLibraryBut.UserControl1.LanguageChanged += UserControl1_LanguageChanged;
            //CultureInfo currLang = WpfControlLibraryBut.UserControl1.Language;
            menuLang.Items.Clear();
            foreach (var lang in WpfControlLibraryBut.UserControl1.Languages)
            {
                MenuItem menuLanguage = new MenuItem
                {
                    Header = lang.DisplayName,
                    Tag = lang,
                    //IsChecked = lang.Equals(currLang)
                };
                menuLanguage.Click += ChangeLanguageClick;
                menuLang.Items.Add(menuLanguage);
            }
        }

        private void ChangeLanguageClick(Object sender, EventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            if (mi != null)
            {
                CultureInfo lang = mi.Tag as CultureInfo;
                if (lang != null)
                {
                   // WpfControlLibraryBut.UserControl1.Language = lang;
                }
            }

        }

        private void UserControl1_LanguageChanged(object sender, EventArgs e)
        {
            //CultureInfo currLang = WpfControlLibraryBut.UserControl1.Language;

            //Отмечаем нужный пункт смены языка как выбранный язык
            foreach (MenuItem i in menuLang.Items)
            {
                CultureInfo ci = i.Tag as CultureInfo;
                //i.IsChecked = ci != null && ci.Equals(currLang);
            }
        }

        private void Connection_AOR_OnComIsOpen(object sender, WpfControlLibraryBut.ComIsOpenEventArgs e)
        {
            IzOpen = e.IsOpen;
        }

        private void UserControlGraph_OnTakeFrq()
        {
            //userControlGraph.freq = userControl1.Frequency;
        }

        private void DecodedMemoryChannelDataRead()
        {
            //AR6000DLL.MemoryChannelDataRead();
            //Thread.Sleep(5);
            //ReceiverControl.textBoxData.Dispatcher.Invoke((delegate ()
            //{
            //    ReceiverControl.textBoxData.Text = AR6000Manager.AOR.MemoryData.ToString();
            //}));
        }


        private void ConnectBut_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ConnectBut_ShowDisconnect()     // Отображение подключения кнопки (загорается красным)
        {
            ConnecctBut.ShowDisconnect();

            ConnecctBut.ShowRead();
            ConnecctBut.ShowWrite();
        }
        private void ConnectBut_ShowConnect()     // Отображение подключения кнопки (загорается зелёным)
        {
            ConnecctBut.ShowConnect();

            ConnecctBut.ShowRead();
            ConnecctBut.ShowWrite();
        }

        
        // ПОДКЛЮЧЕНИЕ К ПРИЁМНИКУ И ВКЛЮЧЕНИЕ ИНТЕРФЕЙСА
        private void ConnectBut_ButServerClick(object sender, RoutedEventArgs e)
        {
            Connection_AOR.IdArOne = 0;
            ReceiverControl.DllArOne = 0;
            ReceiverControl.DeviceNumber = 0;


            Connection_AOR.ConnectionToMain();
            Connection_AOR.ComIsOpen();
            if (IzOpen == 1)
            {
                ConnectBut_ShowConnect();
            }
            else { ConnectBut_ShowDisconnect(); }
            
            //ReceiverControl.LoadTableArone();
             
            /*try
            {                
                if (SerialPort.GetPortNames().Any(x => x == portAdress))    //Проверка, существует ли COM-порт
                {
                    switch (vybor_DLL)
                    {
                        case 0:
                            if (AR6000DLL.port == null)             //Проверка, открыт ли COM-порт
                            {
                                AR6000DLL.OpenPort(portAdress);
                                ARPort_Null();
                            }
                            else
                            {
                                ARPort_IsOpen();  
                            }
                            break;
                        case 1:
                            if (ClassLibrary_ARONE_DLL.port == null)
                            {
                                ClassLibrary_ARONE_DLL.OpenPort(portAdress);
                                AROnePort_Null();
                            }
                            else
                            {
                                AROnePort_IsOpen();
                            }
                            break;
                    }                    
                }
                else { MessageBox.Show("COM-порт отсутствует"); }
            }
            catch { }*/
        }

        private void ARPort_Null()
        {
            if (Ar6000Manager.port.IsOpen)    //Проверка, открылся ли COM-порт
            {
                Connection_AOR.Ask_Arone();
                if (Frequency == 0)     //Проверка, прошёл ли опрос приёмника успешно
                {
                    Connection_AOR.BorderBrush = Brushes.Red;                    
                    ConnectBut_ShowDisconnect();
                }
                else
                {
                    Connection_AOR.BorderBrush = Brushes.Gray;                    
                    ConnectBut_ShowConnect();
                }
            }
            else
            {
                ConnectBut_ShowDisconnect();
            }
        }
        private void ARPort_IsOpen()
        {
            if (Ar6000Manager.port.IsOpen)   //Если порт открыт, то закрываем
            {
                AR6000DLL.ClosePort();
                Connection_AOR.BorderBrush = Brushes.Gray;
                ConnectBut_ShowDisconnect();
            }
            else
            {
                AR6000DLL.OpenPort(portAdress);
                Connection_AOR.Ask_Arone();
                if (Frequency == 0)
                {
                    Connection_AOR.BorderBrush = Brushes.Red;
                    ConnectBut_ShowDisconnect();
                }
                else
                {
                    Connection_AOR.BorderBrush = Brushes.Gray;                    
                    ConnectBut_ShowConnect();
                }
            }
        }
        /*
        private void AROnePort_Null()
        {
            if (ClassLibrary_ARONE.port.IsOpen)
            {
                Connection_AOR.Ask_Arone();
                if (ClassLibrary_ARONE.ARONE.frequency == 0)     //Проверка, прошёл ли опрос приёмника успешно
                {
                    //DisconnectPort();
                    ConnectBut_ShowDisconnect();
                }
                else
                {
                    ConnectBut_ShowConnect();
                }
            }
            else
            {
                ConnectBut_ShowDisconnect();
            }
        }

        */
        private void AROnePort_IsOpen()
        {
            //if (ClassLibrary_ARONE.port.IsOpen)
            //{
            //    ClassLibrary_ARONE_DLL.ClosePort();
            //    ConnectBut_ShowDisconnect();
            //}
            //else
            //{
            //    ClassLibrary_ARONE_DLL.OpenPort(portAdress, baudRate);
            //    Connection_AOR.Ask_Arone();
            //    if (FrequencyOne == 0)
            //    {
            //        //DisconnectPort();
            //        ConnectBut_ShowDisconnect();
            //    }
            //    else
            //    {
            //        ConnectBut_ShowConnect();
            //    }
            //}
        }
        
        // ПОДПИСКА НА СОБЫТИЯ КОНТРОЛА 
        private void UserControl1_Loaded(object sender, RoutedEventArgs e)
        {
            //UserControl = new UserControl();
            //Connection_AOR.InitializeComponent();
            //Zapolnenie_ComboBox();
            //LoadUserControl1();
        }
       
        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            //grid1.Focus();
            Keyboard.ClearFocus();

        }

        //------------------------------------------------------- ПОДКЛЮЧЕНИЕ К ПОТОКУ ЗВУКОВОЙ КАРТЫ С ПРИЁМНИКА----------------------------------------------------
        //private void Graph_AOR_Loaded(object sender, RoutedEventArgs e)
        //{
        //    UserControlGraph = new UserControl();
        //    Graph_AOR.InitializeComponent();            
        //}
        
        private void Window_Closed(object sender, EventArgs e)
        {
            if (writer != null)
            {                
                writer.Close();
                writer = null;
                //Graph_AOR.RecZapis.Visibility = Visibility.Collapsed;
            }
        }

        private void ReceiverControl_Loaded(object sender, RoutedEventArgs e)
        {
            //ReceiverTableControl = new WpfControlLibraryBut.ReceiverTableControl();
            ReceiverTableControl = new UserControl();
            
            ReceiverControl.InitializeComponent();
        }

        private void OnCBChangedLanguage_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (OnCBChangedLanguage.SelectedIndex) 
            {
                case 0:
                    string langE = "Eng";
                    Connection_AOR.Language(langE);
                    ReceiverControl.Language(langE);
                    ControlAR6000.Language(langE);
                    ControlAr6000Table.Language(langE);
                    break;
                case 1:
                    string langR = "Rus";
                    Connection_AOR.Language(langR);
                    ReceiverControl.Language(langR);
                    ControlAR6000.Language(langR);
                    ControlAr6000Table.Language(langR);
                    break;
                case 2:
                    string langA = "Azr";
                    Connection_AOR.Language(langA);
                    ReceiverControl.Language(langA);
                    ControlAR6000.Language(langA);
                    ControlAr6000Table.Language(langA);
                    break;
            }
        }

        private void ConnectAR6000_Loaded(object sender, RoutedEventArgs e)
        {

        }

        private void ConnectAR6000_ButServerClick(object sender, RoutedEventArgs e)
        {
            ControlAR6000.ConnectionToMain();
            ControlAR6000.ComIsOpen();
            if (isOpen)
            {
                AR6000_ShowConnect();
            }
            else { AR6000_ShowDisconnect(); }
        }

        private void AR6000_ShowConnect() 
        {
            ConnecctAR6000.ShowConnect();

            ConnecctAR6000.ShowRead();
            ConnecctAR6000.ShowWrite();
        }

        private void AR6000_ShowDisconnect() 
        {
            ConnecctAR6000.ShowDisconnect();

            ConnecctAR6000.ShowRead();
            ConnecctAR6000.ShowWrite();
        }

        private void LogsClear_Click(object sender, RoutedEventArgs e)
        {
            listLogs.Items.Clear();
        }
    }     

}
 

