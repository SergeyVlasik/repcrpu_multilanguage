﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AORConnection
{
    public partial class MainWindow
    {
        private void ControlAR6000_OnDataToSave(object sender, WpfControlLibraryAR6000.DataToSaveEventArgs e)
        {
            ControlAr6000Table.Frequency = e.Frequency;
            ControlAr6000Table.Modulation = e.Modulation;
            ControlAr6000Table.Attenuator = e.Attenuator;
            ControlAr6000Table.Bandwidth = e.Bandwidth;
        }

        private void ControlAR6000_OnLvlSignal(object sender, WpfControlLibraryAR6000.LevelOfSignalsEventArgs e)
        {
            ControlAr6000Table.SignalLvl = e.SignalLvl;
            ControlAr6000Table.SqlLvl = e.SqlLvl;
        }

        private void ControlAR6000_OnScanInformation(object sender, WpfControlLibraryAR6000.ScanInformationEventArgs e)
        {
            ControlAr6000Table.ScanDelayTime = e.Delay;
            ControlAr6000Table.ScanPauseTime = e.Pause;
        }

        private void ControlAr6000Table_OnSendScanPauseTime(object sender, WpfControlDataGridAR6000.ScanInformation e)
        {
            ControlAR6000?.ScanPauseTimeSet(e.Parameter);
        }

        private void ControlAr6000Table_OnSendScanDelayTime(object sender, WpfControlDataGridAR6000.ScanInformation e)
        {
            ControlAR6000?.ScanDelayTimeSet(e.Parameter);
        }


        /* Logic For AR-ONE*/
        private void Connection_AOR_OnLvlSignal(object sender, WpfControlLibraryBut.LevelOfSignalsEventArgs e)
        {
            ReceiverControl.SignalLvl = e.SignalLvl;
            ReceiverControl.SqlLvl = e.SqlLvl;
            ReceiverControl.Frequency = e.Frequency;
        }
    }
}
