﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Threading;

namespace AR6000
{
    public class Ar6000Manager
    {
        public struct AOR
        {
            public string s;
            public Int64 frequency;
            public int Mode;
            public int Bandwidth;
            public int HighPassFilter;
            public int LowPassFilter;
            public int AutomaticGainControl;
            public int AudioGain;
            public int Attenuator;
            public int Amplifier;
            public double LevelSquelch;
            public int NoiseSquelch;
            public int NoiseSquelchOnOff;
            public int AGC;
            public int AFGain;
            public int ManualGain;
            public int ManualRFGain;
            public int IFGain;
            public int SquelchSelect;
            public int SignalLevel;
            public int AutoSignalLevel;
            public int AutoBackLit;
            public int BackLit_OnOff;
            public int BackLitDimmer;
            public int LCDContrast;
            public int BeepLevel;
            public int SpeakerSelect;
            public int ExternalSpeacker;
            public int DelayTime;
            public int FreeScan;
            public int SignalMeterDisplay;
            public int DuplexMode;
            //public int DuplexFrequencymm;
            public int[] DuplexFrequency;
            public int DuplexFrequencyN;
            public int SignalLevelUnit_dBmV;
            public int SignalLevelUnit_dBm;
            // public int[] SearchDataSetting;
            //public int PassFrequency;
            public int[] PassFrequencyList;
            public int SelectMemory_OnOff;
            public int SelectMemoryList;
            // public int TransferCurrentSearchDataToVF0;
            // public int MemoryDataSetting;
            // public int[] MemoryDataList;
            // public int SelectPriorityChanel;
            // public int IFOutSelection;
            public int DEemphasis;
            public int BFOFreq;
            public static string MemoryData;
            public string StatusReceive;
            public Int64 ScanFrequency;
            public static string MBank;
            public static string MChannel;
            public static List<string> memory;
            public static List<List<string>> dataChannel = new List<List<string>>();
            public static List<string> memTwo = new List<string>();
            public static List<string> memthree = new List<string>();
            public string ScanDelayTime;
            public string ScanPauseTime;
        }
        
        public AOR AORstruct = new AOR();
        public static SerialPort port;
        private Thread thread;

        public delegate void ByteEventHandler();
        public delegate void ByteEventHandler1(string data);
        public delegate void ConnectEventHandler();
        public event ByteEventHandler OnReadByte = () => { };
        public event ByteEventHandler OnWriteByte = () => { };
        public event ConnectEventHandler OnConnectPort = () => { };
        public event ConnectEventHandler OnDisconnectPort = () => { };
        public event ByteEventHandler OnDecodedFrq = () => { };
        public event ByteEventHandler OnDecodedSignalLevel = () => { };
        public event ByteEventHandler OnDecodedMode = () => { };
        public event ByteEventHandler OnDecodedHPF = () => { };
        public event ByteEventHandler OnDecodedBW = () => { };
        public event ByteEventHandler OnDecodedLPF = () => { };
        public event ByteEventHandler OnDecodedAGC = () => { };
        public event ByteEventHandler OnDecodedAudioGain = () => { };
        public event ByteEventHandler OnDecodedATT = () => { };
        public event ByteEventHandler OnDecodedRFGain = () => { };
        public event ByteEventHandler OnDecodedManualGain = () => { };
        public event ByteEventHandler OnDecodedNoiseSQuelch = () => { };
        public event ByteEventHandler OnDecodedNoiseSquelchOnOff = () => { };
        public event ByteEventHandler OnDecodedLevelSquelch = () => { };
        public static event ByteEventHandler OnDecodedMemoryChannelDataRead = () => { };
        public event ByteEventHandler OnDecodedScanDelayTime = () => { };
        public event ByteEventHandler OnDecodedScanPauseTime = () => { };
        public event ByteEventHandler OnDecodedReceiveModeStatus = () => { };

        public delegate void ReceiveModeStatusHandler(string status);
        public event ReceiveModeStatusHandler OnReceiveModeStatus = (status) => { };

        public bool SendToArone(string message)
        {
            try
            {
                message += "\x0D\x0A";
                port.WriteLine(message);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        //public bool TurnON()
        //{
        //    try
        //    {
        //        string message = "X\x0D\x0A";
        //        port.WriteLine(message);
        //        Thread.Sleep(30);
        //        return true;
        //    }
        //    catch (Exception ex) { return false; }
        //}
        //public bool TurnOFF()
        //{
        //    try
        //    {
        //        string message = "QP\x0D\x0A";
        //        port.WriteLine(message);
        //        Thread.Sleep(40);
        //        return true;
        //    }
        //    catch (Exception ex) { return false; }
        //}
        public bool FrequencySet(double FrqMhz)
        {
            try
            {
                if ((FrqMhz < 0.09) || (FrqMhz > 6000)) return false;
                Int64 FrqHz = (Int64)((FrqMhz * 1000000) / 1);
                string frq = FrqHz.ToString().PadLeft(10, '0');
                //string message = "RF" + frq + "\x0D\x0A";
                string message = CommandList.RF + frq + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool FrequencyGet()
        {
            try
            {
                if (port.IsOpen)
                {
                    port?.WriteLine(CommandList.RF + "\x0D\x0A");
                    Thread.Sleep(20);
                }
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool SignalLevelGet()
        {
            try
            {
                if (port != null)
                {
                    if (port.IsOpen)
                    {
                        port.WriteLine("LMX\x0D\x0A");
                        Thread.Sleep(20);
                    }
                }

                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool SignalLvlReportSet(int interval)
        {
            try
            {
                if (interval < 0 || interval > 6000) return false;
                var mesage = "LT" + interval.ToString().PadLeft(4, '0') + "\x0D\x0A";
                port.WriteLine(mesage);
                return true;
            }
            catch { return false; }
        }

        public bool SignalLvlReportGet()
        {
            try
            {

                return true;
            }
            catch { return false; }
        }

        //public bool FrequencySet(string freq_MHz)//MHz
        //{
        //    try
        //    {
        //        long frql;
        //        double frqd;
        //        if (!double.TryParse(freq_MHz, out frqd)) { frqd = 0; return false; }
        //        frqd = 1000000 * frqd;
        //        if ((frqd > 3300000000) || (frqd < 10000)) { return false; }
        //        frql = (long)frqd;
        //        freq_MHz = frql.ToString();
        //        freq_MHz = freq_MHz.PadLeft(10, '0');
        //        string message = "RF" + freq_MHz + "\x0D\x0A";
        //        port.WriteLine(message);
        //        Thread.Sleep(40);
        //        return true;
        //    }
        //    catch (Exception ex) { return false; }
        //}

        public bool AutoModeSet(double AuM)
        {
            try
            {
                if ((AuM > 1) || (AuM < 0)) return false;
                string message = "AU" + AuM + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(40);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutoModeGet()
        {
            try
            {
                port?.WriteLine("AU\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool ModeSet(int Mode)
        {
            try
            {
                if ((Mode > 35) || (Mode < 0)) return false;
                //if ((Mode > 8) && (Mode < 21)) return false;
                string message = "MD" + Mode.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool ModeGet()
        {
            try
            {
                port?.WriteLine("MD\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BandWidthSet(int BW)
        {
            try
            {
                if ((BW > 9) || (BW < 0)) return false;
                string message = "BW0" + BW.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool BandwidthGet()
        {
            try
            {
                port?.WriteLine("BW\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool HighPassFiltrSet(char HPF)
        {
            try
            {
                if ((HPF.ToString() != "0") && (HPF.ToString() != "1") && (HPF.ToString() != "2")) { return false; }
                string message = "HP" + HPF.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool HighPassFiltrGet()
        {
            try
            {
                port?.WriteLine("HP\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LowPassFiltrSet(char LPF)
        {
            try
            {
                if ((LPF.ToString() != "0") && (LPF.ToString() != "1") && (LPF.ToString() != "2")) { return false; }
                string message = "LP" + LPF.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LowPassFiltrGet()
        {
            try
            {
                port?.WriteLine("LP\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AudioGainSet(double AG)
        {
            try
            {
                if ((AG > 255) || (AG < 0)) { return false; }
                string message = "VL" + AG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AudioGainGet()
        {
            try
            {
                port?.WriteLine("VL\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutomaticGainControlSet(string AGC)
        {
            string message;
            try
            {
                if ((AGC != "0") && (AGC != "1") && (AGC != "2") && (AGC != "F")) { return false; }

                else
                    message = "AC" + AGC + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AutomaticGainControlGet()
        {
            try
            {
                port?.WriteLine("AC\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AttenuatorSet(int Att)
        {
            try
            {
                if ((Att > 4) || (Att < 0)) return false;
                string message = "AT" + Att.ToString() + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool AttenuatorGet()
        {
            try
            {
                port?.WriteLine("AT\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool LevelSQuelchSet(double LSQ)
        {
            try
            {
                if ((LSQ > 140) || (LSQ < 0)) return false;
                var decimalNumber = Math.Truncate(LSQ);
                var valueToString = decimalNumber.ToString().PadLeft(3, '0');
                string message = "RQ" + valueToString.Substring(0, 3) + ".0" + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool LevelSQuelchGet()
        {
            try
            {
                port?.WriteLine("RQ.\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool NoiseSQuelchSet(double LSQ)
        {
            try
            {
                if ((LSQ > 255) || (LSQ < 0)) return false;
                //string message = "NQ" + LSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
                string message = "NQ" + LSQ.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool NoiseSQuelchGet()
        {
            try
            {
                port?.WriteLine("NQ\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool RFGainSet(int RFG)
        {
            try
            {
                if ((RFG > 110) || (RFG < 0)) return false;
                string message = "RG" + RFG.ToString().PadLeft(3, '0') + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool RFGainGet()
        {
            try
            {
                port?.WriteLine("RG\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool NoiseSQuelchOnOffSet(int NSQ00)
        {
            try
            {
                if ((NSQ00 > 1) || (NSQ00 < 0)) return false;
                string message = "NE" + NSQ00.ToString() + "\x0D\x0A";
                port.WriteLine(message);
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }
        public bool NoiseSQuelchOnOffGet()
        {
            try
            {
                port?.WriteLine("NE\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool MemoryChannelDataRead(string Bank, string Channel)
        {
            try
            {
                //string mBank = "01";
                //string mChannel = "01";
                port.WriteLine("MA" + Bank + "\x0D\x0A");
                Thread.Sleep(20);

                return true;
            }
            catch (Exception ex) { return false; }
        }

        public static bool MemoryChannelDataReadGet()
        {
            try
            {
                //string bNumber = AOR.MBank;
                string chNumber = AOR.MChannel;
                //port.WriteLine("MA" + bNumber + chNumber + "\x0D\x0A");
                //port.WriteLine("MA" + bNumber + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool MemoryReadMode(int bank)
        {
            try
            {
                port?.WriteLine("MR" + bank.ToString().PadLeft(2, '0') + "00" + "\x0D\x0A");
                Thread.Sleep(20);
                return true;
            }
            catch (Exception ex) { return false; }
        }

        public static bool DeleteMemoryChannel(string channel)
        {
            string message = "MQ" + channel + "\x0D\x0A";  //.PadLeft(2, '0')
            port?.WriteLine(message);
            return true;
        }

        public static bool MemoryChannelSetting(string adressbb, string adresscc, double mFreq, int memory, int memoryPass, double stepFreq, int autoMode, int bw, int receiveMode, int rfAtt, int antenne, string textTag)
        {
            try
            {
                string adress = adressbb + adresscc;
                string message_MX = "MX" + adress + " ";

                if ((mFreq < 0.09) || (mFreq > 6000000000)) return false;
                Int64 FrqHz = (Int64)((mFreq) / 1);
                string frq = FrqHz.ToString().PadLeft(10, '0');
                string message_RF = "RF" + frq + " ";

                string message_MP = "MP" + memoryPass + " ";
                string message_ST = "ST" + stepFreq + " ";
                string message_SH = "SH" + "12.5" + " ";
                string message_AU = "AU" + autoMode + " ";
                string message_AN = "AN" + antenne + " ";

                if ((bw > 9) || (bw < 0)) return false;
                string message_BW = "BW" + bw.ToString() + " ";

                if ((memory < 0) || (memory > 1)) return false;
                string message_GA = "GA" + memory.ToString() + " ";

                if ((receiveMode > 35) || (receiveMode < 0)) return false;
                string message_MD = "MD" + receiveMode.ToString().PadLeft(2, '0') + " ";

                if ((rfAtt > 14) || (rfAtt < 0)) return false;
                string message_AT = "AT" + rfAtt.ToString() + " ";

                if (textTag.Length >= 12) textTag = textTag.Substring(0, 12);
                string text = "TM" + textTag + " ";

                string command = message_MX + message_RF + message_GA + message_MP + message_ST + message_SH + message_AU + message_BW + message_MD + message_AT + message_AN + text + "\x0D\x0A";
                port?.WriteLine(command);

                return true;
            }
            catch (Exception ex) { return false; }
        }

        public bool ReceiveModeStatusGet()
        {
            try
            {
                if (port != null)
                {
                    if (port.IsOpen)
                    {
                        var message = "RX" + "\x0D\x0A";
                        port?.WriteLine(message);
                        Thread.Sleep(20);
                    }
                }
                return true;
            }
            catch { return false; }
        }

        public bool ScanGroupSettingSet(int bank)
        {
            try
            {
                var message = "GM" + "00" +
                    AORstruct.ScanDelayTime + "XB000" + AORstruct.LevelSquelch.ToString() + AORstruct.ScanPauseTime + "XMFF" + "ML0" + "BK0139" + "\x0D\x0A";
                //bank.ToString().PadLeft(2, '0')
                port.WriteLine(message);
                return true;
            }
            catch { return false; }
        }

        public void OpenPort(string portName)
        {
            if (port == null)
                port = new SerialPort();
            if (port.IsOpen)
                ClosePort();
            try
            {
                port.PortName = portName;
                port.BaudRate = 115200;
                port.Parity = Parity.None;
                port.DataBits = 8;
                port.StopBits = StopBits.One;
                port.RtsEnable = true;
                port.DtrEnable = true;
                port.ReceivedBytesThreshold = 1000;
                port.Open();
                if (thread != null)
                {
                    thread.Abort();
                    thread.Join(500);
                    thread = null;
                }
                try
                {
                    thread = new Thread(new ThreadStart(ReadExistingComPort)) { IsBackground = true };
                    thread.Start();
                }
                catch (Exception ex) { }
                try { ConnectPort(); }
                catch { }

            }
            catch (Exception ex)
            {
                ex.ToString();
                DisconnectPort();
            }
        }

        protected virtual void ConnectPort()
        {
            OnConnectPort?.Invoke();
        }
        protected virtual void DisconnectPort()
        {
            OnDisconnectPort?.Invoke();
        }
        public void ClosePort()
        {
            try
            {
                port.DiscardInBuffer();
                port.BaseStream.Flush();
            }
            catch (Exception ex) { }
            try
            {
                port.DiscardOutBuffer();
            }
            catch (Exception ex) { }
            try
            {
                port.Close();
                if (thread != null)
                {
                    thread.Abort();
                    thread.Join(500);
                    thread = null;
                }
                DisconnectPort();
            }
            catch (Exception ex) { }
        }
        protected virtual void ReadByte(byte[] bByte)
        {
            OnReadByte?.Invoke();
        }
        protected virtual void WriteByte(byte[] bByte)
        {
            OnWriteByte?.Invoke();
        }

        public bool StartScanSet(string bank)
        {
            try
            {
                string message = "MS" + bank.PadLeft(2, '0') + "\x0D\x0A";
                port?.WriteLine(message);
                return true;
            }
            catch { return false; }
            //var linkOn = "ML1" + "\x0D\x0A";
            //port?.WriteLine(linkOn);
        }

        public bool StopScanSet()
        {
            try
            {
                port?.WriteLine("VB");
                return true;
            }
            catch { return false; }

        }

        public bool DelayTime_ScanDelaySet(string time)
        {
            string message = "XD" + time + "\x0D\x0A";
            port?.WriteLine(message);
            return true;
        }

        public bool DelayTime_ScanDelayGet()
        {
            port.WriteLine("XD\x0D\x0A");
            return true;
        }

        public bool PauseTime_ScanPauseTimeSet(string time)
        {
            var messge = "XP" + time + "\x0D\x0A";
            port?.WriteLine(messge);
            return true;
        }

        public bool PauseTime_ScanPauseTimeGet()
        {
            port.WriteLine("XP\x0D\x0A");
            return true;
        }

        public bool SearchAutoStoreOnOff()
        {
            try
            {
                var message = "AS1\x0D\x0A";
                port.WriteLine(message);
                return true;
            }
            catch { return false; }
        }

        double ArOne_frequency;
        private void ReadExistingComPort()
        {
            string comand = null;
            while (true)
            {
                try
                {
                    comand = null;
                    //string s = _port.ReadExisting();
                    if (port != null)
                    {
                        if (port.IsOpen)
                        {
                            string s = port.ReadLine();
                            if (s.Length > 2)
                            {

                                comand = s.Substring(0, 2);
                                if (s.Substring(3, 2) == "RF")
                                {
                                    double.TryParse(s.Substring(5, 10), out ArOne_frequency);
                                    Int64.TryParse((ArOne_frequency).ToString(), out AORstruct.frequency);
                                    OnDecodedFrq?.Invoke();
                                }
                                switch (comand)
                                {
                                    case "LT":
                                        var data = int.Parse(s.Substring(2, 4));
                                        break;
                                    case "LM":
                                        AORstruct.SignalLevel = int.Parse(s.Substring(2, 3));
                                        OnDecodedSignalLevel?.Invoke();
                                        break;

                                    case "VL":
                                        int.TryParse(s.Substring(2, 3), out AORstruct.AudioGain);
                                        OnDecodedAudioGain?.Invoke();
                                        break;

                                    case "MD":
                                        int.TryParse(s.Substring(2, 2), out AORstruct.Mode);
                                        OnDecodedMode?.Invoke();
                                        break;

                                    case "BW":
                                        int.TryParse(s.Substring(2, 1), out AORstruct.Bandwidth);
                                        OnDecodedBW?.Invoke();
                                        break;

                                    case "AT":
                                        int.TryParse(s.Substring(2, 2), out AORstruct.Attenuator);
                                        OnDecodedATT?.Invoke();
                                        break;

                                    case "HP":
                                        int.TryParse(s.Substring(2, 1), out AORstruct.HighPassFilter);
                                        OnDecodedHPF?.Invoke();
                                        break;
                                    case "LP":
                                        int.TryParse(s.Substring(2, 1), out AORstruct.LowPassFilter);
                                        OnDecodedLPF?.Invoke();
                                        break;

                                    case "AC":
                                        string bbc = s.Substring(2, 1);
                                        if (bbc == "F") { AORstruct.AGC = 3; }
                                        else
                                        {
                                            int.TryParse(s.Substring(2, 1), out AORstruct.AGC);
                                        }
                                        bbc = "";
                                        OnDecodedAGC?.Invoke();
                                        break;
                                    case "RG":          //agc must be manual
                                        int.TryParse(s.Substring(3, 3), out AORstruct.ManualRFGain);
                                        OnDecodedRFGain?.Invoke();
                                        break;
                                    case "NQ":
                                        if (s.Length == 8)
                                        {
                                            int.TryParse(s.Substring(3, 3), out AORstruct.NoiseSquelch);
                                            OnDecodedNoiseSQuelch?.Invoke();
                                        }
                                        break;
                                    case "RQ":
                                        double.TryParse(s.Substring(3, 3), out var integer);
                                        double.TryParse(s.Substring(7, 1), out var dec);
                                        AORstruct.LevelSquelch = integer + dec / 10;
                                        //double.TryParse(s.Substring(3, 3), out AORstruct.LevelSquelch);
                                        OnDecodedLevelSquelch?.Invoke();
                                        break;
                                    case "NE":
                                        int.TryParse(s.Substring(2, 1), out AORstruct.NoiseSquelchOnOff);
                                        OnDecodedNoiseSquelchOnOff?.Invoke();
                                        break;
                                    case "MS":
                                        var statusScan = s.Substring(0, 2);
                                        OnReceiveModeStatus?.Invoke(statusScan);

                                        AORstruct.StatusReceive = s.Substring(5, 4);

                                        double.TryParse(s.Substring(20, 10), out double _ScanFreq);
                                        Int64.TryParse((_ScanFreq).ToString(), out AORstruct.ScanFrequency);

                                        OnDecodedReceiveModeStatus?.Invoke();
                                        break;
                                    case "MX":
                                        AOR.MemoryData = s; //.Substring(2, 55)
                                        /*
                                        AOR.memory = new List<string> (s.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries));
                                        foreach (var item in AOR.memory)
                                        {
                                            AOR.memTwo.Add(item);
                                        }
                                        IEnumerable<string> mem = AOR.memTwo.Distinct();
                                        */
                                        AOR.memthree.Add(s);
                                        OnDecodedMemoryChannelDataRead?.Invoke();
                                        break;
                                    case "XD":
                                        AORstruct.ScanDelayTime = s.Substring(2, 3);
                                        OnDecodedScanDelayTime?.Invoke();
                                        break;
                                    case "XP":
                                        AORstruct.ScanPauseTime = s.Substring(2, 2);
                                        OnDecodedScanPauseTime?.Invoke();
                                        break;
                                    case "MR":
                                        var statusMem = s.Substring(0, 2);
                                        OnReceiveModeStatus?.Invoke(statusMem);
                                        break;
                                    case "VB":
                                        var statusVFO = s.Substring(0, 2);
                                        OnReceiveModeStatus?.Invoke(statusVFO);
                                        break;
                                    case "GM":
                                        var group = s.Substring(0, 2);
                                        OnReceiveModeStatus?.Invoke(group);
                                        break;
                                }
                            }
                        }

                    }


                }
                catch { }
            }
        }

    }
}
