﻿using System;
using System.Collections.Generic;

namespace AR6000
{
    public struct Variables
    {
        public string s;
        public long freqToLong;
        public double freqToDouble;
        public int Mode;
        public int Bandwidth;
        public int HighPassFilter;
        public int LowPassFilter;
        public int AutomaticGainControl;
        public int AudioGain;
        public int Attenuator;
        public int Amplifier;
        public double LevelSquelch;
        public int NoiseSquelch;
        public int NoiseSquelchOnOff;
        public int AGC;
        public int AFGain;
        public int ManualGain;
        public int ManualRFGain;
        public int IFGain;
        public int SquelchSelect;
        public int SignalLevel;
        public int AutoSignalLevel;
        public int AutoBackLit;
        public int BackLit_OnOff;
        public int BackLitDimmer;
        public int LCDContrast;
        public int BeepLevel;
        public int SpeakerSelect;
        public int ExternalSpeacker;
        public int DelayTime;
        public int FreeScan;
        public int SignalMeterDisplay;
        public int DuplexMode;
        //public int DuplexFrequencymm;
        public int[] DuplexFrequency;
        public int DuplexFrequencyN;
        public int SignalLevelUnit_dBmV;
        public int SignalLevelUnit_dBm;
        // public int[] SearchDataSetting;
        //public int PassFrequency;
        public int[] PassFrequencyList;
        public int SelectMemory_OnOff;
        public int SelectMemoryList;
        // public int TransferCurrentSearchDataToVF0;
        // public int MemoryDataSetting;
        // public int[] MemoryDataList;
        // public int SelectPriorityChanel;
        // public int IFOutSelection;
        public int DEemphasis;
        public int BFOFreq;
        public static string MemoryData;
        public string StatusReceive;
        public Int64 ScanFrequency;
        public static string MBank;
        public static string MChannel;
        public static List<string> memory;
        public static List<List<string>> dataChannel = new List<List<string>>();
        public static List<string> memTwo = new List<string>();
        public static List<string> memthree = new List<string>();
        public string ScanDelayTime;
        public string ScanPauseTime;
    }
}