﻿namespace AR6000
{
    public interface ILibraryStructure
    {
        bool FrequencyGet();
        bool FrequencySet(double frequency);

        bool SignalLevelGet();

        bool ModeGet();
        bool ModeSet(int mode);

        bool BandWidthGet();
        bool BandWidthSet(int band);

        bool HighPassFilterGet();
        bool HighPassFilterSet(char hpf);

        bool LowPassFilterGet();
        bool LowPassFilterSet(char lpf);

        bool AudioGainGet();
        bool AudioGainSet(double ag);

        bool AutomaticGainControlGet();
        bool AutomaticGainControlSet(string agc);

        bool AttenuatorGet();
        bool AttenuatorSet(int att);

        bool LevelSquelchGet();
        bool LevelSquelchSet(double lsq);

        bool NoiseSquelchGet();
        bool NoiseSquelchSet(double nsq);

        bool RFGainGet();
        bool RFGainSet(int rfg);

        bool NoiseSquelchOnOffGet();
        bool NoiseSquelchOnOffSet(int nsq);

        bool MemoryChannelDataRead(string bank);
        bool MemoryReadMode(int bank);
        //bool DeleteMemoryChannel(string channel);

        bool ReceiveModeStatusGet();

        bool ScanGroupSettingSet(int bank);

        void OpenPort(string portName);
        void ClosePort();

        bool StartScanSet(string bank);
        bool StopScanSet();

        bool DelayTime_ScanDelayGet();
        bool DelayTime_ScanDelaySet(string time);

        bool PauseTime_ScanPauseTimeGet();
        bool PauseTime_ScanPauseTimeSet(string time);
    }
}