﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using AR6000;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void InitConnection() 
        {
            try 
            {
                gaugeLvl.PrimaryScale.RangeBegin = -120;
                gaugeLvl.PrimaryScale.RangeEnd = 0;

                PortDisconnected();
                SubscribeToReceiver();
            }
            catch { }            
        }

        /**
         * Subscribe to events from receiver
         */
        private void SubscribeToReceiver()
        {
            AR6000DLL = new Ar6000Manager();

            AR6000DLL.OnConnectPort += new Ar6000Manager.ConnectEventHandler(PortConnected);
            AR6000DLL.OnDisconnectPort += new Ar6000Manager.ConnectEventHandler(PortDisconnected);
            AR6000DLL.OnSendFrequency += AR6000DLL_OnSendFrequency;
            AR6000DLL.OnSendSignalLevel += AR6000DLL_OnSendSignalLevel;
            AR6000DLL.OnSendMode += AR6000DLL_OnSendMode;
            AR6000DLL.OnSendBandWidth += AR6000DLL_OnSendBandWidth;
            AR6000DLL.OnSendAttenuator += AR6000DLL_OnSendAttenuator;
            AR6000DLL.OnSendLowPassFilter += AR6000DLL_OnSendLowPassFilter;
            AR6000DLL.OnSendHighPassFilter += AR6000DLL_OnSendHighPassFilter;
            AR6000DLL.OnSendAutomaticGainControl += AR6000DLL_OnSendAutomaticGainControl;
            AR6000DLL.OnSendRFGain += AR6000DLL_OnSendRFGain;
            AR6000DLL.OnDecodedNoiseSQuelch += new Ar6000Manager.ByteEventHandler(DecodedNoiseSquelch);
            AR6000DLL.OnDecodedNoiseSquelchOnOff += new Ar6000Manager.ByteEventHandler(DecodedNoiseSquelchOnOff);
            AR6000DLL.OnDecodedLevelSquelch += AR6000DLL_OnDecodedLevelSquelch;
            AR6000DLL.OnDecodedAudioGain += new Ar6000Manager.ByteEventHandler(DecodedAudioGain);
            AR6000DLL.OnDecodedScanDelayTime += AR6000DLL_OnDecodedScanDelayTime;
            AR6000DLL.OnDecodedScanPauseTime += AR6000DLL_OnDecodedScanPauseTime;
            AR6000DLL.OnDecodedReceiveModeStatus += AR6000DLL_OnDecodedReceiveModeStatus;
            AR6000DLL.OnReceiveModeStatus += AR6000DLL_OnReceiveModeStatus;
        }







        #region ConnectionToReceiver
        public void ConnectionToMain()
        {
            try
            {
                if (SerialPort.GetPortNames().Any(x => x == portAdress))    //Проверка, существует ли COM-порт
                {
                    if (Ar6000Manager.port == null)             //Проверка, открыт ли COM-порт
                    {
                        AR6000DLL.OpenPort(portAdress);
                        ARPort_Null();
                    }
                    else
                    {
                        ARPort_IsOpen();
                    }                    
                }
                else
                {
                    string messRus = "COM-порт отсутствует";
                    string messEng = "No COM-port";
                    switch (langMessage)
                    {
                        case "Rus":
                            MessageBox.Show(messRus);
                            break;
                        case "Eng":
                            MessageBox.Show(messEng);
                            break;
                    }
                }
            }
            catch { }
        }
        private void ARPort_Null()
        {
            if (Ar6000Manager.port.IsOpen)    //Проверка, открылся ли COM-порт
            {
                AskReceiver();

                isLvlRequest = true;
                TaskManager();
            }
            else
            {
                isLvlRequest = false;
            }
        }
        private void ARPort_IsOpen()
        {
            if (Ar6000Manager.port.IsOpen)   //Если порт открыт, то закрываем
            {
                AR6000DLL.ClosePort();
                BorderBrush = Brushes.Gray;
                isLvlRequest = false;
            }
            else
            {
                AR6000DLL.OpenPort(PortAdress);
                AskReceiver();

                isLvlRequest = true;
                TaskManager();
            }
        }

        public void ComIsOpen()
        {
            if (Ar6000Manager.port.IsOpen)
            {
                comIsOpen = true;
                OnComIsOpen?.Invoke(this, new ComIsOpenEventArgs(comIsOpen));
            }
            else
            {
                comIsOpen = false;
                OnComIsOpen?.Invoke(this, new ComIsOpenEventArgs(comIsOpen));
            }
        }
        #endregion
    }
}
