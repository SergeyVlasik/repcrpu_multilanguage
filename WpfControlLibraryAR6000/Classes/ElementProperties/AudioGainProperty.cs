﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        
        private void bMute_Checked(object sender, RoutedEventArgs e)
        {
            ResourceDictionary dict = new ResourceDictionary
            {
                Source = new Uri(String.Format("/WpfControlLibraryAR6000;component/Icons/Icon.Mute.xaml"), UriKind.Relative)
            };

            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                          where d.Source != null && d.Source.OriginalString.StartsWith("/WpfControlLibraryAR6000;component/Icons/Icon.")
                                          select d).First();

            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);
                Resources.MergedDictionaries.Insert(ind, dict);

            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }
            try
            {
                valueMemory = SAudioGain.Value;
                AR6000DLL?.AudioGainSet(0);
                SAudioGain.Value = 0;
            }
            catch { }
        }

        private void bMute_Unchecked(object sender, RoutedEventArgs e)
        {
            ResourceDictionary dict = new ResourceDictionary
            {
                Source = new Uri(String.Format("/WpfControlLibraryAR6000;component/Icons/Icon.Play.xaml"), UriKind.Relative)
            };

            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                          where d.Source != null && d.Source.OriginalString.StartsWith("/WpfControlLibraryAR6000;component/Icons/Icon.")
                                          select d).First();

            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);
                Resources.MergedDictionaries.Insert(ind, dict);

            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }

            try
            {
                if (valueMemory != 0)
                {
                    SAudioGain.Value = valueMemory;
                }
                else { }

                AR6000DLL?.AudioGainSet(SAudioGain.Value);
            }
            catch { }
        }
        
    }
}
