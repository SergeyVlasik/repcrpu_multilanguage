﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void ComboboxItems() 
        {
            comboBoxMode.SelectedIndex = 0;
            comboBoxBW.SelectedIndex = 0;
            comboBoxATT.SelectedIndex = 0;
            comboBoxLPF.SelectedIndex = 0;
            comboBoxHPF.SelectedIndex = 0;
            comboBoxAGC.SelectedIndex = 0;
            comboBoxSQL.SelectedIndex = 0;
            comboBoxFrqStep.SelectedIndex = 0;
        }
    }
}
