﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void textBoxFrq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!textBoxFrq.Text.Contains(",")
              && textBoxFrq.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }

        private void textBoxFrq_KeyDown(object sender, KeyEventArgs e)
        {
            double frqq;

            if (e.Key == Key.Enter)
            {
                if ((DotInFrq) && (countafterdot == 0))
                {
                    DotInFrq = false;
                    countafterdot = 0;

                    double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                    if ((frqq > 0.09) && (frqq < 6000)) AR6000DLL.FrequencySet(frqq);
                    FrqToAOR = "";
                    textBoxFrq.Text = FrqToAOR;
                    return;
                }

                DotInFrq = false;
                countafterdot = 0;

                double.TryParse(textBoxFrq.Text, out frqq);

                if ((frqq > 0.09) && (frqq < 6000))
                {
                    AR6000DLL.FrequencySet(frqq);
                }
                else
                {
                    switch (langMessage)
                    {
                        case "Rus":
                            MessageBox.Show(messageRus);
                            break;
                        case "Eng":
                            MessageBox.Show(messgeEng);
                            break;
                    }
                }

                textBoxFrq.Text = FrqToAOR;
                Keyboard.ClearFocus();
            }
        }

        private void textBoxFrq_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
        }

        private void textBoxFrq_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }

        private void textBoxFrq_LostFocus(object sender, RoutedEventArgs e)
        {

        }
    }
}
