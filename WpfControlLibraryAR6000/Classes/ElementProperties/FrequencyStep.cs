﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void butFrqMinus_Click(object sender, RoutedEventArgs e)
        {            
            var myfrq = double.Parse(Frequency.ToString());
            myfrq -= step_frq_MHz;
            if (myfrq > 30000000.0)
            {
                AR6000DLL.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть меньше!"); }
        }

        private void comboBoxFrqStep_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (comboBoxFrqStep.SelectedIndex)
            {
                case 0: step_frq_MHz = 1f; break;// / 1000000f; break;
                case 1: step_frq_MHz = 10f; break;// / 1000000f; break;
                case 2: step_frq_MHz = 50f; break;// / 1000000f; break;
                case 3: step_frq_MHz = 100f; break;// / 1000000f; break;
                case 4: step_frq_MHz = 500f; break;// / 1000000f; break;
                case 5: step_frq_MHz = 1000f; break;// / 1000000f; break;
                case 6: step_frq_MHz = 5000f; break;// / 1000000f; break;
                case 7: step_frq_MHz = 10000f; break;// / 1000000f; break;
                case 8: step_frq_MHz = 50000f; break;// / 1000000f; break;
                case 9: step_frq_MHz = 100000f; break;// / 1000000f; break;
                case 10: step_frq_MHz = 500000f; break;// / 1000000f; break;
                case 11: step_frq_MHz = 1000000f; break;// / 1000000f; break;
                default: MessageBox.Show("case not worked!"); break;
            }
        }

        private void butFrqPlus_Click(object sender, RoutedEventArgs e)
        {
            var myfrq = double.Parse(Frequency.ToString());
            myfrq += step_frq_MHz;
            if (myfrq < 6000000000.0)
            {
                AR6000DLL.FrequencySet(myfrq / 1000000);
            }
            else { };//MessageBox.Show("Не может быть больше!"); }
        }
    }
}
