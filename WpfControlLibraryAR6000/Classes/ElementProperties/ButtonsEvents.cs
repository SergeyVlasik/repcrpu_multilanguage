﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void ButOne_Click(object sender, RoutedEventArgs e)
        {
            try
            {                
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "1";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "1";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "1";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButTwo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "2";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "2";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "2";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButThree_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "3";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "3";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "3";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButFour_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "4";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "4";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "4";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButFive_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "5";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "5";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "5";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButSix_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "6";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "6";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "6";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButSeven_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "7";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "7";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "7";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButEight_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "8";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "8";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "8";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButNine_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "9";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "9";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR; // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "9";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButZero_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "0";
                    textBoxFrq.Text = FrqToAOR; //" МГц";
                    return;
                }
                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "0";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR; //MГц                
            }
            catch { }
        }

        private void ButDot_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DotInFrq == true) return;
                if (countafterdot > 3) return;
                DotInFrq = true;
                countafterdot = 0;
                FrqToAOR += ",";
                textBoxFrq.Text = FrqToAOR; // МГц";                
            }
            catch { }
        }

        private void ButEsc_Click(object sender, RoutedEventArgs e)
        {
            DotInFrq = false;
            countafterdot = 0;
            FrqToAOR = "";
            textBoxFrq.Text = FrqToAOR;
        }

        private void ButEnt_Click(object sender, RoutedEventArgs e)
        {
            double frqq;

            if ((DotInFrq) && (countafterdot == 0))
            {
                DotInFrq = false;
                countafterdot = 0;

                double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                if ((frqq > 0.09) && (frqq < 6000)) AR6000DLL.FrequencySet(frqq);
                FrqToAOR = "";
                textBoxFrq.Text = FrqToAOR.ToString();
                return;
            }

            DotInFrq = false;
            countafterdot = 0;

            double.TryParse(textBoxFrq.Text, out frqq);

            if ((frqq > 0.09) && (frqq < 6000))
            {
                AR6000DLL.FrequencySet(frqq);
            }
            else
            {
                switch (langMessage)
                {
                    case "Rus":
                        MessageBox.Show(messageRus);
                        break;
                    case "Eng":
                        MessageBox.Show(messgeEng);
                        break;
                }
            }

            textBoxFrq.Text = FrqToAOR;
            OnDataToSave?.Invoke(this, new DataToSaveEventArgs(Frequency, Mode, Attenuator, Bandwidth));
        }
    }
}
