﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        public event EventHandler<ComIsOpenEventArgs> OnComIsOpen;
        public event EventHandler<FreqBwEventArgs> OnFrqBwPeleng;
        public event EventHandler<DataToSaveEventArgs> OnDataToSave;
        public event EventHandler<LevelOfSignalsEventArgs> OnLvlSignal;
        public event EventHandler<ScanInformationEventArgs> OnScanInformation;

        public delegate void ChangedFrequencyPanorama(object sender, Int64 ChangedFreq, double ChangedBw);
        public event ChangedFrequencyPanorama FrequencyChanged;

        public delegate void SendScanModeStatus(object sender, string data, long frequency);
        public event SendScanModeStatus OnSendScanModeStatus = (sender, data, frequency) => { };

        public delegate void SendReceiveModeStatus(object sender, string data);
        public event SendReceiveModeStatus OnSendReceiveModeStatus = (sender, data) => { };
    }
}
