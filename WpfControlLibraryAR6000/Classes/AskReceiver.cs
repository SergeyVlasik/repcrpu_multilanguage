﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void AskReceiver() 
        {
            try
            {
                AR6000DLL.FrequencyGet();
                Thread.Sleep(5);

                

                AR6000DLL.SignalLevelGet();
                Thread.Sleep(5);

                //AR6000DLL.SignalLvlReportSet(10);

                AR6000DLL.ModeGet();
                Thread.Sleep(5);

                AR6000DLL.BandWidthGet();
                Thread.Sleep(5);

                AR6000DLL.AttenuatorGet();
                Thread.Sleep(5);

                AR6000DLL.LowPassFilterGet();
                Thread.Sleep(5);

                AR6000DLL.HighPassFilterGet();
                Thread.Sleep(5);

                AR6000DLL.AutomaticGainControlGet();
                Thread.Sleep(5);

                AR6000DLL.RFGainGet();
                Thread.Sleep(5);

                AR6000DLL.NoiseSquelchOnOffGet();
                Thread.Sleep(5);

                //AR6000DLL.NioseSQuelchGet();
                //Thread.Sleep(5);

                AR6000DLL.AudioGainGet();
                Thread.Sleep(5);

                AR6000DLL.DelayTime_ScanDelayGet();
                Thread.Sleep(5);

                AR6000DLL.PauseTime_ScanPauseTimeGet();
                Thread.Sleep(5);
                //AR6000DLL.StatusModeReceive();
                //Thread.Sleep(5);
                //AR6000DLL.MemoryReadMode();
                //Thread.Sleep(5);
                //AR6000Manager.MemoryChannelDataReadGet();
                //Thread.Sleep(5);
            }
            catch { }
        }
    }
}
