﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        string langMessage = "Rus";
        public string LangMessage
        {
            get { return langMessage; }
            set { langMessage = value; }
        }

        public string portAdress = "COM3";
        public int baudRate = 19200;
        public string PortAdress
        {
            get => portAdress;
            set { portAdress = value; }
        }
        public int BaudRate
        {
            get => baudRate;
            set { baudRate = value; }
        }

        bool comIsOpen = false;

        int countafterdot = 0;
        bool DotInFrq = false;
        bool zapolneno = false;

        public float step_frq_MHz = 10f / 1000000f;
        double valueMemory = 10;

        #region Title
        string dB = "dB";
        public string Hz = "Hz";
        string kHz = "kHz";
        string MHz = " MHz";
        string FrqToAOR = "";
        string dBm = "dBm";
        string UsilVKL = "RF ON";
        string vykl = "Off";
        string vkl = "On";
        string Attt = "Att";
        string Avto = "Auto";
        readonly string messageRus = "Неверно введена частота";
        readonly string messgeEng = "Incorrectly entered frequency";
        #endregion

        #region DataFromReceiver

        private long frequency = 104;
        public long Frequency
        {
            get => frequency;
            set
            {
                frequency = value;
            }
        }

        public long FrecuencyChanged 
        {
            get => Frequency;
            set
            {
                Frequency = value;
                OnFreqChanged(value, DecodeBwPanoram(BandwidthOnChanged));
            }
        }
        public int SignalLevel
        {
            get { return AR6000DLL.AORstruct.SignalLevel; }
            set { AR6000DLL.AORstruct.SignalLevel = value; }
        }
        public int Mode
        {
            get { return AR6000DLL.AORstruct.Mode; }
            set { AR6000DLL.AORstruct.Mode = value; }
        }
        public int Bandwidth
        {
            get { return AR6000DLL.AORstruct.Bandwidth; }
            set { AR6000DLL.AORstruct.Bandwidth = value; }
        }

        public int BandwidthOnChanged
        {
            get => AR6000DLL.AORstruct.Bandwidth;
            set
            {
                AR6000DLL.AORstruct.Bandwidth = value;
                OnFreqChanged(FrecuencyChanged, DecodeBwPanoram(BandwidthOnChanged));
            }
        }
        public int Attenuator
        {
            get { return AR6000DLL.AORstruct.Attenuator; }
            set { }
        }
        public int HighPassFilter
        {
            get { return AR6000DLL.AORstruct.HighPassFilter; }
            set { }
        }
        public int LowPassFilter
        {
            get { return AR6000DLL.AORstruct.LowPassFilter; }
            set { }
        }
        public int AGC
        {
            get { return AR6000DLL.AORstruct.AGC; }
            set { }
        }
        public int NoiseSquelch
        {
            get { return AR6000DLL.AORstruct.NoiseSquelch; }
            set { }
        }

        public double LevelSquelch 
        {
            get { return AR6000DLL.AORstruct.LevelSquelch; }
            set { }
        }
        public int NoiseSquelchOnOff
        {
            get { return AR6000DLL.AORstruct.NoiseSquelchOnOff; }
            set { }
        }
        public int AudioGain
        {
            get { return AR6000DLL.AORstruct.AudioGain; }
            set { }
        }
        #endregion

        private double DecodeBwPanoram(int bw)
        {
            switch (bw)
            {
                case 0: return 0.2;
                case 1: return 0.5;
                case 2: return 1.0;
                case 3: return 3.0;
                case 4: return 6.0;
                case 5: return 15;
                case 6: return 30;
                case 7: return 100;
                case 8: return 200;
                case 9: return 300;
                default: return 200;
            }
        }
    }
}
