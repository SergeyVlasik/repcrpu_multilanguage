﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public class LevelOfSignalsEventArgs : EventArgs
    {
        public LevelOfSignalsEventArgs(double signalLvl, double sqlLvl)
        {
            SignalLvl = signalLvl;
            SqlLvl = sqlLvl;
        }

        public double SignalLvl { get; set; }
        public double SqlLvl { get; set; }
    }
}
