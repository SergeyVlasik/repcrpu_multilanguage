﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public class ComIsOpenEventArgs : EventArgs
    {
        public ComIsOpenEventArgs(bool isOpen)
        {
            IsOpen = isOpen;
        }

        public bool IsOpen { get; private set; }
    }
}
