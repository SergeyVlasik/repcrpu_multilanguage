﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public class DataToSaveEventArgs : EventArgs
    {
        public DataToSaveEventArgs(Int64 Freq, int Md, int At, int BW) 
        {
            Frequency = Freq;
            Modulation = Md;
            Attenuator = At;
            Bandwidth = BW;
        }

        public Int64 Frequency { get; set; }
        public int Modulation { get; set; }
        public int Attenuator { get; set; }

        public int Bandwidth { get; set; }
    }
}
