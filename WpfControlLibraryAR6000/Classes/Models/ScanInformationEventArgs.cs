﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public class ScanInformationEventArgs : EventArgs
    {
        public ScanInformationEventArgs(string pause, string delay) 
        {
            Pause = pause;
            Delay = delay;
        }

        public string Pause { get; private set; }
        public string Delay { get; private set; }
    }
}
