﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        protected virtual void OnFreqChanged(Int64 Frequency, double ChangedBw) 
        {
            FrequencyChanged?.Invoke(this, Frequency, ChangedBw);
        }
    }
}
