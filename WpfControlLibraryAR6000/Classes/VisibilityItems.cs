﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void PortDisconnected()
        {
            Display.IsEnabled = false;
            ButOne.IsEnabled = false;
            ButTwo.IsEnabled = false;
            ButThree.IsEnabled = false;
            ButFour.IsEnabled = false;
            ButFive.IsEnabled = false;
            ButSix.IsEnabled = false;
            ButSeven.IsEnabled = false;
            ButEight.IsEnabled = false;
            ButNine.IsEnabled = false;
            ButZero.IsEnabled = false;
            ButDot.IsEnabled = false;
            ButEnt.IsEnabled = false;
            ButEsc.IsEnabled = false;
            comboBoxMode.IsEnabled = false;
            comboBoxATT.IsEnabled = false;
            comboBoxBW.IsEnabled = false;
            comboBoxHPF.IsEnabled = false;
            comboBoxLPF.IsEnabled = false;
            lsiglevel.IsEnabled = false;
            lMode.IsEnabled = false;
            lBw.IsEnabled = false;
            lAt.IsEnabled = false;
            comboBoxFrqStep.IsEnabled = false;
            butFrqMinus.IsEnabled = false;
            butFrqPlus.IsEnabled = false;
            comboBoxSQL.IsEnabled = false;
            comboBoxAGC.IsEnabled = false;
            sliderSQL.IsEnabled = false;
            sliderManualGain.IsEnabled = false;
            textBoxFrq.IsEnabled = false;
            SAudioGain.IsEnabled = false;
        }

        private void PortConnected()
        {
            Display.IsEnabled = true;
            ButOne.IsEnabled = true;
            ButTwo.IsEnabled = true;
            ButThree.IsEnabled = true;
            ButFour.IsEnabled = true;
            ButFive.IsEnabled = true;
            ButSix.IsEnabled = true;
            ButSeven.IsEnabled = true;
            ButEight.IsEnabled = true;
            ButNine.IsEnabled = true;
            ButZero.IsEnabled = true;
            ButDot.IsEnabled = true;
            ButEnt.IsEnabled = true;
            ButEsc.IsEnabled = true;
            comboBoxMode.IsEnabled = true;
            comboBoxATT.IsEnabled = true;
            comboBoxBW.IsEnabled = true;
            comboBoxHPF.IsEnabled = true;
            comboBoxLPF.IsEnabled = true;
            lsiglevel.IsEnabled = true;
            lMode.IsEnabled = true;
            lBw.IsEnabled = true;
            lAt.IsEnabled = true;
            comboBoxFrqStep.IsEnabled = true;
            butFrqMinus.IsEnabled = true;
            butFrqPlus.IsEnabled = true;
            comboBoxSQL.IsEnabled = true;
            comboBoxAGC.IsEnabled = true;
            sliderSQL.IsEnabled = true;
            sliderManualGain.IsEnabled = true;
            textBoxFrq.IsEnabled = true;
            SAudioGain.IsEnabled = true;
        }
    }
}
