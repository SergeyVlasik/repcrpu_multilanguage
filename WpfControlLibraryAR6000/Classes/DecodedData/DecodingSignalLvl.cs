﻿using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private bool isLvlRequest = false;
        CancellationTokenSource cts = new CancellationTokenSource();
        private int _CancellationTokenDelay = 100;
        public int CancellationTokenDelay
        {
            get { return _CancellationTokenDelay; }
            set
            {
                if (_CancellationTokenDelay != value)
                {
                    _CancellationTokenDelay = value;
                }
            }
        }
        private int _RequestLvlTimer = 10;
        public int RequestLvlTimer
        {
            get { return _RequestLvlTimer; }
            set
            {
                if (_RequestLvlTimer != value)
                {
                    _RequestLvlTimer = value;
                }
            }
        }

        private void TaskManager() 
        {
            Task.Run(() => SignalLvlRequest());
        }

        private async void SignalLvlRequest() 
        {
            try 
            {
                while (isLvlRequest)
                {
                    cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;

                    AR6000DLL.SignalLevelGet();

                    var T = await Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; });
                    await Task.Delay(_RequestLvlTimer);
                }
            }
            catch { }            
        }


        private void AR6000DLL_OnSendSignalLevel(int parameter)
        {
            DispatchIfNecessary(() =>
            {
                lsiglevel.Content = "-" + parameter + dB;//" дБ";

                //Отображение уровня сигнала на метре
                var level = parameter * (-1.0);
                if (level <= 0 && level >= -120) { gaugeLvl.PrimaryScale.Value = level; }
                else
                {
                    if (level > 0) { gaugeLvl.PrimaryScale.Value = 0; }
                    if (level < -120) { gaugeLvl.PrimaryScale.Value = -120; }
                }

                PSigLevel.Value = level;
                OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(level, LevelSquelch * -1d));
            });
        }
    }
}
