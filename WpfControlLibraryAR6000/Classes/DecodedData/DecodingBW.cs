﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void comboBoxBW_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                switch (comboBoxBW.SelectedIndex)
                {
                    case 0: AR6000DLL.BandWidthSet(0); break;
                    case 1: AR6000DLL.BandWidthSet(1); break;
                    case 2: AR6000DLL.BandWidthSet(2); break;
                    case 3: AR6000DLL.BandWidthSet(3); break;
                    case 4: AR6000DLL.BandWidthSet(4); break;
                    case 5: AR6000DLL.BandWidthSet(5); break;
                    case 6: AR6000DLL.BandWidthSet(6); break;
                    case 7: AR6000DLL.BandWidthSet(7); break;
                    case 8: AR6000DLL.BandWidthSet(8); break;
                    case 9: AR6000DLL.BandWidthSet(9); break;
                    default: break;
                }
                Thread.Sleep(20);
                AR6000DLL.BandWidthGet();
                OnDataToSave?.Invoke(this, new DataToSaveEventArgs(Frequency, Mode, Attenuator, Bandwidth));
            }
            catch { }
        }


        private void AR6000DLL_OnSendBandWidth(int parameter)
        {
            try
            {
                lBw.Dispatcher.Invoke((delegate ()
                {
                    switch (parameter)
                    {
                        case 0: lBw.Content = "0.2 " + kHz; break;// кГц"; 
                        case 1: lBw.Content = "0.5 " + kHz; break; //кГц"; 
                        case 2: lBw.Content = "1 " + kHz; break; //кГц"; 
                        case 3: lBw.Content = "3.0 " + kHz; break; //кГц";
                        case 4: lBw.Content = "6.0 " + kHz; break; //кГц";
                        case 5: lBw.Content = "15 " + kHz; break; //кГц"; 
                        case 6: lBw.Content = "30 " + kHz; break; //кГц"; 
                        case 7: lBw.Content = "100 " + kHz; break; //кГц"; 
                        case 8: lBw.Content = "200 " + kHz; break; //кГц"; 
                        case 9: lBw.Content = "300 " + kHz; break; //кГц"; 
                    }
                }));

                Bandwidth = parameter;
                BandwidthOnChanged = parameter;

                comboBoxBW.Dispatcher.Invoke((delegate ()
                {
                    if (zapolneno == true) comboBoxBW.SelectedIndex = parameter;
                }));

                OnDataToSave?.Invoke(this, new DataToSaveEventArgs(Frequency, Mode, Attenuator, Bandwidth));
            }
            catch { }
        }
    }
}
