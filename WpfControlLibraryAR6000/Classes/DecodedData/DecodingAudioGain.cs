﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using AR6000;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void DecodedAudioGain() 
        {
            SAudioGain.Dispatcher.Invoke((delegate ()
            {
                SAudioGain.Value = AudioGain;
            }));
        }
        private void SAudioGain_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try 
            {
                if (Ar6000Manager.port != null && Ar6000Manager.port.IsOpen)
                {
                    if (SAudioGain.Value != 0)
                    {
                        bMute.IsChecked = false;
                    }
                    else
                    {
                        bMute.IsChecked = true;
                    }
                    AR6000DLL?.AudioGainSet(SAudioGain.Value);
                }
            }
            catch { }
        }

        private void SAudioGain_MouseMove(object sender, MouseEventArgs e)
        {

        }
    }
}
