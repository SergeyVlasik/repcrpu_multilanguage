﻿using System;
using System.Windows;
using System.Windows.Controls;


namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void AR6000DLL_OnSendAutomaticGainControl(int parameter)
        {
            DispatchIfNecessary(() =>
            {
                comboBoxAGC.SelectedIndex = parameter;
                sliderManualGain.IsEnabled = parameter == 3;
            });
        }

        private void AR6000DLL_OnSendRFGain(int parameter)
        {
            sliderManualGain.Dispatcher.Invoke((delegate ()
            {
                sliderManualGain.Value = parameter;
            }));
        }

        private void comboBoxAGC_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                switch (comboBoxAGC.SelectedIndex)
                {
                    case 0:
                        _ = AR6000DLL.AutomaticGainControlSet("0");
                        sliderManualGain.IsEnabled = false;
                        break;
                    case 1:
                        _ = AR6000DLL.AutomaticGainControlSet("1");
                        sliderManualGain.IsEnabled = false;
                        break;
                    case 2:
                        _ = AR6000DLL.AutomaticGainControlSet("2");
                        sliderManualGain.IsEnabled = false;
                        break;
                    case 3:
                        _ = AR6000DLL.AutomaticGainControlSet("F");
                        sliderManualGain.IsEnabled = true;
                        break;
                    default:
                        _ = AR6000DLL.AutomaticGainControlSet("0");
                        sliderManualGain.IsEnabled = false;
                        break;
                }
            }
            catch { }
        }

        private void sliderManualGain_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                _ = AR6000DLL.RFGainSet(int.Parse(sliderManualGain.Value.ToString()));
            }
            catch { }
        }
    }
}
