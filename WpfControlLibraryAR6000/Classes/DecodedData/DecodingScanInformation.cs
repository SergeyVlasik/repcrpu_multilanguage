﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {

        private void AR6000DLL_OnDecodedScanDelayTime()
        {
            var delay = AR6000DLL.AORstruct.ScanDelayTime;
            var pause = AR6000DLL.AORstruct.ScanPauseTime;

            if (pause != null) 
            {
                OnScanInformation(this, new ScanInformationEventArgs(pause, delay));
            }
            else { OnScanInformation(this, new ScanInformationEventArgs(" ", delay)); }            
        }


        private void AR6000DLL_OnDecodedScanPauseTime()
        {
            var delay = AR6000DLL.AORstruct.ScanDelayTime;
            var pause = AR6000DLL.AORstruct.ScanPauseTime;

            if (delay != null) 
            {
                OnScanInformation(this, new ScanInformationEventArgs(pause, delay));
            }
            else { OnScanInformation(this, new ScanInformationEventArgs(pause, " ")); }
        }

        public void ScanDelayTimeSet(string data) 
        {
            AR6000DLL?.DelayTime_ScanDelaySet(data);
        }

        public void ScanPauseTimeSet(string data) 
        {
            AR6000DLL?.PauseTime_ScanPauseTimeSet(data);
        }
    }
}
