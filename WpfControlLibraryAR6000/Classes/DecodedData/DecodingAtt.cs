﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {

        private void comboBoxATT_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                switch (comboBoxATT.SelectedIndex)
                {
                    case 0: AR6000DLL.AttenuatorSet(0); break;
                    case 1: AR6000DLL.AttenuatorSet(1); break;
                    case 2: AR6000DLL.AttenuatorSet(2); break;
                    case 3: AR6000DLL.AttenuatorSet(3); break;
                    case 4: AR6000DLL.AttenuatorSet(4); break;
                    case 10: AR6000DLL.AttenuatorSet(4); break;
                    case 11: AR6000DLL.AttenuatorSet(4); break;
                    case 12: AR6000DLL.AttenuatorSet(4); break;
                    case 13: AR6000DLL.AttenuatorSet(4); break;
                    case 14: AR6000DLL.AttenuatorSet(4); break;
                }

                Thread.Sleep(20);
                AR6000DLL.AttenuatorGet();
            }
            catch { }
        }


        private void AR6000DLL_OnSendAttenuator(int parameter)
        {
            try
            {
                lAt.Dispatcher.Invoke((delegate ()
                {
                    switch (parameter)
                    {
                        case 0: lAt.Content = UsilVKL; break;// "Усил ВКЛ"; break;
                        case 1: lAt.Content = "0" + dB; break;// "Атт 0дБ"; break;
                        case 2: lAt.Content = "-10" + dB; break;// "Атт -10дБ"; break;
                        case 3: lAt.Content = "-20" + dB; break;//"Атт -20дБ"; break;
                        case 4: lAt.Content = Avto; break;// "Атт Авто"; break;
                        case 10: lAt.Content = Attt + " " + Avto; break;// "Атт Авто"; break;
                        case 11: lAt.Content = Attt + " " + Avto; break;// "Атт Авто"; break;
                        case 12: lAt.Content = Attt + " " + Avto; break; //"Атт Авто"; break;
                        case 13: lAt.Content = Attt + " " + Avto; break; //"Атт Авто"; break;
                        case 14: lAt.Content = Attt + " " + Avto; break; //"Атт Авто"; break;
                    }

                    Attenuator = parameter;
                    OnDataToSave?.Invoke(this, new DataToSaveEventArgs(Frequency, Mode, Attenuator, Bandwidth));
                }));

                comboBoxATT.Dispatcher.Invoke((delegate ()
                {
                    if (zapolneno == true) comboBoxATT.SelectedIndex = parameter;
                }));
            }
            catch { }
        }
    }
}
