﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        public void GetScanStatus() 
        {
            AR6000DLL?.ReceiveModeStatusGet();
            //AR6000DLL.FrequencyGet();
        }

        private void AR6000DLL_OnDecodedReceiveModeStatus()
        {
            DispatchIfNecessary(() => 
            {
                textBoxFrq.Text = (AR6000DLL.AORstruct.ScanFrequency / 1000000).ToString() + "," + (AR6000DLL.AORstruct.ScanFrequency % 1000000).ToString("000000") + MHz;
                FrecuencyChanged = AR6000DLL.AORstruct.ScanFrequency;
            });
            
            OnSendScanModeStatus?.Invoke(this, AR6000DLL.AORstruct.StatusReceive, AR6000DLL.AORstruct.ScanFrequency);
        }

        private void AR6000DLL_OnReceiveModeStatus(string status)
        {
            OnSendReceiveModeStatus?.Invoke(this, status);
        }
    }
}
