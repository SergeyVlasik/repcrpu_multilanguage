﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void DecodedMode() 
        {
            try 
            {
                lMode.Dispatcher.Invoke((delegate () 
                {
                    switch (Mode)
                    {
                        case 00: lMode.Content = "FM"; break;
                        case 01: lMode.Content = "FMST"; break;
                        case 02: lMode.Content = "AM"; break;
                        case 03: lMode.Content = "SAM"; break;
                        case 04: lMode.Content = "USB"; break;
                        case 05: lMode.Content = "LSB"; break;
                        case 06: lMode.Content = "CW"; break;
                        case 07: lMode.Content = "ISB"; break;
                        case 08: lMode.Content = "AIQ"; break;
                        case 21: lMode.Content = "WFM"; break;
                        case 22: lMode.Content = "WFM"; break;
                        case 23: lMode.Content = "FMST"; break;
                        case 24: lMode.Content = "NFM"; break;
                        case 25: lMode.Content = "SFM"; break;
                        case 26: lMode.Content = "WAM"; break;
                        case 27: lMode.Content = "AM"; break;
                        case 28: lMode.Content = "NAM"; break;
                        case 29: lMode.Content = "SAM"; break;
                        case 30: lMode.Content = "USB"; break;
                        case 31: lMode.Content = "LSB"; break;
                        case 32: lMode.Content = "CW1"; break;
                        case 33: lMode.Content = "CW2"; break;
                        case 34: lMode.Content = "ISB"; break;
                        case 35: lMode.Content = "AIQ"; break;
                    }
                    OnDataToSave?.Invoke(this, new DataToSaveEventArgs(Frequency, Mode, Attenuator, Bandwidth));
                }));

                comboBoxMode.Dispatcher.Invoke((delegate ()
                {
                    try
                    {
                        if (zapolneno == true)
                        {
                            comboBoxMode.SelectedIndex = Mode;
                            Thread.Sleep(20);
                        }
                    }
                    catch { }
                }));
            }
            catch { }
        }

        private void ComboBoxMode_SelectionChanged(object sender, SelectionChangedEventArgs e) 
        {
            try 
            {
                switch (comboBoxMode.SelectedIndex)
                {
                    case 0: AR6000DLL.ModeSet(00); break;
                    case 1: AR6000DLL.ModeSet(01); break;
                    case 2: AR6000DLL.ModeSet(02); break;
                    case 3: AR6000DLL.ModeSet(03); break;
                    case 4: AR6000DLL.ModeSet(04); break;
                    case 5: AR6000DLL.ModeSet(05); break;
                    case 6: AR6000DLL.ModeSet(06); break;
                    case 7: AR6000DLL.ModeSet(07); break;
                    case 8: AR6000DLL.ModeSet(08); break;
                    case 9: AR6000DLL.ModeSet(21); break;
                    case 10: AR6000DLL.ModeSet(22); break;
                    case 11: AR6000DLL.ModeSet(23); break;
                    case 12: AR6000DLL.ModeSet(24); break;
                    case 13: AR6000DLL.ModeSet(25); break;
                    case 14: AR6000DLL.ModeSet(26); break;
                    case 15: AR6000DLL.ModeSet(27); break;
                    case 16: AR6000DLL.ModeSet(28); break;
                    case 17: AR6000DLL.ModeSet(29); break;
                    case 18: AR6000DLL.ModeSet(30); break;
                    case 19: AR6000DLL.ModeSet(31); break;
                    case 20: AR6000DLL.ModeSet(32); break;
                    case 21: AR6000DLL.ModeSet(33); break;
                    case 22: AR6000DLL.ModeSet(34); break;
                    case 23: AR6000DLL.ModeSet(35); break;
                    default: AR6000DLL.ModeSet(00); break;
                }
                Thread.Sleep(20);
                AR6000DLL.ModeGet();
                AR6000DLL.BandWidthGet();
            }
            catch { }
        }


        private void AR6000DLL_OnSendMode(int parameter)
        {
            try
            {
                lMode.Dispatcher.Invoke((delegate ()
                {
                    switch (parameter)
                    {
                        case 00: lMode.Content = "FM"; break;
                        case 01: lMode.Content = "FMST"; break;
                        case 02: lMode.Content = "AM"; break;
                        case 03: lMode.Content = "SAM"; break;
                        case 04: lMode.Content = "USB"; break;
                        case 05: lMode.Content = "LSB"; break;
                        case 06: lMode.Content = "CW"; break;
                        case 07: lMode.Content = "ISB"; break;
                        case 08: lMode.Content = "AIQ"; break;
                        case 21: lMode.Content = "WFM"; break;
                        case 22: lMode.Content = "WFM"; break;
                        case 23: lMode.Content = "FMST"; break;
                        case 24: lMode.Content = "NFM"; break;
                        case 25: lMode.Content = "SFM"; break;
                        case 26: lMode.Content = "WAM"; break;
                        case 27: lMode.Content = "AM"; break;
                        case 28: lMode.Content = "NAM"; break;
                        case 29: lMode.Content = "SAM"; break;
                        case 30: lMode.Content = "USB"; break;
                        case 31: lMode.Content = "LSB"; break;
                        case 32: lMode.Content = "CW1"; break;
                        case 33: lMode.Content = "CW2"; break;
                        case 34: lMode.Content = "ISB"; break;
                        case 35: lMode.Content = "AIQ"; break;
                    }
                    OnDataToSave?.Invoke(this, new DataToSaveEventArgs(Frequency, Mode, Attenuator, Bandwidth));
                }));

                comboBoxMode.Dispatcher.Invoke((delegate ()
                {
                    try
                    {
                        if (zapolneno == true)
                        {
                            comboBoxMode.SelectedIndex = Mode;
                            Thread.Sleep(20);
                        }
                    }
                    catch { }
                }));
            }
            catch { }
        }
    }
}
