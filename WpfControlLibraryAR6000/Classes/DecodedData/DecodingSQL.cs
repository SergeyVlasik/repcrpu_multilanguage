﻿using Nito.AsyncEx;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        AsyncAutoResetEvent _asyncSemaphore = new AsyncAutoResetEvent();

        private int NoiseSQL { get; set; }
        private int OptionsNoise { get; set; }

        private bool isCmbAsk = true;
        enum OptionsSQL 
        {
            LevelSql,
            NoiseSQL
        }

        enum NoiseSQLOptions 
        {
            Off,
            On
        }

        private void DecodedNoiseSquelch()
        {
            DispatchIfNecessary(() => 
            {
                sliderSQL.Value = NoiseSquelch;
                PSqlLevel.Value = NoiseSquelch;
            });
        }

        private void AR6000DLL_OnDecodedLevelSquelch()
        {
            DispatchIfNecessary(() =>
            {
                if (isCmbAsk) 
                {
                    sliderSQL.Value = LevelSquelch * -1;
                    PSqlLevel.Value = LevelSquelch * -1;
                    OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(PSigLevel.Value, PSqlLevel.Value));
                    isCmbAsk = false;
                }
                else 
                {
                    PSqlLevel.Value = LevelSquelch * -1;
                    OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(PSigLevel.Value, PSqlLevel.Value));
                }
            });
        }

        private void DecodedNoiseSquelchOnOff()
        {
            DispatchIfNecessary(() =>
            {
                if(NoiseSquelchOnOff == 0) 
                {
                    NoiseSQL = (int)NoiseSQLOptions.Off;
                    AR6000DLL.NoiseSquelchOnOffSet(1);
                }
                else 
                {
                    NoiseSQL = (int)NoiseSQLOptions.On;
                    AR6000DLL.NoiseSquelchGet();
                }
            });
        }

        private void comboBoxSQL_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (comboBoxSQL.SelectedIndex) 
            {
                case 0:
                    OptionsNoise = (int)OptionsSQL.NoiseSQL;
                    SliderRange();
                    isCmbAsk = true;
                    AR6000DLL.NoiseSquelchGet();
                    break;
                case 1:
                    OptionsNoise = (int)OptionsSQL.LevelSql;
                    SliderRangeDb();
                    isCmbAsk = true;
                    AR6000DLL.LevelSquelchGet();
                    break;
                default:
                    OptionsNoise = (int)OptionsSQL.NoiseSQL;
                    SliderRange();
                    AR6000DLL.NoiseSquelchGet();
                    break;
            }
        }

        private void sliderSQL_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            switch (comboBoxSQL.SelectedIndex)
            {
                case 0:
                    AR6000DLL.NoiseSquelchSet(sliderSQL.Value);
                    SliderTheresold(sliderSQL.Value);
                    break;
                case 1:
                    AR6000DLL.LevelSquelchSet(sliderSQL.Value * -1);
                    AR6000DLL.LevelSquelchGet();
                    break;
                default:
                    AR6000DLL.NoiseSquelchSet(sliderSQL.Value);
                    break;
            }
        }

        private void SliderTheresold(double value) 
        {
            DispatchIfNecessary(() => 
            {
                PSqlLevel.Value = value;
            });
        }

        private void SliderRangeDb() 
        {
            sliderSQL.Minimum = -140.0;
            sliderSQL.Maximum = 0.0;
            sliderSQL.TickFrequency = 0.1;

            PSqlLevel.Minimum = -140;
            PSqlLevel.Maximum = 0;
        }

        private void SliderRange() 
        {
            sliderSQL.Minimum = 0;
            sliderSQL.Maximum = 255;
            sliderSQL.TickFrequency = 1;

            PSqlLevel.Minimum = 0;
            PSqlLevel.Maximum = 255;
        }


        private void BSQLMore_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                switch (OptionsNoise) 
                {
                    case 0:
                        if (sliderSQL.Value <= 0)
                        {
                            sliderSQL.Value++;
                        }
                        else { }
                        break;
                    case 1:
                        if (sliderSQL.Value <= 255)
                        {
                            sliderSQL.Value++;
                        }
                        else { }
                        break;
                    default:
                        break;
                }
            }
            catch { }

        }

        private void BSQLLess_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (OptionsNoise)
                {
                    case 0:
                        if (sliderSQL.Value >= -140)
                        {
                            sliderSQL.Value--;
                        }
                        else { }
                        break;
                    case 1:
                        if (sliderSQL.Value >= 0)
                        {
                            sliderSQL.Value--;
                        }
                        else { }
                        break;
                    default:
                        break;
                }
            }
            catch { }
        }
    }
}
