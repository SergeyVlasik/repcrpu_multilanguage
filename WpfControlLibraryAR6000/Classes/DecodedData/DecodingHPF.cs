﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void comboBoxHPF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (comboBoxHPF.SelectedIndex)
            {
                case 0: AR6000DLL?.HighPassFilterSet('0'); break;
                case 1: AR6000DLL.HighPassFilterSet('1'); break;
                case 2: AR6000DLL.HighPassFilterSet('2'); break;
                default: AR6000DLL.HighPassFilterSet('0'); break;
            }
            Thread.Sleep(20);
            AR6000DLL?.HighPassFilterGet();
        }
        
        private void AR6000DLL_OnSendHighPassFilter(int parameter)
        {
            comboBoxHPF.Dispatcher.Invoke((delegate ()
            {
                if (zapolneno == false) comboBoxHPF.SelectedIndex = parameter;
            }));
        }
    }
}
