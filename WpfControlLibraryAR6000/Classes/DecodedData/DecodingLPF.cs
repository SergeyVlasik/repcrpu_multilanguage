﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void comboBoxLPF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (comboBoxLPF.SelectedIndex)
            {
                case 0: AR6000DLL?.LowPassFilterSet('0'); break;
                case 1: AR6000DLL?.LowPassFilterSet('1'); break;
                case 2: AR6000DLL?.LowPassFilterSet('2'); break;
                default: AR6000DLL?.LowPassFilterSet('0'); break;
            }
            Thread.Sleep(20);
            AR6000DLL?.LowPassFilterGet();
        }


        private void AR6000DLL_OnSendLowPassFilter(int parameter)
        {
            comboBoxLPF.Dispatcher.Invoke((delegate ()
            {
                if (zapolneno == false) comboBoxLPF.SelectedIndex = parameter;
            }));
        }
    }
}
