﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfControlLibraryAR6000
{
    public partial class UserControlAR6000
    {
        private void AR6000DLL_OnSendFrequency(long frequency)
        {
            DispatchIfNecessary((() =>
            {
                textBoxFrq.Text = (frequency / 1000000).ToString() + "," + (frequency % 1000000).ToString("000000") + MHz;
                FrecuencyChanged = frequency;
                OnDataToSave?.Invoke(this, new DataToSaveEventArgs(frequency, Mode, Attenuator, Bandwidth));
            }));
        }
    }
}
