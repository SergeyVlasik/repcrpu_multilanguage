﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AR6000;

namespace WpfControlLibraryAR6000
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControlAR6000 : UserControl
    {
        #region Properies
        public Ar6000Manager AR6000DLL;

        
        #endregion

        public UserControlAR6000()
        {
            InitializeComponent();

            InitConnection();
            Language(ConvertLang(language));
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        public void ReadFrqBwPeleng()
        {
            OnFrqBwPeleng?.Invoke(this, new FreqBwEventArgs(Frequency, Bandwidth, SignalLevel));
        }

        public void FrequencyFromPanorama(double frequency) 
        {
            AR6000DLL.FrequencySet(frequency);
        }

    }
}
