﻿#pragma checksum "..\..\UserControlAR6000.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "C6BAD212465F94CB8A8D581FF5D7C6239BEF42F4E9F958F1B2858F6F686C2F38"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using Arction.Gauges;
using Arction.Gauges.Borders;
using Arction.Gauges.Dials;
using Arction.Gauges.ValueMappers;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfControlLibraryAR6000;


namespace WpfControlLibraryAR6000 {
    
    
    /// <summary>
    /// UserControlAR6000
    /// </summary>
    public partial class UserControlAR6000 : System.Windows.Controls.UserControl, System.Windows.Markup.IComponentConnector {
        
        /// <summary>
        /// GridOne Name Field
        /// </summary>
        
        #line 42 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Grid GridOne;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxMode Name Field
        /// </summary>
        
        #line 56 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxMode;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxBW Name Field
        /// </summary>
        
        #line 83 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxBW;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxATT Name Field
        /// </summary>
        
        #line 95 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxATT;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxLPF Name Field
        /// </summary>
        
        #line 103 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxLPF;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxHPF Name Field
        /// </summary>
        
        #line 109 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxHPF;
        
        #line default
        #line hidden
        
        /// <summary>
        /// Display Name Field
        /// </summary>
        
        #line 130 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.WrapPanel Display;
        
        #line default
        #line hidden
        
        /// <summary>
        /// lsiglevel Name Field
        /// </summary>
        
        #line 147 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Label lsiglevel;
        
        #line default
        #line hidden
        
        /// <summary>
        /// lAt Name Field
        /// </summary>
        
        #line 148 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Label lAt;
        
        #line default
        #line hidden
        
        /// <summary>
        /// lMode Name Field
        /// </summary>
        
        #line 149 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Label lMode;
        
        #line default
        #line hidden
        
        /// <summary>
        /// lBw Name Field
        /// </summary>
        
        #line 150 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Label lBw;
        
        #line default
        #line hidden
        
        /// <summary>
        /// textBoxFrq Name Field
        /// </summary>
        
        #line 152 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.TextBox textBoxFrq;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar PSigLevel;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ProgressBar PSqlLevel;
        
        #line default
        #line hidden
        
        /// <summary>
        /// gaugeLvl Name Field
        /// </summary>
        
        #line 164 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public Arction.Gauges.Gauge gaugeLvl;
        
        #line default
        #line hidden
        
        /// <summary>
        /// SAudioGain Name Field
        /// </summary>
        
        #line 189 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Slider SAudioGain;
        
        #line default
        #line hidden
        
        
        #line 219 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Primitives.ToggleButton bMute;
        
        #line default
        #line hidden
        
        
        #line 222 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas cd_play;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButOne Name Field
        /// </summary>
        
        #line 243 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButOne;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButTwo Name Field
        /// </summary>
        
        #line 244 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButTwo;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButThree Name Field
        /// </summary>
        
        #line 245 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButThree;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButFour Name Field
        /// </summary>
        
        #line 246 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButFour;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButFive Name Field
        /// </summary>
        
        #line 247 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButFive;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButSix Name Field
        /// </summary>
        
        #line 248 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButSix;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButSeven Name Field
        /// </summary>
        
        #line 249 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButSeven;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButEight Name Field
        /// </summary>
        
        #line 250 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButEight;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButNine Name Field
        /// </summary>
        
        #line 251 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButNine;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButZero Name Field
        /// </summary>
        
        #line 252 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButZero;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButDot Name Field
        /// </summary>
        
        #line 253 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButDot;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButEsc Name Field
        /// </summary>
        
        #line 254 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButEsc;
        
        #line default
        #line hidden
        
        /// <summary>
        /// ButEnt Name Field
        /// </summary>
        
        #line 255 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button ButEnt;
        
        #line default
        #line hidden
        
        /// <summary>
        /// butFrqMinus Name Field
        /// </summary>
        
        #line 271 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button butFrqMinus;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxFrqStep Name Field
        /// </summary>
        
        #line 272 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxFrqStep;
        
        #line default
        #line hidden
        
        /// <summary>
        /// butFrqPlus Name Field
        /// </summary>
        
        #line 286 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Button butFrqPlus;
        
        #line default
        #line hidden
        
        /// <summary>
        /// GroupUsil Name Field
        /// </summary>
        
        #line 289 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.GroupBox GroupUsil;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxAGC Name Field
        /// </summary>
        
        #line 291 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxAGC;
        
        #line default
        #line hidden
        
        /// <summary>
        /// sliderManualGain Name Field
        /// </summary>
        
        #line 297 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Slider sliderManualGain;
        
        #line default
        #line hidden
        
        /// <summary>
        /// GroupSQL Name Field
        /// </summary>
        
        #line 303 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.GroupBox GroupSQL;
        
        #line default
        #line hidden
        
        /// <summary>
        /// comboBoxSQL Name Field
        /// </summary>
        
        #line 310 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.ComboBox comboBoxSQL;
        
        #line default
        #line hidden
        
        
        #line 326 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bSQLLess;
        
        #line default
        #line hidden
        
        /// <summary>
        /// sliderSQL Name Field
        /// </summary>
        
        #line 335 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        public System.Windows.Controls.Slider sliderSQL;
        
        #line default
        #line hidden
        
        
        #line 341 "..\..\UserControlAR6000.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button bSQLMore;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfControlLibraryAR6000;component/usercontrolar6000.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\UserControlAR6000.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.GridOne = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.comboBoxMode = ((System.Windows.Controls.ComboBox)(target));
            
            #line 56 "..\..\UserControlAR6000.xaml"
            this.comboBoxMode.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.ComboBoxMode_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.comboBoxBW = ((System.Windows.Controls.ComboBox)(target));
            
            #line 83 "..\..\UserControlAR6000.xaml"
            this.comboBoxBW.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxBW_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 4:
            this.comboBoxATT = ((System.Windows.Controls.ComboBox)(target));
            
            #line 95 "..\..\UserControlAR6000.xaml"
            this.comboBoxATT.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxATT_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.comboBoxLPF = ((System.Windows.Controls.ComboBox)(target));
            
            #line 103 "..\..\UserControlAR6000.xaml"
            this.comboBoxLPF.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxLPF_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.comboBoxHPF = ((System.Windows.Controls.ComboBox)(target));
            
            #line 109 "..\..\UserControlAR6000.xaml"
            this.comboBoxHPF.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxHPF_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 7:
            this.Display = ((System.Windows.Controls.WrapPanel)(target));
            return;
            case 8:
            this.lsiglevel = ((System.Windows.Controls.Label)(target));
            return;
            case 9:
            this.lAt = ((System.Windows.Controls.Label)(target));
            return;
            case 10:
            this.lMode = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.lBw = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.textBoxFrq = ((System.Windows.Controls.TextBox)(target));
            
            #line 153 "..\..\UserControlAR6000.xaml"
            this.textBoxFrq.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.textBoxFrq_PreviewTextInput);
            
            #line default
            #line hidden
            
            #line 156 "..\..\UserControlAR6000.xaml"
            this.textBoxFrq.KeyDown += new System.Windows.Input.KeyEventHandler(this.textBoxFrq_KeyDown);
            
            #line default
            #line hidden
            
            #line 157 "..\..\UserControlAR6000.xaml"
            this.textBoxFrq.LostFocus += new System.Windows.RoutedEventHandler(this.textBoxFrq_LostFocus);
            
            #line default
            #line hidden
            
            #line 157 "..\..\UserControlAR6000.xaml"
            this.textBoxFrq.LostKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.textBoxFrq_LostKeyboardFocus);
            
            #line default
            #line hidden
            
            #line 158 "..\..\UserControlAR6000.xaml"
            this.textBoxFrq.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(this.textBoxFrq_PreviewKeyDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.PSigLevel = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 14:
            this.PSqlLevel = ((System.Windows.Controls.ProgressBar)(target));
            return;
            case 15:
            this.gaugeLvl = ((Arction.Gauges.Gauge)(target));
            return;
            case 16:
            this.SAudioGain = ((System.Windows.Controls.Slider)(target));
            
            #line 190 "..\..\UserControlAR6000.xaml"
            this.SAudioGain.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.SAudioGain_ValueChanged);
            
            #line default
            #line hidden
            
            #line 194 "..\..\UserControlAR6000.xaml"
            this.SAudioGain.MouseMove += new System.Windows.Input.MouseEventHandler(this.SAudioGain_MouseMove);
            
            #line default
            #line hidden
            return;
            case 17:
            this.bMute = ((System.Windows.Controls.Primitives.ToggleButton)(target));
            
            #line 220 "..\..\UserControlAR6000.xaml"
            this.bMute.Checked += new System.Windows.RoutedEventHandler(this.bMute_Checked);
            
            #line default
            #line hidden
            
            #line 220 "..\..\UserControlAR6000.xaml"
            this.bMute.Unchecked += new System.Windows.RoutedEventHandler(this.bMute_Unchecked);
            
            #line default
            #line hidden
            return;
            case 18:
            this.cd_play = ((System.Windows.Controls.Canvas)(target));
            return;
            case 19:
            this.ButOne = ((System.Windows.Controls.Button)(target));
            
            #line 243 "..\..\UserControlAR6000.xaml"
            this.ButOne.Click += new System.Windows.RoutedEventHandler(this.ButOne_Click);
            
            #line default
            #line hidden
            return;
            case 20:
            this.ButTwo = ((System.Windows.Controls.Button)(target));
            
            #line 244 "..\..\UserControlAR6000.xaml"
            this.ButTwo.Click += new System.Windows.RoutedEventHandler(this.ButTwo_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.ButThree = ((System.Windows.Controls.Button)(target));
            
            #line 245 "..\..\UserControlAR6000.xaml"
            this.ButThree.Click += new System.Windows.RoutedEventHandler(this.ButThree_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.ButFour = ((System.Windows.Controls.Button)(target));
            
            #line 246 "..\..\UserControlAR6000.xaml"
            this.ButFour.Click += new System.Windows.RoutedEventHandler(this.ButFour_Click);
            
            #line default
            #line hidden
            return;
            case 23:
            this.ButFive = ((System.Windows.Controls.Button)(target));
            
            #line 247 "..\..\UserControlAR6000.xaml"
            this.ButFive.Click += new System.Windows.RoutedEventHandler(this.ButFive_Click);
            
            #line default
            #line hidden
            return;
            case 24:
            this.ButSix = ((System.Windows.Controls.Button)(target));
            
            #line 248 "..\..\UserControlAR6000.xaml"
            this.ButSix.Click += new System.Windows.RoutedEventHandler(this.ButSix_Click);
            
            #line default
            #line hidden
            return;
            case 25:
            this.ButSeven = ((System.Windows.Controls.Button)(target));
            
            #line 249 "..\..\UserControlAR6000.xaml"
            this.ButSeven.Click += new System.Windows.RoutedEventHandler(this.ButSeven_Click);
            
            #line default
            #line hidden
            return;
            case 26:
            this.ButEight = ((System.Windows.Controls.Button)(target));
            
            #line 250 "..\..\UserControlAR6000.xaml"
            this.ButEight.Click += new System.Windows.RoutedEventHandler(this.ButEight_Click);
            
            #line default
            #line hidden
            return;
            case 27:
            this.ButNine = ((System.Windows.Controls.Button)(target));
            
            #line 251 "..\..\UserControlAR6000.xaml"
            this.ButNine.Click += new System.Windows.RoutedEventHandler(this.ButNine_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.ButZero = ((System.Windows.Controls.Button)(target));
            
            #line 252 "..\..\UserControlAR6000.xaml"
            this.ButZero.Click += new System.Windows.RoutedEventHandler(this.ButZero_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.ButDot = ((System.Windows.Controls.Button)(target));
            
            #line 253 "..\..\UserControlAR6000.xaml"
            this.ButDot.Click += new System.Windows.RoutedEventHandler(this.ButDot_Click);
            
            #line default
            #line hidden
            return;
            case 30:
            this.ButEsc = ((System.Windows.Controls.Button)(target));
            
            #line 254 "..\..\UserControlAR6000.xaml"
            this.ButEsc.Click += new System.Windows.RoutedEventHandler(this.ButEsc_Click);
            
            #line default
            #line hidden
            return;
            case 31:
            this.ButEnt = ((System.Windows.Controls.Button)(target));
            
            #line 255 "..\..\UserControlAR6000.xaml"
            this.ButEnt.Click += new System.Windows.RoutedEventHandler(this.ButEnt_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.butFrqMinus = ((System.Windows.Controls.Button)(target));
            
            #line 271 "..\..\UserControlAR6000.xaml"
            this.butFrqMinus.Click += new System.Windows.RoutedEventHandler(this.butFrqMinus_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.comboBoxFrqStep = ((System.Windows.Controls.ComboBox)(target));
            
            #line 272 "..\..\UserControlAR6000.xaml"
            this.comboBoxFrqStep.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxFrqStep_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 34:
            this.butFrqPlus = ((System.Windows.Controls.Button)(target));
            
            #line 286 "..\..\UserControlAR6000.xaml"
            this.butFrqPlus.Click += new System.Windows.RoutedEventHandler(this.butFrqPlus_Click);
            
            #line default
            #line hidden
            return;
            case 35:
            this.GroupUsil = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 36:
            this.comboBoxAGC = ((System.Windows.Controls.ComboBox)(target));
            
            #line 291 "..\..\UserControlAR6000.xaml"
            this.comboBoxAGC.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxAGC_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 37:
            this.sliderManualGain = ((System.Windows.Controls.Slider)(target));
            
            #line 297 "..\..\UserControlAR6000.xaml"
            this.sliderManualGain.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.sliderManualGain_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 38:
            this.GroupSQL = ((System.Windows.Controls.GroupBox)(target));
            return;
            case 39:
            this.comboBoxSQL = ((System.Windows.Controls.ComboBox)(target));
            
            #line 311 "..\..\UserControlAR6000.xaml"
            this.comboBoxSQL.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.comboBoxSQL_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 40:
            this.bSQLLess = ((System.Windows.Controls.Button)(target));
            
            #line 326 "..\..\UserControlAR6000.xaml"
            this.bSQLLess.Click += new System.Windows.RoutedEventHandler(this.BSQLLess_Click);
            
            #line default
            #line hidden
            return;
            case 41:
            this.sliderSQL = ((System.Windows.Controls.Slider)(target));
            
            #line 335 "..\..\UserControlAR6000.xaml"
            this.sliderSQL.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.sliderSQL_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 42:
            this.bSQLMore = ((System.Windows.Controls.Button)(target));
            
            #line 341 "..\..\UserControlAR6000.xaml"
            this.bSQLMore.Click += new System.Windows.RoutedEventHandler(this.BSQLMore_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

