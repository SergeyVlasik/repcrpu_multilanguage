﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryBut
{
    public class ComIsOpenEventArgs : EventArgs
    {
        public ComIsOpenEventArgs(int isOpen)
        {
            IsOpen = isOpen;
        }

        public int IsOpen { get; private set; }
    }
}
