﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // ЗАПОЛНЕНИЕ КОМБОБОКСОВ В ЗАВИСИМОСТИ ОТ DLL
        private void Zapolnenie_ComboBox()
        {
            switch (vybor_DLL)
            {
                case 0:

                    break;
                case 1:
                    comboBoxMode.SelectedIndex = 0;
                    comboBoxBW.SelectedIndex = 0;
                    comboBoxATT.SelectedIndex = 0;
                    comboBoxLPF.SelectedIndex = 0;
                    comboBoxHPF.SelectedIndex = 0;
                    comboBoxAGC.SelectedIndex = 0;
                    comboBoxSQL.SelectedIndex = 0;
                    comboBoxFrqStep.SelectedIndex = 0;                    
                    /*
                    List<string> onemode = new List<string>() { "FM", "AM", "CW", "USB", "LSB", "WFM", "NFM" };
                    comboBoxMode.ItemsSource = onemode;

                    List<string> onebw = new List<string>() {"0.5 " + kHz, "3.0 " + kHz, "6.0 " + kHz, "8.5 " + kHz,
                                                                        "16 " + kHz, "30 " + kHz, "100 " + kHz, "200 " + kHz, "300 " + kHz  };
                    comboBoxBW.ItemsSource = onebw;

                    List<string> oneatt = new List<string>() { "0 " + dB, "10 " + dB, "20 " + dB, Avto };
                    comboBoxATT.ItemsSource = oneatt;

                    List<string> onelpf = new List<string>() { "3 " + kHz, "4 " + kHz, "6 " + kHz, "12 " + kHz, Avto };
                    comboBoxLPF.ItemsSource = onelpf;

                    //List<string> onehpf = new List<string>() {"1", "2" };                    
                    //ObservableCollection<string> oneHpf = new ObservableCollection<string>() { "50 " + Hz, "200 " + Hz, "300 " + Hz, "400 " + Hz, Avto };                    
                    //onehpf = new List<string>() { "50 " + Hz, "200 " + Hz, "300 " + Hz, "400 " + Hz, Avto };
                    ViewModelComboBox viewModel = DataContext as ViewModelComboBox;
                    
                    //viewModel.AddCollectionItems("50 " + Hz, "200 " + Hz); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Добавление через ObservableCollection
                    //ObservableCollection<string> oneHpf = viewModel.CollectionItems;
                    //comboBoxHPF.ItemsSource = oneHpf;
                    comboBoxHPF.SelectedIndex = 0;

                    List<string> oneagc = new List<string>() { OneAGCOff, OneAGCFast, OneAGCSlow, OneAGCMiddle };
                    comboBoxAGC.ItemsSource = oneagc;
                    
                    List<string> onefrq = new List<string>() { };
                    comboBoxFrqStep.ItemsSource = null;
                    onefrq = new List<string>() { "1 " + Hz, "10 " + Hz, "50 " + Hz, "100 " + Hz, "500 " + Hz, "1 " + kHz, "5 " + kHz,
                                                                    "10 " + kHz, "50 " + kHz, "100 " + kHz, "500 " + kHz, "1 " + MHz, "5 " + MHz, "10 " + MHz };                    
                    comboBoxFrqStep.ItemsSource = onefrq;
                    comboBoxFrqStep.SelectedIndex = 0;
                    
                    List<string> onesql = new List<string>() { NoiseSql, LevelSql };
                    comboBoxSQL.ItemsSource = onesql;
                    //comboBoxHPF.SelectedIndex = 0;
                    */

                    //List<string> onehpf = new List<string>() {"1", "2" };                    
                    //ObservableCollection<string> oneHpf = new ObservableCollection<string>() { "50 " + Hz, "200 " + Hz, "300 " + Hz, "400 " + Hz, Avto };                    
                    //onehpf = new List<string>() { "50 " + Hz, "200 " + Hz, "300 " + Hz, "400 " + Hz, Avto };
                    /*
                    ViewModelComboBox viewModel = DataContext as ViewModelComboBox;

                    viewModel.AddCollectionItems("50 " + Hz, "200 " + Hz); //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Добавление через ObservableCollection
                    ObservableCollection<ItemsComBoxArone> oneHpf = viewModel.CollectionItems;
                    
                    //comboBoxHPF.ItemsSource = oneHpf;
                    comboBoxHPF.SelectedIndex = 0;
                    */
                    break;
                default: break;
            }
        }
    }
}
