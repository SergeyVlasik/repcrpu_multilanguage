﻿using System.Collections.ObjectModel;
using System.ComponentModel;

namespace WpfControlLibraryBut
{
    public class ViewModelComboBox : INotifyPropertyChanged
    {
        public ViewModelComboBox()
        {
            CollectionItems = new ObservableCollection<ItemsComBoxArone>();
        }

        public void AddCollectionItems(string itemName_1, string itemName_2)
        {
            ItemsComBoxArone itemsComBoxArone = new ItemsComBoxArone { FrqHz_1 = itemName_1, FrqHz_2 = itemName_2 };
            //CollectionItems.DefaultIfEmpty();
            CollectionItems.Insert(0, itemsComBoxArone);
            
            //CollectionItems.Add(itemsComBoxArone);            
        }

        private ObservableCollection<ItemsComBoxArone> collectionItems;
        public ObservableCollection<ItemsComBoxArone> CollectionItems
        {
            get { return collectionItems; }
            set
            {
                collectionItems = value;
                OnPropertyChanged("CollectionItems");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            handler(this, new PropertyChangedEventArgs(name));
        }
    }
}
