﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // НАСТРОЙКА ФИЛЬТРА НИЖНИХ ЧАСТОТ
        private void DecodedLPF()
        {
            switch (vybor_DLL)
            {
                case 0:
                    comboBoxLPF.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == false) comboBoxLPF.SelectedIndex = LowPassFilter;
                    }));
                    break;
                case 1:
                    comboBoxLPF.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == false) comboBoxLPF.SelectedIndex = LowPassFilterOne;
                    }));
                    break;
                default: break;
            }

        }

        private void comboBoxLPF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OncomboboxLPF?.Invoke();
            if (zapolneno == true) return;
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    switch (comboBoxLPF.SelectedIndex)
                    {
                        case 0: AR6000DLL.LowPassFilterSet('0'); break;
                        case 1: AR6000DLL.LowPassFilterSet('1'); break;
                        case 2: AR6000DLL.LowPassFilterSet('2'); break;
                        default: MessageBox.Show("case not worked!"); break;
                    }
                    Thread.Sleep(20);
                    AR6000DLL.LowPassFilterGet();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            switch (comboBoxLPF.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE_DLL.LowPassFiltrSet("0"); break;
                                case 1: ClassLibrary_ARONE_DLL.LowPassFiltrSet("1"); break;
                                case 2: ClassLibrary_ARONE_DLL.LowPassFiltrSet("2"); break;
                                case 3: ClassLibrary_ARONE_DLL.LowPassFiltrSet("3"); break;
                                case 4: ClassLibrary_ARONE_DLL.LowPassFiltrSet("4"); break;
                                default: MessageBox.Show("case not worked!"); break;
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL.LowPassFiltrGet();
                            break;
                        case 1:
                            switch (comboBoxLPF.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE_DLL2.LowPassFiltrSet("0"); break;
                                case 1: ClassLibrary_ARONE_DLL2.LowPassFiltrSet("1"); break;
                                case 2: ClassLibrary_ARONE_DLL2.LowPassFiltrSet("2"); break;
                                case 3: ClassLibrary_ARONE_DLL2.LowPassFiltrSet("3"); break;
                                case 4: ClassLibrary_ARONE_DLL2.LowPassFiltrSet("4"); break;
                                default: MessageBox.Show("case not worked!"); break;
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL2.LowPassFiltrGet();
                            break;
                    }
                    
                    break;
                default: break;
            }
        }

    }
}
