﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Xml;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        //Евент для оповещения всех окон приложения
        public static event EventHandler LanguageChanged;

        public static List<CultureInfo> Languages { get; } = new List<CultureInfo>();

        public  string Lang = "ru-RU";
        private  Int16 language = 1;
        public  Int16 ChangeLang
        {
            get
            {
                return language;
            }
            set
            {
                language = value;                
                Language (ConvertLang(value));                
            }
        }

        private string ConvertLang(Int16 Lan)
        {
            switch (Lan)
            {
                case 0: return "ru-RU";
                case 1: return "en-US";
                default: return "ru-RU";
            }
        }
        
        
        Dictionary<string, string> TranslateDictionary;

        public new void Language(string value)
        {
            //1. Создаём ResourceDictionary для новой культуры
            ResourceDictionary dict = new ResourceDictionary();
            switch (value)
            {
                case "Rus":
                    dict.Source = new Uri(String.Format("/WpfControlLibraryBut;component/ResourcesLang/lang.ru-RU.xaml", value), UriKind.Relative);                        
                break;
                case "Eng":
                    dict.Source = new Uri(String.Format("/WpfControlLibraryBut;component/ResourcesLang/lang.en-US.xaml", value), UriKind.Relative);                        
                    break;
                case "Azr":
                    dict.Source = new Uri(String.Format("/WpfControlLibraryBut;component/ResourcesLang/lang.az-AZ.xaml", value), UriKind.Relative);
                    break;
                default:
                    dict.Source = new Uri("/WpfControlLibraryBut;component/ResourcesLang/lang.en-US.xaml", UriKind.Relative);
                    break;
            }               

            //2. Находим старую ResourceDictionary и удаляем его и добавляем новую ResourceDictionary
            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                            where d.Source != null && d.Source.OriginalString.StartsWith("/WpfControlLibraryBut;component/ResourcesLang/lang.")
                                            select d).First();
                
            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);                    
                Resources.MergedDictionaries.Insert(ind, dict);
                
            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }
            Zapolnenie_ComboBox();
            //LoadDictionary(value); 
            LangMessage = value;
        }

        private void LoadDictionary(string value)
        {
            XmlDocument xDoc = new XmlDocument();
            if (System.IO.File.Exists("E:\\СПО\\RepCRPU — ENG\\WpfControlLibraryBut\\ResourcesLang" + "\\TranslationPropertyes"))
            {
                xDoc.Load(System.IO.Directory.GetCurrentDirectory() + "\\ResourcesLang" + "\\TranslationPropertyes.xml");
            }
            
            else
            {
                switch (value)
                {
                    case "ru-RU":
                        break;
                    case "en-US":
                        break;
                    default:
                        break;
                }
            }
            xDoc.Load("E:\\СПО\\RepCRPU — ENG\\WpfControlLibraryBut\\ResourcesLang" + "\\TranslationPropertyes.xml");

            TranslateDictionary = new Dictionary<string, string>();

            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode x2Node in xRoot.ChildNodes)
            {
                if (x2Node.NodeType == XmlNodeType.Comment)
                    continue;

                // получаем атрибут ID
                if (x2Node.Attributes.Count > 0)
                {
                    XmlNode attr = x2Node.Attributes.GetNamedItem("ID");
                    if (attr.InnerText == "FreqHz")
                    {
                        foreach (XmlNode childnode in x2Node.ChildNodes)
                        {
                            // если узел - language
                            if (childnode.Name == value.Substring(3, 2))
                            {
                                if (!TranslateDictionary.ContainsKey(attr.Value))
                                    TranslateDictionary.Add(attr.Value, childnode.InnerText);
                                FrqHz = childnode.InnerText;
                                //Zapolnenie_ComboBox();
                            }
                        }
                    }
                }
            }

            
            //FrqHz = TranslateDictionary[;
            
        }

    }
}
