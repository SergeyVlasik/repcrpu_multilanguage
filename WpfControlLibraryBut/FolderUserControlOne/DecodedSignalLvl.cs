﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        private bool isLvlRequest = false;
        CancellationTokenSource cts = new CancellationTokenSource();
        private int _CancellationTokenDelay = 100;
        public int CancellationTokenDelay
        {
            get { return _CancellationTokenDelay; }
            set
            {
                if (_CancellationTokenDelay != value)
                {
                    _CancellationTokenDelay = value;
                }
            }
        }
        private int _RequestLvlTimer = 10;
        public int RequestLvlTimer
        {
            get { return _RequestLvlTimer; }
            set
            {
                if (_RequestLvlTimer != value)
                {
                    _RequestLvlTimer = value;
                }
            }
        }

        private void TaskManager()
        {
            Task.Run(() => SignalLvlRequest());
        }

        private async void SignalLvlRequest() 
        {
            try
            {
                while (isLvlRequest)
                {
                    cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;

                    switch (IdArOne)
                    {
                        case 0:
                            ClassLibrary_ARONE_DLL.SignalLevelUnit_dBm_Get();
                            ClassLibrary_ARONE.SignalLevelGet();
                            break;
                        case 1:
                            ClassLibrary_ARONE_DLL2.SignalLevelUnit_dBm_Get();
                            ClassLibrary_ARONESecond.SignalLevelGet();
                            break;
                    }
                    

                    var T = await Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; });
                    await Task.Delay(_RequestLvlTimer);
                }
            }
            catch { }
        }


        // ПОЛУЧЕНИЕ УРОВНЯ СИГНАЛА  AR6000Manager
        private void DecodedSignalLevel()
        {

        }

        // ПОЛУЧЕНИЕ УРОВНЯ СИГНАЛА С ARONE
        private void DecodedSignalLevelArone()
        {
            PSigLevel.Dispatcher.Invoke(delegate ()
            {
                switch (IdArOne)
                {
                    case 0:
                        PSigLevel.Value = ClassLibrary_ARONE_DLL.ArOne.SignalLevel;
                        OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(PSigLevel.Value, ClassLibrary_ARONE_DLL.ArOne.LevelSquelchTreshold * 3.8, FrequencyOne));
                        break;
                    case 1:
                        PSigLevel.Value = ClassLibrary_ARONE_DLL2.ArOne.SignalLevel;
                        OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(PSigLevel.Value, ClassLibrary_ARONE_DLL2.ArOne.LevelSquelchTreshold * 3.8, FrequencyOne));
                        break;
                }

            });
        }

        private void DecodedSignalLevelUnit_dBm()
        {
            lsiglevel.Dispatcher.Invoke((delegate ()
            {
                switch (IdArOne)
                {
                    case 0:
                        lsiglevel.Content = (ClassLibrary_ARONE_DLL.ArOne.SignalLevelUnit_dBm).ToString() + dB;//" дБ";
                        break;
                    case 1:
                        lsiglevel.Content = (ClassLibrary_ARONE_DLL2.ArOne.SignalLevelUnit_dBm).ToString() + dB;//" дБ";
                        break;
                }                                                                                                             //ConnectBut.ShowRead();
            }));

            gaugeLvl.Dispatcher.Invoke((delegate ()
            {
                //Отображение уровня сигнала на метре  
                var lvl = 0;
                switch (IdArOne)
                {
                    case 0:
                        lvl = ClassLibrary_ARONE_DLL.ArOne.SignalLevelUnit_dBm;
                        break;
                    case 1:
                        lvl = ClassLibrary_ARONE_DLL2.ArOne.SignalLevelUnit_dBm;
                        break;
                }
                
                if (lvl <= 0 && lvl >= -120) { gaugeLvl.PrimaryScale.Value = lvl; }
                else
                {
                    if (lvl > 0) { gaugeLvl.PrimaryScale.Value = 0; }
                    if (lvl < -120) { gaugeLvl.PrimaryScale.Value = -120; }                    
                }                
            }));
        }

        // ТАЙМЕР ПОЛУЧЕНИЯ УРОВНЯ СИГНАЛА
        private void DispatcherTimer1_Tick(object sender, EventArgs e)
        {
            switch (vybor_DLL)
            {
                case 0:
                    AR6000DLL.SignalLevelGet();
                    break;
                case 1:
                    //ClassLibrary_ARONE_DLL.SignalLevelUnit_dBm_Get();
                    //ClassLibrary_ARONE.SignalLevelGet();
                    break;
            }

        }



    }
}
