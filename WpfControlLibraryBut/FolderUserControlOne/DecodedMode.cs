﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // ПОЛУЧЕНИЕ ВИДА МОДУЛЯЦИИ
        private void DecodedMode()
        {
            try
            {
                switch (vybor_DLL)
                {
                    case 0:
                        lMode.Dispatcher.Invoke((delegate ()
                        {
                            switch (Mode)
                            {
                                case 00: lMode.Content = "FM"; break;
                                case 01: lMode.Content = "FMST"; break;
                                case 02: lMode.Content = "AM"; break;
                                case 03: lMode.Content = "SAM"; break;
                                case 04: lMode.Content = "USB"; break;
                                case 05: lMode.Content = "LSB"; break;
                                case 06: lMode.Content = "CW"; break;
                                case 07: lMode.Content = "ISB"; break;
                                case 08: lMode.Content = "AIQ"; break;
                                case 21: lMode.Content = "WFM"; break;
                                case 22: lMode.Content = "WFM"; break;
                                case 23: lMode.Content = "FMST"; break;
                                case 24: lMode.Content = "NFM"; break;
                                case 25: lMode.Content = "SFM"; break;
                                case 26: lMode.Content = "WAM"; break;
                                case 27: lMode.Content = "AM"; break;
                                case 28: lMode.Content = "NAM"; break;
                                case 29: lMode.Content = "SAM"; break;
                                case 30: lMode.Content = "USB"; break;
                                case 31: lMode.Content = "LSB"; break;
                                case 32: lMode.Content = "CW1"; break;
                                case 33: lMode.Content = "CW2"; break;
                                case 34: lMode.Content = "ISB"; break;
                                case 35: lMode.Content = "AIQ"; break;
                            }
                        }));

                        comboBoxMode.Dispatcher.Invoke((delegate ()
                        {
                            try
                            {
                                if (zapolneno == true)
                                {
                                    comboBoxMode.SelectedIndex = Mode;
                                    Thread.Sleep(20);
                                }
                            }
                            catch { MessageBox.Show("!!!"); }
                            //ConnectBut.ShowRead();
                        }));
                        break;
                    case 1:
                        lMode.Dispatcher.Invoke((delegate ()
                        {
                            switch (ModeOne)
                            {
                                case 0: lMode.Content = "FM"; break;
                                case 1: lMode.Content = "AM"; break;
                                case 2: lMode.Content = "CW"; break;
                                case 3: lMode.Content = "USB"; break;
                                case 4: lMode.Content = "LSB"; break;
                                case 5: lMode.Content = "WFM"; break;
                                case 6: lMode.Content = "NFM"; break;
                            }
                        }));

                        comboBoxMode.Dispatcher.Invoke((delegate ()
                        {
                            try
                            {
                                if (zapolneno == true)
                                {
                                    comboBoxMode.SelectedIndex = ModeOne;
                                    Thread.Sleep(20);
                                }

                            }
                            catch { MessageBox.Show("!!!"); }
                            //ConnectBut.ShowRead();
                        }));
                        break;
                    default: break;
                }
            }
            catch { }
        }

        //ОБРАБОТЧИК КОМБОБОКСОВ
        private void comboBoxMode_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnComboboxMode?.Invoke();
            if (zapolneno == true) return;
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    switch (comboBoxMode.SelectedIndex)
                    {
                        case 0: AR6000DLL.ModeSet(00); break;
                        case 1: AR6000DLL.ModeSet(01); break;
                        case 2: AR6000DLL.ModeSet(02); break;
                        case 3: AR6000DLL.ModeSet(03); break;
                        case 4: AR6000DLL.ModeSet(04); break;
                        case 5: AR6000DLL.ModeSet(05); break;
                        case 6: AR6000DLL.ModeSet(06); break;
                        case 7: AR6000DLL.ModeSet(07); break;
                        case 8: AR6000DLL.ModeSet(08); break;
                        case 9: AR6000DLL.ModeSet(21); break;
                        case 10: AR6000DLL.ModeSet(22); break;
                        case 11: AR6000DLL.ModeSet(23); break;
                        case 12: AR6000DLL.ModeSet(24); break;
                        case 13: AR6000DLL.ModeSet(25); break;
                        case 14: AR6000DLL.ModeSet(26); break;
                        case 15: AR6000DLL.ModeSet(27); break;
                        case 16: AR6000DLL.ModeSet(28); break;
                        case 17: AR6000DLL.ModeSet(29); break;
                        case 18: AR6000DLL.ModeSet(30); break;
                        case 19: AR6000DLL.ModeSet(31); break;
                        case 20: AR6000DLL.ModeSet(32); break;
                        case 21: AR6000DLL.ModeSet(33); break;
                        case 22: AR6000DLL.ModeSet(34); break;
                        case 23: AR6000DLL.ModeSet(35); break;
                        default: MessageBox.Show("case not worked!"); break;
                    }
                    Thread.Sleep(20);
                    AR6000DLL.ModeGet();
                    AR6000DLL.BandWidthGet();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            switch (comboBoxMode.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE.ModeSet(0); break;
                                case 1: ClassLibrary_ARONE.ModeSet(1); break;
                                case 2: ClassLibrary_ARONE.ModeSet(2); break;
                                case 3: ClassLibrary_ARONE.ModeSet(3); break;
                                case 4: ClassLibrary_ARONE.ModeSet(4); break;
                                case 5: ClassLibrary_ARONE.ModeSet(5); break;
                                case 6: ClassLibrary_ARONE.ModeSet(6); break;

                                default: MessageBox.Show("case not worked!"); break;
                            }
                            //Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL.ModeGet();
                            ClassLibrary_ARONE_DLL.BandwidthGet();
                            break;
                        case 1:
                            switch (comboBoxMode.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONESecond.ModeSet(0); break;
                                case 1: ClassLibrary_ARONESecond.ModeSet(1); break;
                                case 2: ClassLibrary_ARONESecond.ModeSet(2); break;
                                case 3: ClassLibrary_ARONESecond.ModeSet(3); break;
                                case 4: ClassLibrary_ARONESecond.ModeSet(4); break;
                                case 5: ClassLibrary_ARONESecond.ModeSet(5); break;
                                case 6: ClassLibrary_ARONESecond.ModeSet(6); break;

                                default: MessageBox.Show("case not worked!"); break;
                            }
                            //Thread.Sleep(20);

                            ClassLibrary_ARONE_DLL2.ModeGet();
                            ClassLibrary_ARONE_DLL2.BandwidthGet();
                            break;
                    }
                    
                    break;
                default: break;
            }
        }
    }
}
