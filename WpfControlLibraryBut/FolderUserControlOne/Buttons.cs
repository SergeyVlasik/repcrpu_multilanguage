﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        //ОБРАБОТЧИК НАЖАТИЙ НА КНОПКИ
        private void ButOne_Click(object sender, RoutedEventArgs e)
        {
            OnButOneClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "1";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "1";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "1";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }

        }

        private void ButTwo_Click(object sender, RoutedEventArgs e)
        {
            OnButTwoClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "2";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "2";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "2";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButThree_Click(object sender, RoutedEventArgs e)
        {
            OnButThreeClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "3";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "3";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "3";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButFour_Click(object sender, RoutedEventArgs e)
        {
            OnButFourClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "4";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "4";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "4";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButFive_Click(object sender, RoutedEventArgs e)
        {
            OnButFiveClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "5";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "5";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "5";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButSix_Click(object sender, RoutedEventArgs e)
        {
            OnButSixClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "6";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "6";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "6";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButSeven_Click(object sender, RoutedEventArgs e)
        {
            OnButSevenClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "7";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "7";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "7";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButEight_Click(object sender, RoutedEventArgs e)
        {
            OnButEightClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "8";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "8";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "8";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButNine_Click(object sender, RoutedEventArgs e)
        {
            OnButNineClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot > 5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "9";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if ((DotInFrq) && (countafterdot == 0))
                {
                    FrqToAOR += "9";
                    countafterdot++;
                    textBoxFrq.Text = FrqToAOR.ToString(); // " MГц";
                    return;
                }

                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "9";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButZero_Click(object sender, RoutedEventArgs e)
        {
            OnButZeroClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (countafterdot >5) return;
                if (FrqToAOR == "")
                {
                    FrqToAOR = "0";
                    textBoxFrq.Text = FrqToAOR.ToString(); //" МГц";
                    return;
                }
                if (double.Parse(FrqToAOR) < 6000)
                {
                    if (DotInFrq) countafterdot++;
                    FrqToAOR += "0";
                    if (double.Parse(FrqToAOR) > 6000) FrqToAOR = FrqToAOR.Substring(0, FrqToAOR.Length - 1);
                }
                textBoxFrq.Text = FrqToAOR.ToString(); //MГц                
            }
            catch { }
        }

        private void ButDot_Click(object sender, RoutedEventArgs e)
        {
            OnButDotClick?.Invoke();
            try
            {
                //ConnectBut.ShowWrite();
                if (DotInFrq == true) return;
                if (countafterdot > 3) return;
                DotInFrq = true;
                countafterdot = 0;
                FrqToAOR += ",";
                textBoxFrq.Text = FrqToAOR.ToString(); // МГц";                
            }
            catch { }
        }

        private void ButEnt_Click(object sender, RoutedEventArgs e)
        {
            OnButEntClick?.Invoke();
            //ConnectBut.ShowWrite();
            double frqq;
            string messageRus = "Неверно введена частота";
            string messgeEng = "Incorrectly entered frequency";
            switch (vybor_DLL)
            {
                case 0:
                    if ((DotInFrq) && (countafterdot == 0))
                    {
                        DotInFrq = false;
                        countafterdot = 0;

                        double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                        if ((frqq > 0.09) && (frqq < 6000)) AR6000DLL.FrequencySet(frqq);
                        FrqToAOR = "";
                        textBoxFrq.Text = FrqToAOR.ToString();
                        return;
                    }

                    DotInFrq = false;
                    countafterdot = 0;

                    double.TryParse(textBoxFrq.Text, out frqq);

                    if ((frqq > 0.09) && (frqq < 6000))
                    {
                        AR6000DLL.FrequencySet(frqq);
                    }
                    else { MessageBox.Show("Неверно введена частота"); }

                    textBoxFrq.Text = FrqToAOR.ToString();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            if ((DotInFrq) && (countafterdot == 0))
                            {
                                DotInFrq = false;
                                countafterdot = 0;

                                double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                                if ((frqq > 0.09) && (frqq < 3300)) ClassLibrary_ARONE.FrequencySet(frqq);
                                FrqToAOR = "";
                                textBoxFrq.Text = FrqToAOR.ToString();
                                return;
                            }
                            break;
                        case 1:
                            if ((DotInFrq) && (countafterdot == 0))
                            {
                                DotInFrq = false;
                                countafterdot = 0;

                                double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                                if ((frqq > 0.09) && (frqq < 3300)) ClassLibrary_ARONESecond.FrequencySet(frqq);
                                FrqToAOR = "";
                                textBoxFrq.Text = FrqToAOR.ToString();
                                return;
                            }
                            break;
                    }
                    

                    DotInFrq = false;
                    countafterdot = 0;

                    double.TryParse(textBoxFrq.Text, out frqq);

                    switch (IdArOne)
                    {
                        case 0:
                            if ((frqq > 0.09) && (frqq < 3300))
                            {
                                ClassLibrary_ARONE.FrequencySet(frqq);
                            }
                            else
                            {
                                switch (langMessage)
                                {
                                    case "Rus":
                                        MessageBox.Show(messageRus);
                                        break;
                                    case "Eng":
                                        MessageBox.Show(messgeEng);
                                        break;
                                }
                            }
                            break;
                        case 1:
                            if ((frqq > 0.09) && (frqq < 3300))
                            {
                                ClassLibrary_ARONESecond.FrequencySet(frqq);
                            }
                            else
                            {
                                switch (langMessage)
                                {
                                    case "Rus":
                                        MessageBox.Show(messageRus);
                                        break;
                                    case "Eng":
                                        MessageBox.Show(messgeEng);
                                        break;
                                }
                            }
                            break;
                    }
                   
                    Thread.Sleep(20);
                    textBoxFrq.Text = FrqToAOR.ToString();
                    break;
            }
        }

        private void ButEsc_Click(object sender, RoutedEventArgs e)
        {
            OnButEscClick?.Invoke();
            DotInFrq = false;
            countafterdot = 0;
            //ConnectBut.ShowWrite();
            FrqToAOR = "";
            textBoxFrq.Text = FrqToAOR.ToString();
        }

        //ВВОД В ТЕКСТБОКС
        private void textBoxFrq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            OnTextBoxFrqPreviewTextInput?.Invoke(this, e);
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
               && (!textBoxFrq.Text.Contains(",")
               && textBoxFrq.Text.Length != 0)))
            {
                e.Handled = true;
                //Keyboard.ClearFocus();
            }
        }

        private void textBoxFrq_KeyDown(object sender, KeyEventArgs e)
        {
            OnTextBoxFrqKeyDown?.Invoke(this, e);
            //OnButEntClick?.Invoke();
            double frqq;
            string messageRus = "Неверно введена частота";
            string messgeEng = "Incorrectly entered frequency";
            if (e.Key == Key.Enter)
            {
                switch (IdArOne)
                {
                    case 0:
                        if ((DotInFrq) && (countafterdot == 0))
                        {
                            DotInFrq = false;
                            countafterdot = 0;

                            double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                            if ((frqq > 0.09) && (frqq < 3300)) ClassLibrary_ARONE.FrequencySet(frqq);
                            FrqToAOR = "";
                            textBoxFrq.Text = FrqToAOR.ToString();
                            return;
                        }

                        DotInFrq = false;
                        countafterdot = 0;

                        double.TryParse(textBoxFrq.Text, out frqq);

                        if ((frqq > 0.09) && (frqq < 3300))
                        {
                            ClassLibrary_ARONE.FrequencySet(frqq);
                        }
                        else
                        {
                            switch (langMessage)
                            {
                                case "Rus":
                                    MessageBox.Show(messageRus);
                                    break;
                                case "Eng":
                                    MessageBox.Show(messgeEng);
                                    break;
                            }
                        }

                        break;
                    case 1:
                        if ((DotInFrq) && (countafterdot == 0))
                        {
                            DotInFrq = false;
                            countafterdot = 0;

                            double.TryParse(textBoxFrq.Text.Substring(0, textBoxFrq.Text.Length - 1), out frqq);
                            if ((frqq > 0.09) && (frqq < 3300)) ClassLibrary_ARONESecond.FrequencySet(frqq);
                            FrqToAOR = "";
                            textBoxFrq.Text = FrqToAOR.ToString();
                            return;
                        }

                        DotInFrq = false;
                        countafterdot = 0;

                        double.TryParse(textBoxFrq.Text, out frqq);

                        if ((frqq > 0.09) && (frqq < 3300))
                        {
                            ClassLibrary_ARONESecond.FrequencySet(frqq);
                        }
                        else
                        {
                            switch (langMessage)
                            {
                                case "Rus":
                                    MessageBox.Show(messageRus);
                                    break;
                                case "Eng":
                                    MessageBox.Show(messgeEng);
                                    break;
                            }
                        }

                        break;
                }
                
                textBoxFrq.Text = FrqToAOR.ToString();
                OnButEntClick?.Invoke();
                Keyboard.ClearFocus();
            }
        }
    }
}
