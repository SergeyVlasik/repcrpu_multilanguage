﻿using System;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        #region AR6000Manager
        //------------------------------Свойства  из AR6000DLL     
        public Int64 Frequency
        {
            get;
            set;
        }
        public int SignalLevel
        {
            get { return AR6000DLL.AORstruct.SignalLevel; }
            set { AR6000DLL.AORstruct.SignalLevel = value; }
        }
        public int Mode
        {
            get { return AR6000DLL.AORstruct.Mode; }
            set { AR6000DLL.AORstruct.Mode = value; }
        }
        public int Bandwidth
        {
            get { return AR6000DLL.AORstruct.Bandwidth; }
            set { AR6000DLL.AORstruct.Bandwidth = value; }
        }
        public int Attenuator
        {
            get { return AR6000DLL.AORstruct.Attenuator; }
            set { }
        }
        public int HighPassFilter
        {
            get { return AR6000DLL.AORstruct.HighPassFilter; }
            set { }
        }
        public int LowPassFilter
        {
            get { return AR6000DLL.AORstruct.LowPassFilter; }
            set { }
        }
        public int AGC
        {
            get { return AR6000DLL.AORstruct.AGC; }
            set { }
        }
        public int NoiseSquelch
        {
            get { return AR6000DLL.AORstruct.NoiseSquelch; }
            set { }
        }
        public int NoiseSquelchOnOff
        {
            get { return AR6000DLL.AORstruct.NoiseSquelchOnOff; }
            set { }
        }
        public int AudioGain
        {
            get { return AR6000DLL.AORstruct.AudioGain; }
            set { }
        }
        #endregion

        #region ArOne
        //------------------------------Свойства  из ArOne
        public Int64 FrequencyOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0: 
                        return ClassLibrary_ARONE_DLL.ArOne.frequency;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.frequency;                        
                }
                return FrequencyOne;
            }
            set { }
        }
        public Int64 FrequencyOneChanged
        {
            get => FrequencyOne;
            set
            {
                switch (IdArOne)
                {
                    case 0:
                         ClassLibrary_ARONE_DLL.ArOne.frequency = value;
                        break;
                    case 1:
                        ClassLibrary_ARONE_DLL2.ArOne.frequency = value;
                        break;
                }
                OnFreqChanged(value, DecodeBwPanoram(BandwidthOnChanged));
            }
        }
        //public int SignalLevelOne
        //{
        //    get => ObjectReceiver.ArOne.SignalLevel;
        //    set { }
        //}
        public int SignalLvlDbm
        {
            get 
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.SignalLevelUnit_dBm;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.SignalLevelUnit_dBm;
                }
                return SignalLvlDbm;
            }
            set { }
        }
        public int ModeOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.Mode;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.Mode;
                }
                return ModeOne;
            }
            set { }
        }
        public Int16 BandwidthOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.Bandwidth;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.Bandwidth;
                }
                return BandwidthOne;
            }
            set { }
        }
        public Int16 BandwidthOnChanged
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.Bandwidth;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.Bandwidth;
                }
                return BandwidthOne;
            }
            set
            {
                switch (IdArOne)
                {
                    case 0:
                        ClassLibrary_ARONE_DLL.ArOne.Bandwidth = value;
                        break;
                    case 1:
                        ClassLibrary_ARONE_DLL2.ArOne.Bandwidth = value;
                        break;
                }
                OnFreqChanged(FrequencyOneChanged, DecodeBwPanoram(BandwidthOnChanged));
            }
        }
        public int HighPassFilterOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.HighPassFilter;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.HighPassFilter;
                }
                return HighPassFilterOne;
            }
            set { }
        }
        public int LowPassFilterOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.LowPassFilter;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.LowPassFilter;
                }
                return LowPassFilterOne;
            }
            set { }
        }
        public int AttenuatorOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.Attenuator;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.Attenuator;
                }
                return AttenuatorOne;
            }
            set { }
        }
        public int AGCOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.AGC;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.AGC;
                }
                return AGCOne;
            }
            set { }
        }
        public int AFGainOne
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.AFGain;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.AFGain;
                }
                return AFGainOne;
            }
            set { }
        }
        public int SqlSelect
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.SquelchSelect;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.SquelchSelect;
                }
                return SqlSelect;
            }
            set { }
        }
        public double NoiseSquelchTreshold
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.NoiseSquelchTreshold;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.NoiseSquelchTreshold;
                }
                return NoiseSquelchTreshold;
            }
            set { }
        }
        public double LevelSquelchTreshold
        {
            get
            {
                switch (IdArOne)
                {
                    case 0:
                        return ClassLibrary_ARONE_DLL.ArOne.LevelSquelchTreshold;
                    case 1:
                        return ClassLibrary_ARONE_DLL2.ArOne.LevelSquelchTreshold;
                }
                return LevelSquelchTreshold;
            }
            set { }
        }

        #endregion

        string langMessage = "Rus";
        public string LangMessage
        {
            get { return langMessage; }
            set { langMessage = value; }
        }
    }
}
