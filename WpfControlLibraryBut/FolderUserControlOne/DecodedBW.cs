﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // ПОЛУЧЕНИЕ ПОЛОСЫ
        private void DecodedBW()
        {
            try
            {
                switch (vybor_DLL)
                {
                    case 0:
                        lBw.Dispatcher.Invoke((delegate ()
                        {
                            switch (Bandwidth)
                            {
                                case 0: lBw.Content = "0.2 " + kHz; break;// кГц"; 
                                case 1: lBw.Content = "0.5 " + kHz; break; //кГц"; 
                                case 2: lBw.Content = "1 " + kHz; break; //кГц"; 
                                case 3: lBw.Content = "3.0 " + kHz; break; //кГц";
                                case 4: lBw.Content = "6.0 " + kHz; break; //кГц";
                                case 5: lBw.Content = "15 " + kHz; break; //кГц"; 
                                case 6: lBw.Content = "30 " + kHz; break; //кГц"; 
                                case 7: lBw.Content = "100 " + kHz; break; //кГц"; 
                                case 8: lBw.Content = "200 " + kHz; break; //кГц"; 
                                case 9: lBw.Content = "300 " + kHz; break; //кГц"; 
                            }
                        }));

                        comboBoxBW.Dispatcher.Invoke((delegate ()
                        {
                            if (zapolneno == true) comboBoxBW.SelectedIndex = Bandwidth;
                        }));
                        break;
                    case 1:
                        lBw.Dispatcher.Invoke((delegate ()
                        {
                            switch (BandwidthOne)
                            {
                                case 0: lBw.Content = "0.5 " + kHz; break;// кГц"                                
                                case 1: lBw.Content = "3.0 " + kHz; break; //кГц";
                                case 2: lBw.Content = "6.0 " + kHz; break; //кГц";
                                case 3: lBw.Content = "8.5 " + kHz; ; break;
                                case 4: lBw.Content = "16 " + kHz; ; break;
                                case 5: lBw.Content = "30 " + kHz; break; //кГц"; 
                                case 6: lBw.Content = "100 " + kHz; break; //кГц"; 
                                case 7: lBw.Content = "200 " + kHz; break; //кГц"; 
                                case 8: lBw.Content = "300 " + kHz; break; //кГц"; 
                            }
                        }));

                        BandwidthOnChanged = BandwidthOne;

                        comboBoxBW.Dispatcher.Invoke((delegate ()
                        {
                            if (zapolneno == true)
                            {
                                comboBoxBW.SelectedIndex = BandwidthOne;
                                Thread.Sleep(20);
                            }
                        }));
                        break;
                    default: break;
                }
            }
            catch { }

        }

        private void comboBoxBW_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnComboboxBW?.Invoke();
            if (zapolneno == true) return;
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    switch (comboBoxBW.SelectedIndex)
                    {
                        case 0: AR6000DLL.BandWidthSet(0); break;
                        case 1: AR6000DLL.BandWidthSet(1); break;
                        case 2: AR6000DLL.BandWidthSet(2); break;
                        case 3: AR6000DLL.BandWidthSet(3); break;
                        case 4: AR6000DLL.BandWidthSet(4); break;
                        case 5: AR6000DLL.BandWidthSet(5); break;
                        case 6: AR6000DLL.BandWidthSet(6); break;
                        case 7: AR6000DLL.BandWidthSet(7); break;
                        case 8: AR6000DLL.BandWidthSet(8); break;
                        case 9: AR6000DLL.BandWidthSet(9); break;
                        default: break;
                    }
                    Thread.Sleep(20);
                    AR6000DLL.BandWidthGet();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            switch (comboBoxBW.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE.BandWidthSet(0); break;
                                case 1: ClassLibrary_ARONE.BandWidthSet(1); break;
                                case 2: ClassLibrary_ARONE.BandWidthSet(2); break;
                                case 3: ClassLibrary_ARONE.BandWidthSet(3); break;
                                case 4: ClassLibrary_ARONE.BandWidthSet(4); break;
                                case 5: ClassLibrary_ARONE.BandWidthSet(5); break;
                                case 6: ClassLibrary_ARONE.BandWidthSet(6); break;
                                case 7: ClassLibrary_ARONE.BandWidthSet(7); break;
                                case 8: ClassLibrary_ARONE.BandWidthSet(8); break;
                                default: break;
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL.BandwidthGet();
                            break;
                        case 1:
                            switch (comboBoxBW.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONESecond.BandWidthSet(0); break;
                                case 1: ClassLibrary_ARONESecond.BandWidthSet(1); break;
                                case 2: ClassLibrary_ARONESecond.BandWidthSet(2); break;
                                case 3: ClassLibrary_ARONESecond.BandWidthSet(3); break;
                                case 4: ClassLibrary_ARONESecond.BandWidthSet(4); break;
                                case 5: ClassLibrary_ARONESecond.BandWidthSet(5); break;
                                case 6: ClassLibrary_ARONESecond.BandWidthSet(6); break;
                                case 7: ClassLibrary_ARONESecond.BandWidthSet(7); break;
                                case 8: ClassLibrary_ARONESecond.BandWidthSet(8); break;
                                default: break;
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL2.BandwidthGet();
                            break;
                    }
                    
                    break;
                default: break;
            }
        }
    }
}
