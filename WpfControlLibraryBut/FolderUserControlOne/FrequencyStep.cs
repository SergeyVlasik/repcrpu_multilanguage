﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        //ПЕРЕСТРОЙКА С ЗАДАННЫМ ШАГОМ ПО ЧАСТОТЕ
        private void comboBoxFrqStep_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnComboBoxFrqStep?.Invoke();
            switch (vybor_DLL)
            {
                case 0:
                    switch ((int)comboBoxFrqStep.SelectedIndex)
                    {
                        case 0: step_frq_MHz = 1f; break;// / 1000000f; break;
                        case 1: step_frq_MHz = 10f; break;// / 1000000f; break;
                        case 2: step_frq_MHz = 50f; break;// / 1000000f; break;
                        case 3: step_frq_MHz = 100f; break;// / 1000000f; break;
                        case 4: step_frq_MHz = 500f; break;// / 1000000f; break;
                        case 5: step_frq_MHz = 1000f; break;// / 1000000f; break;
                        case 6: step_frq_MHz = 5000f; break;// / 1000000f; break;
                        case 7: step_frq_MHz = 10000f; break;// / 1000000f; break;
                        case 8: step_frq_MHz = 50000f; break;// / 1000000f; break;
                        case 9: step_frq_MHz = 100000f; break;// / 1000000f; break;
                        case 10: step_frq_MHz = 500000f; break;// / 1000000f; break;
                        case 11: step_frq_MHz = 1000000f; break;// / 1000000f; break;
                        default: MessageBox.Show("case not worked!"); break;
                    }
                    break;
                case 1:
                    switch ((int)comboBoxFrqStep.SelectedIndex)
                    {
                        case 0: step_frq_MHz = 1f; break;// / 1000000f; break;
                        case 1: step_frq_MHz = 10f; break;// / 1000000f; break;
                        case 2: step_frq_MHz = 50f; break;// / 1000000f; break;
                        case 3: step_frq_MHz = 100f; break;// / 1000000f; break;
                        case 4: step_frq_MHz = 500f; break;// / 1000000f; break;
                        case 5: step_frq_MHz = 1000f; break;// / 1000000f; break;
                        case 6: step_frq_MHz = 5000f; break;// / 1000000f; break;
                        case 7: step_frq_MHz = 10000f; break;// / 1000000f; break;
                        case 8: step_frq_MHz = 50000f; break;// / 1000000f; break;
                        case 9: step_frq_MHz = 100000f; break;// / 1000000f; break;
                        case 10: step_frq_MHz = 500000f; break;// / 1000000f; break;
                        case 11: step_frq_MHz = 1000000f; break;// / 1000000f; break;
                        case 12: step_frq_MHz = 5000000f; break;// / 1000000f; break;
                        case 13: step_frq_MHz = 10000000f; break;// / 1000000f; break;
                        default: MessageBox.Show("case not worked!"); break;
                    }
                    break;
                default: break;
            }
        }

        private void butFrqMinus_Click(object sender, RoutedEventArgs e)
        {
            OnButFrqMinus?.Invoke();
            //ConnectBut.ShowWrite();
            double myfrq;
            switch (vybor_DLL)
            {
                case 0:
                    myfrq = double.Parse(Frequency.ToString());
                    myfrq = myfrq - step_frq_MHz;
                    if (myfrq > 30000)
                    {
                        AR6000DLL.FrequencySet(myfrq / 1000000);
                    }
                    else { };//MessageBox.Show("Не может быть меньше!"); }
                    break;
                case 1:
                    myfrq = double.Parse(FrequencyOne.ToString());
                    myfrq = myfrq - step_frq_MHz;
                    switch (IdArOne)
                    {
                        case 0:
                            if (myfrq > 30000)
                            {
                                ClassLibrary_ARONE.FrequencySet(myfrq / 1000000);
                            }
                            else { };//MessageBox.Show("Не может быть меньше!"); }
                            break;
                        case 1:
                            if (myfrq > 30000)
                            {
                                ClassLibrary_ARONESecond.FrequencySet(myfrq / 1000000);
                            }
                            else { };//MessageBox.Show("Не может быть меньше!"); }
                            break;
                    }
                   
                    break;
                default: break;
            }
        }

        private void butFrqPlus_Click(object sender, RoutedEventArgs e)
        {
            OnButFrqPlus?.Invoke();
            //ConnectBut.ShowWrite();
            double myfrq;
            switch (vybor_DLL)
            {
                case 0:
                    myfrq = double.Parse(Frequency.ToString());
                    myfrq = myfrq + step_frq_MHz;
                    if (myfrq < 6000000000)
                    {
                        AR6000DLL.FrequencySet(myfrq / 1000000);
                    }
                    else { };//MessageBox.Show("Не может быть больше!"); }
                    break;
                case 1:
                    myfrq = double.Parse(FrequencyOne.ToString());
                    myfrq = myfrq + step_frq_MHz;
                    switch (IdArOne)
                    {
                        case 0:
                            if (myfrq < 3300000000)
                            {
                                ClassLibrary_ARONE.FrequencySet(myfrq / 1000000);
                                Thread.Sleep(20);
                            }
                            else { };//MessageBox.Show("Не может быть больше!"); }
                            break;
                        case 1:
                            if (myfrq < 3300000000)
                            {
                                ClassLibrary_ARONESecond.FrequencySet(myfrq / 1000000);
                                Thread.Sleep(20);
                            }
                            else { };//MessageBox.Show("Не может быть больше!"); }
                            break;
                    }
                    
                    break;
                default: break;
            }
        }

    }
}
