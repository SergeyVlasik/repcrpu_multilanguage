﻿using System;
using System.Windows.Controls;
using System.Windows.Input;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        #region delegate
        public delegate void MyControlEventHandler();
        public event MyControlEventHandler OnButOneClick;
        public event MyControlEventHandler OnButTwoClick;
        public event MyControlEventHandler OnButThreeClick;
        public event MyControlEventHandler OnButFourClick;
        public event MyControlEventHandler OnButFiveClick;
        public event MyControlEventHandler OnButSixClick;
        public event MyControlEventHandler OnButSevenClick;
        public event MyControlEventHandler OnButEightClick;
        public event MyControlEventHandler OnButNineClick;
        public event MyControlEventHandler OnButZeroClick;
        public event MyControlEventHandler OnButDotClick;
        public event MyControlEventHandler OnButEntClick;
        public event MyControlEventHandler OnButEscClick;
        public event MyControlEventHandler OnButFrqMinus;
        public event MyControlEventHandler OnButFrqPlus;
        public event MyControlEventHandler OnTextBoxFrqLostFocus;
        public event MyControlEventHandler OnWriteFrequency;

        public delegate void SelectionChangedEventHandler();
        public static event SelectionChangedEventHandler OnComboboxMode;
        public event SelectionChangedEventHandler OnComboboxBW;
        public event SelectionChangedEventHandler OnComboboxATT;
        public event SelectionChangedEventHandler OncomboboxLPF;
        public event SelectionChangedEventHandler OnComboboxHPF;
        public event SelectionChangedEventHandler OnComboBoxFrqStep;
        public event SelectionChangedEventHandler OnComboBoxAGC;
        public event SelectionChangedEventHandler OnComboBoxSQL;

        public delegate void PropertyChangedEventHandler();
        public event PropertyChangedEventHandler OnSliderManualGain;
        public event PropertyChangedEventHandler OnSliderSQL;
        public event PropertyChangedEventHandler OnSAudioGain;

        public delegate void TextChangedEventHandler(object sender, TextCompositionEventArgs e);
        public event TextChangedEventHandler OnTextBoxFrqPreviewTextInput;

        public delegate void KeyEventHandler(object sender, KeyEventArgs e);
        public event KeyEventHandler OnTextBoxFrqKeyDown;

        public delegate void KeyboardEventHandler(object sender, KeyboardFocusChangedEventArgs e);
        public event KeyboardEventHandler OnTextBoxFrqKeyboardEventHandler;

        public delegate void MouseEventHandler(object sender, MouseEventArgs e);
        public event MouseEventHandler OnSAudioGainMouseMove;
        #endregion

        public event EventHandler<FrequencyEventArgs> OnReadFreq;  //Передача частоты в UserControlGraph
        public event EventHandler<ComIsOpenEventArgs> OnComIsOpen;
        public event EventHandler<FreqBwEventArgs> OnFrqBwPeleng;

        public delegate void ChangedFrequencyPanorama (object sender, Int64 ChangedFreq, double ChangedBw);
        public event ChangedFrequencyPanorama FrequencyChanged;

        public event EventHandler<LevelOfSignalsEventArgs> OnLvlSignal;

        public delegate void OnSendNumberOfArOne(int number);
        public event OnSendNumberOfArOne OnSendNumberArOne;
    }
}
