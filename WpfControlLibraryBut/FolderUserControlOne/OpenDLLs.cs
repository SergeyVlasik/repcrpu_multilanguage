﻿using System.Windows.Controls;
using AR6000;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        private void OpenARDll()
        {
            AR6000DLL = new Ar6000Manager();
            AR6000DLL.OnConnectPort += new Ar6000Manager.ConnectEventHandler(ConnectPort);
            AR6000DLL.OnDisconnectPort += new Ar6000Manager.ConnectEventHandler(DisconnectPort);
            AR6000DLL.OnDecodedFrq += new Ar6000Manager.ByteEventHandler(DecodedFrq);
            AR6000DLL.OnDecodedSignalLevel += new Ar6000Manager.ByteEventHandler(DecodedSignalLevel);
            AR6000DLL.OnDecodedMode += new Ar6000Manager.ByteEventHandler(DecodedMode);
            AR6000DLL.OnDecodedBW += new Ar6000Manager.ByteEventHandler(DecodedBW);
            AR6000DLL.OnDecodedATT += new Ar6000Manager.ByteEventHandler(DecodedATT);
            AR6000DLL.OnDecodedLPF += new Ar6000Manager.ByteEventHandler(DecodedLPF);
            AR6000DLL.OnDecodedHPF += new Ar6000Manager.ByteEventHandler(DecodedHPF);
            AR6000DLL.OnDecodedAGC += new Ar6000Manager.ByteEventHandler(DecodedAGC);
            AR6000DLL.OnDecodedNoiseSQuelch += new Ar6000Manager.ByteEventHandler(DecodedNoiseSquelch);
            AR6000DLL.OnDecodedNoiseSquelchOnOff += new Ar6000Manager.ByteEventHandler(DecodedNoiseSquelchOnOff);
            AR6000DLL.OnDecodedAudioGain += new Ar6000Manager.ByteEventHandler(DecodedAudioGain);
            //AR6000DLL.OnDecodedMemoryChannelDataRead += new AR6000Manager.ByteEventHandler(DecodedMemoryChannelDataRead);
            //ReceiverTableControl.OnLoad += new EventHandler(ReadMemory);
        }
        private void OpenArOneDll0()
        {
            ClassLibrary_ARONE_DLL.OnConnectPort += new ClassLibrary_ARONE.ConnectEventHandler(ConnectPort);
            ClassLibrary_ARONE_DLL.OnDisconnectPort += new ClassLibrary_ARONE.ConnectEventHandler(DisconnectPort);
            ClassLibrary_ARONE_DLL.OnDecodedFrq += new ClassLibrary_ARONE.ByteEventHandler(DecodedFrq);
            ClassLibrary_ARONE_DLL.OnDecodedSignalLevel += new ClassLibrary_ARONE.ByteEventHandler(DecodedSignalLevelArone);
            ClassLibrary_ARONE_DLL.OnDecodedMode += new ClassLibrary_ARONE.ByteEventHandler(DecodedMode);
            ClassLibrary_ARONE_DLL.OnDecodedBW += new ClassLibrary_ARONE.ByteEventHandler(DecodedBW);
            ClassLibrary_ARONE_DLL.OnDecodedATT += new ClassLibrary_ARONE.ByteEventHandler(DecodedATT);
            ClassLibrary_ARONE_DLL.OnDecodedLPF += new ClassLibrary_ARONE.ByteEventHandler(DecodedLPF);
            ClassLibrary_ARONE_DLL.OnDecodedHPF += new ClassLibrary_ARONE.ByteEventHandler(DecodedHPF);
            ClassLibrary_ARONE_DLL.OnDecodedAGC += new ClassLibrary_ARONE.ByteEventHandler(DecodedAGC);
            ClassLibrary_ARONE_DLL.OnDecodedAFGain += new ClassLibrary_ARONE.ByteEventHandler(DecodedAudioGain); //SAudioGain_OnSAudioGain
            ClassLibrary_ARONE_DLL.OnDecodedSquelchSelect += new ClassLibrary_ARONE.ByteEventHandler(DecodedSquelchSelect);
            ClassLibrary_ARONE_DLL.OnDecodedNoiseSquelchTreshold += new ClassLibrary_ARONE.ByteEventHandler(DecodedNoiseSquelchTreshold);
            ClassLibrary_ARONE_DLL.OnDecodedLevelSquelchTreshold += new ClassLibrary_ARONE.ByteEventHandler(DecodedLevelSquelchTreshold);
            ClassLibrary_ARONE_DLL.OnDecodedSignalLevelUnit_dBm += new ClassLibrary_ARONE.ByteEventHandler(DecodedSignalLevelUnit_dBm);
            ClassLibrary_ARONE_DLL.OnDecodedManualGain += new ClassLibrary_ARONE.ByteEventHandler(DecodedMaualGain);
        }

        private void OpenArOneDll1()
        {
            ClassLibrary_ARONE_DLL2.OnConnectPort += new DllSecondArOne.ClassLibrary_ARONESecond.ConnectEventHandler(ConnectPort);
            ClassLibrary_ARONE_DLL2.OnDisconnectPort += new DllSecondArOne.ClassLibrary_ARONESecond.ConnectEventHandler(DisconnectPort);
            ClassLibrary_ARONE_DLL2.OnDecodedFrq += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedFrq);
            ClassLibrary_ARONE_DLL2.OnDecodedSignalLevel += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedSignalLevelArone);
            ClassLibrary_ARONE_DLL2.OnDecodedMode += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedMode);
            ClassLibrary_ARONE_DLL2.OnDecodedBW += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedBW);
            ClassLibrary_ARONE_DLL2.OnDecodedATT += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedATT);
            ClassLibrary_ARONE_DLL2.OnDecodedLPF += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedLPF);
            ClassLibrary_ARONE_DLL2.OnDecodedHPF += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedHPF);
            ClassLibrary_ARONE_DLL2.OnDecodedAGC += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedAGC);
            ClassLibrary_ARONE_DLL2.OnDecodedAFGain += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedAudioGain); //SAudioGain_OnSAudioGain
            ClassLibrary_ARONE_DLL2.OnDecodedSquelchSelect += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedSquelchSelect);
            ClassLibrary_ARONE_DLL2.OnDecodedNoiseSquelchTreshold += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedNoiseSquelchTreshold);
            ClassLibrary_ARONE_DLL2.OnDecodedLevelSquelchTreshold += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedLevelSquelchTreshold);
            ClassLibrary_ARONE_DLL2.OnDecodedSignalLevelUnit_dBm += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedSignalLevelUnit_dBm);
            ClassLibrary_ARONE_DLL2.OnDecodedManualGain += new DllSecondArOne.ClassLibrary_ARONESecond.ByteEventHandler(DecodedMaualGain);
        }
    }
}
