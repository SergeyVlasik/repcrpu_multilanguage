﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryBut
{
    public class LevelOfSignalsEventArgs : EventArgs
    {
        public LevelOfSignalsEventArgs(double signalLvl, double sqlLvl, Int64 frq)
        {
            SignalLvl = signalLvl;
            SqlLvl = sqlLvl;
            Frequency = frq;
        }

        public double SignalLvl { get; set; }
        public double SqlLvl { get; set; }

        public Int64 Frequency { get; set; }
    }
}
