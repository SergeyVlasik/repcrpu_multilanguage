﻿using DllSecondArOne;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AR6000;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // УПРАВЛЕНИЕ УРОВНЕМ ЗВУКА
        private void DecodedAudioGain()
        {
            try
            {
                switch (vybor_DLL)
                {
                    case 0:
                        SAudioGain.Dispatcher.Invoke((delegate ()
                        {
                            if (zapolneno == false) SAudioGain.Value = AudioGain;
                        }));
                        break;
                    case 1:
                        SAudioGain.Dispatcher.Invoke((delegate ()
                        {
                            if (zapolneno == true)
                            {
                                if (AFGainOne == 0)
                                {
                                    SAudioGain.Value = AFGainOne;
                                    bMute.IsChecked = true;
                                }
                                else
                                {
                                    SAudioGain.Value = AFGainOne;
                                    bMute.IsChecked = false;
                                }
                            }
                            else { }
                        }));
                        break;
                    default: break;
                }
            }
            catch { }
        }
        //УПРАВЛЕНИЕ ЗВУКОВЫМ СЛАЙДЕРОМ
        private void SAudioGain_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            OnSAudioGain?.Invoke();
            
            try
            {
                SAudioGain_OnSAudioGain();
            }
            catch { }
        }
        private void SAudioGain_OnSAudioGain()
        {
            try
            {
                switch (vybor_DLL)
                {
                    case 0:
                        if (Ar6000Manager.port == null)
                        {

                        }
                        else
                        {
                            if (Ar6000Manager.port.IsOpen)
                            {
                                AR6000DLL?.AudioGainSet(SAudioGain.Value);
                            }
                        }
                        break;
                    case 1:
                        switch (IdArOne)
                        {
                            case 0:
                                if (ClassLibrary_ARONE.port == null)
                                {

                                }
                                else
                                {
                                    if (ClassLibrary_ARONE.port.IsOpen)
                                    {
                                        if (SAudioGain.Value != 0)
                                        {
                                            if (bMute != null)
                                                bMute.IsChecked = false;
                                        }
                                        else
                                        {
                                            if (bMute != null)
                                                bMute.IsChecked = true;
                                        }

                                        ClassLibrary_ARONE_DLL?.AFGainSet(SAudioGain.Value);
                                    }
                                }
                                break;
                            case 1:
                                if (ClassLibrary_ARONESecond.port == null)
                                {

                                }
                                else
                                {
                                    if (ClassLibrary_ARONESecond.port.IsOpen)
                                    {
                                        if (SAudioGain.Value != 0)
                                        {
                                            bMute.IsChecked = false;
                                        }
                                        else
                                        {
                                            bMute.IsChecked = true;
                                        }

                                        ClassLibrary_ARONE_DLL2?.AFGainSet(SAudioGain.Value);
                                    }
                                }
                                break;
                        }
                        
                        break;
                    default: break;
                }
            }
            catch { }
        }

        private void SAudioGain_MouseMove(object sender, MouseEventArgs e)
        {
            OnSAudioGainMouseMove?.Invoke(this, e);
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                //Connection_AOR.SAudioGain.ToolTip = volumeProcents.ToString();
                SAudioGain.Dispatcher.Invoke((delegate ()
                {
                    volumeProcents = (AudioGain * 100) / 255;
                    //Connection_AOR.SAudioGain.ToolTip = volumeProcents.ToString();
                }));

            }
        }
    }
}
