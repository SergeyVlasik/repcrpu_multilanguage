﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // УСИЛЕНИЕ
        private void DecodedAGC()
        {
            switch (vybor_DLL)
            {
                case 0:
                    comboBoxAGC.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == true) comboBoxAGC.SelectedIndex = AGC;
                    }));
                    break;
                case 1:
                    comboBoxAGC.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == false) comboBoxAGC.SelectedIndex = AGCOne;
                    }));
                    break;
                default: break;
            }
        }

        private void DecodedMaualGain()
        {
            sliderManualGain.Dispatcher.Invoke((delegate ()
            {
                switch (IdArOne)
                {
                    case 0:
                        sliderManualGain.Value = ClassLibrary_ARONE_DLL.ArOne.ManualGain;
                        break;
                    case 1:
                        sliderManualGain.Value = ClassLibrary_ARONE_DLL2.ArOne.ManualGain;
                        break;
                }
                
            }));
        }

        private void comboBoxAGC_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnComboBoxAGC?.Invoke();
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            if (comboBoxAGC.SelectedIndex == 0)
                            {
                                sliderManualGain.IsEnabled = true;
                                ClassLibrary_ARONE_DLL.AutomaticGainControlSet(0);
                            }
                            else
                            {
                                sliderManualGain.IsEnabled = false;
                                ClassLibrary_ARONE_DLL.AutomaticGainControlSet(comboBoxAGC.SelectedIndex);
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL.AutomaticGainControlGet();
                            break;
                        case 1:
                            if (comboBoxAGC.SelectedIndex == 0)
                            {
                                sliderManualGain.IsEnabled = true;
                                ClassLibrary_ARONE_DLL2.AutomaticGainControlSet(0);
                            }
                            else
                            {
                                sliderManualGain.IsEnabled = false;
                                ClassLibrary_ARONE_DLL2.AutomaticGainControlSet(comboBoxAGC.SelectedIndex);
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL2.AutomaticGainControlGet();
                            break;
                    }
                    
                    break;
                default: break;
            }
        }



        private void sliderManualGain_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            OnSliderManualGain?.Invoke();
            switch (vybor_DLL)
            {
                case 0:
                    try
                    {
                        //ConnectBut.ShowWrite();
                        AR6000DLL.RFGainSet(int.Parse(sliderManualGain.Value.ToString()));
                    }
                    catch { }
                    break;
                case 1:
                    try
                    {
                        switch (IdArOne)
                        {
                            case 0:
                                ClassLibrary_ARONE_DLL.ManualGainSet((int)sliderManualGain.Value, 0);
                                break;
                            case 1:
                                ClassLibrary_ARONE_DLL2.ManualGainSet((int)sliderManualGain.Value, 0);
                                break;
                        }
                        //ConnectBut.ShowWrite();
                        
                    }
                    catch { }
                    break;
                default: break;
            }
        }

    }
}
