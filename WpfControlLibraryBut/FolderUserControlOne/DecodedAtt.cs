﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // ПОЛУЧЕНИЕ ЗНАЧЕНИЯ АТТЕНЮАТОРА
        private void DecodedATT()
        {
            switch (vybor_DLL)
            {
                case 0:
                    lAt.Dispatcher.Invoke((delegate ()
                    {
                        switch (Attenuator)
                        {
                            case 0: lAt.Content = UsilVKL; break;// "Усил ВКЛ"; break;
                            case 1: lAt.Content = "0" + dB; break;// "Атт 0дБ"; break;
                            case 2: lAt.Content = "-10" + dB; break;// "Атт -10дБ"; break;
                            case 3: lAt.Content = "-20" + dB; break;//"Атт -20дБ"; break;
                            case 10: lAt.Content = Avto; break;// "Атт Авто"; break;
                            case 11: lAt.Content = Attt + " " + Avto; break;// "Атт Авто"; break;
                            case 12: lAt.Content = Attt + " " + Avto; break; //"Атт Авто"; break;
                            case 13: lAt.Content = Attt + " " + Avto; break; //"Атт Авто"; break;
                            case 14: lAt.Content = Attt + " " + Avto; break; //"Атт Авто"; break;
                        }
                    }));

                    comboBoxATT.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == true) comboBoxATT.SelectedIndex = Attenuator;
                    }));
                    break;
                case 1:
                    lAt.Dispatcher.Invoke((delegate ()
                    {
                        switch (AttenuatorOne)
                        {
                            case 0: lAt.Content = "0" + dB; break;// "Атт 0дБ"; break;
                            case 1: lAt.Content = "-10" + dB; break;// "Атт -10дБ"; break;
                            case 2: lAt.Content = "-20" + dB; break;//"Атт -20дБ"; break;
                            case 3: lAt.Content = Avto; break;// "Атт Авто"; break;                            
                        }
                    }));

                    comboBoxATT.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == true) comboBoxATT.SelectedIndex = AttenuatorOne;
                    }));
                    break;
                default: break;
            }

        }

        private void comboBoxATT_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnComboboxATT?.Invoke();
            if (zapolneno == true) return;
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    switch (comboBoxATT.SelectedIndex)
                    {
                        case 0: AR6000DLL.AttenuatorSet(0); break;
                        case 1: AR6000DLL.AttenuatorSet(1); break;
                        case 2: AR6000DLL.AttenuatorSet(2); break;
                        case 3: AR6000DLL.AttenuatorSet(3); break;
                        case 4: AR6000DLL.AttenuatorSet(4); break;
                        case 11: AR6000DLL.AttenuatorSet(4); break;
                        case 12: AR6000DLL.AttenuatorSet(4); break;
                        case 13: AR6000DLL.AttenuatorSet(4); break;
                        case 14: AR6000DLL.AttenuatorSet(4); break;
                    }

                    Thread.Sleep(20);
                    AR6000DLL.AttenuatorGet();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            switch (comboBoxATT.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE_DLL.AttenuatorSet(0); break;
                                case 1: ClassLibrary_ARONE_DLL.AttenuatorSet(1); break;
                                case 2: ClassLibrary_ARONE_DLL.AttenuatorSet(2); break;
                                case 3: ClassLibrary_ARONE_DLL.AttenuatorSet(3); break;
                                case 4: ClassLibrary_ARONE_DLL.AttenuatorSet(4); break;
                                case 11: ClassLibrary_ARONE_DLL.AttenuatorSet(4); break;
                                case 12: ClassLibrary_ARONE_DLL.AttenuatorSet(4); break;
                                case 13: ClassLibrary_ARONE_DLL.AttenuatorSet(4); break;
                                case 14: ClassLibrary_ARONE_DLL.AttenuatorSet(4); break;
                            }

                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL.AttenuatorGet();
                            break;
                        case 1:
                            switch (comboBoxATT.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE_DLL2.AttenuatorSet(0); break;
                                case 1: ClassLibrary_ARONE_DLL2.AttenuatorSet(1); break;
                                case 2: ClassLibrary_ARONE_DLL2.AttenuatorSet(2); break;
                                case 3: ClassLibrary_ARONE_DLL2.AttenuatorSet(3); break;
                                case 4: ClassLibrary_ARONE_DLL2.AttenuatorSet(4); break;
                                case 11: ClassLibrary_ARONE_DLL2.AttenuatorSet(4); break;
                                case 12: ClassLibrary_ARONE_DLL2.AttenuatorSet(4); break;
                                case 13: ClassLibrary_ARONE_DLL2.AttenuatorSet(4); break;
                                case 14: ClassLibrary_ARONE_DLL2.AttenuatorSet(4); break;
                            }

                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL2.AttenuatorGet();
                            break;
                    }
                    
                    break;
                default: break;
            }
        }

    }
}
