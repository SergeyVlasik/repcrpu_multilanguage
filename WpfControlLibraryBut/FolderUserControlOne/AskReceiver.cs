﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // ОПРОС ПРИЁМНИКА
        public void Ask_Arone()
        {

            switch (vybor_DLL)
            {
                case 0:
                    try
                    {

                    }
                    catch { }
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            ChangeAskedArone();
                            break;
                        case 1:
                            ChangeAskedArone2();
                            break;
                    }                   
                    break;
                default: break;
            }
        }

        private void ChangeAskedArone()
        {
            try
            {
                ClassLibrary_ARONE.FrequencyGet();
                Thread.Sleep(1000);

                ClassLibrary_ARONE_DLL.ModeGet();
                Thread.Sleep(350);

                ClassLibrary_ARONE.FreeScanGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE.DelayTime_ScanDelayGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE.SignalLevelGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.BandwidthGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.AttenuatorGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.AFGainGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.LowPassFiltrGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.HighPassFiltrGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.AutomaticGainControlGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.RFGainGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.SquelchSelectGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.SignalLevelUnit_dBm_Get();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.ManualGainSet(150, 0);
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.ManualGainGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.AmplifierSet(1);
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.RFGainSet(255);
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL.IFGainSet(255);
                Thread.Sleep(5);
            }
            catch { }
        }

        private void ChangeAskedArone2()
        {
            try
            {
                ClassLibrary_ARONESecond.FrequencyGet();
                Thread.Sleep(1000);

                ClassLibrary_ARONE_DLL2.ModeGet();
                Thread.Sleep(350);

                ClassLibrary_ARONESecond.FreeScanGet();
                Thread.Sleep(5);

                ClassLibrary_ARONESecond.DelayTime_ScanDelayGet();
                Thread.Sleep(5);

                ClassLibrary_ARONESecond.SignalLevelGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.BandwidthGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.AttenuatorGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.AFGainGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.LowPassFiltrGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.HighPassFiltrGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.AutomaticGainControlGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.RFGainGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.SquelchSelectGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.SignalLevelUnit_dBm_Get();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.ManualGainSet(150, 0);
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.ManualGainGet();
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.AmplifierSet(1);
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.RFGainSet(255);
                Thread.Sleep(5);

                ClassLibrary_ARONE_DLL2.IFGainSet(255);
                Thread.Sleep(5);
            }
            catch { }
        }
    }
}
