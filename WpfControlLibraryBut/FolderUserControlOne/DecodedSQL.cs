﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // ШУМОПОДАВЛЕНИЕ ВКЛ ВЫКЛ  ДЛЯ AR6000Manager
        private void DecodedNoiseSquelchOnOff()
        {
            comboBoxSQL.Dispatcher.Invoke((delegate ()
            {
                if (zapolneno == false) comboBoxSQL.SelectedIndex = NoiseSquelchOnOff;
            }));
        }
        //ОТОБРАЖЕНИЕ ПОРОГА СЛЫШИМОСТИ
        private void DecodedSquelchSelect()
        {
            comboBoxSQL.Dispatcher.Invoke((delegate ()
            {
                if (zapolneno == false) comboBoxSQL.SelectedIndex = SqlSelect;
            }));
        }

        //ШУМОПОДАВЛЕНИЕ ВКЛ ВЫКЛ
        private void comboBoxSQL_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //OnComboBoxSQL?.Invoke();
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    if (comboBoxSQL.SelectedIndex == 1)
                    {
                        sliderSQL.IsEnabled = true;
                        AR6000DLL.NoiseSquelchOnOffSet(1);
                    }
                    else
                    {
                        sliderSQL.IsEnabled = false;
                        AR6000DLL.NoiseSquelchOnOffSet(comboBoxSQL.SelectedIndex);
                    }
                    Thread.Sleep(20);
                    AR6000DLL.NoiseSquelchOnOffGet();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            if (comboBoxSQL.SelectedIndex == 0)
                            {
                                sliderSQL.IsEnabled = true;
                                ClassLibrary_ARONE_DLL.SquelchSelectSet(0);
                                ClassLibrary_ARONE_DLL.NoiseSQGet();
                            }
                            else
                            {
                                sliderSQL.IsEnabled = true;
                                ClassLibrary_ARONE_DLL.SquelchSelectSet(1);
                                ClassLibrary_ARONE_DLL.LevelSQGet();
                            }
                            break;
                        case 1:
                            if (comboBoxSQL.SelectedIndex == 0)
                            {
                                sliderSQL.IsEnabled = true;
                                ClassLibrary_ARONE_DLL2.SquelchSelectSet(0);
                                ClassLibrary_ARONE_DLL2.NoiseSQGet();
                            }
                            else
                            {
                                sliderSQL.IsEnabled = true;
                                ClassLibrary_ARONE_DLL2.SquelchSelectSet(1);
                                ClassLibrary_ARONE_DLL2.LevelSQGet();
                            }
                            break;
                    }
                    
                    Thread.Sleep(20);

                    break;
                default: break;
            }

        }

        // ШУМОПОДАВЛЕНИЕ Ручное ДЛЯ AR6000Manager
        private void DecodedNoiseSquelch()
        {
            sliderSQL.Dispatcher.Invoke((delegate ()
            {
                sliderSQL.Value = NoiseSquelch;
            }));
        }

        //УРОВЕНЬ ПОРОГА ДЛЯ ARONE
        private void DecodedNoiseSquelchTreshold()
        {
            sliderSQL.Dispatcher.Invoke((delegate ()
            {
                sliderSQL.Value = NoiseSquelchTreshold;
            }));
        }

        private void DecodedLevelSquelchTreshold()
        {
            sliderSQL.Dispatcher.Invoke((delegate ()
            {
                //sliderSQL.Value = LevelSquelchTreshold;
                PSqlLevel.Value = LevelSquelchTreshold * 3.8;
                OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(PSigLevel.Value, PSqlLevel.Value, FrequencyOne));
            }));
        }

        //СЛАЙДЕР ШУМОПОДАВЛЕНИЯ
        private void sliderSQL_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            OnSliderSQL?.Invoke();
            switch (vybor_DLL)
            {
                case 0:
                    try
                    {
                        //ConnectBut.ShowWrite();
                        AR6000DLL.NoiseSquelchSet(sliderSQL.Value);
                    }
                    catch { }
                    break;
                case 1:
                    try
                    {
                        switch (IdArOne)
                        {
                            case 0:
                                if (comboBoxSQL.SelectedIndex == 0)
                                {
                                    ClassLibrary_ARONE_DLL.NoiseSQSet((int)sliderSQL.Value);
                                }
                                else
                                {
                                    ClassLibrary_ARONE_DLL?.LevelSQSet((int)sliderSQL.Value);
                                }
                                break;
                            case 1:
                                if (comboBoxSQL.SelectedIndex == 0)
                                {
                                    ClassLibrary_ARONE_DLL2.NoiseSQSet((int)sliderSQL.Value);
                                }
                                else
                                {
                                    ClassLibrary_ARONE_DLL2.LevelSQSet((int)sliderSQL.Value);
                                }
                                break;
                        }
                        

                    }
                    catch { }
                    break;


            }

        }

    }
}
