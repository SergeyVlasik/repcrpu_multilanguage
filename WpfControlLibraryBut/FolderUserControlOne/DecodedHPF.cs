﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControl1 : UserControl
    {
        // НАСТРОЙКА ФИЛЬТРА ВЕРХНИХ ЧАСТОТ
        private void DecodedHPF()
        {
            switch (vybor_DLL)
            {
                case 0:
                    comboBoxHPF.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == false) comboBoxHPF.SelectedIndex = HighPassFilter;
                    }));
                    break;
                case 1:
                    comboBoxHPF.Dispatcher.Invoke((delegate ()
                    {
                        if (zapolneno == false) comboBoxHPF.SelectedIndex = HighPassFilterOne;
                    }));
                    break;
                default: break;
            }

        }

        private void comboBoxHPF_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnComboboxHPF?.Invoke();
            if (zapolneno == true) return;
            //ConnectBut.ShowWrite();
            switch (vybor_DLL)
            {
                case 0:
                    switch (comboBoxHPF.SelectedIndex)
                    {
                        case 0: AR6000DLL.HighPassFilterSet('0'); break;
                        case 1: AR6000DLL.HighPassFilterSet('1'); break;
                        case 2: AR6000DLL.HighPassFilterSet('2'); break;
                        default: MessageBox.Show("case not worked!"); break;
                    }
                    Thread.Sleep(20);
                    AR6000DLL.HighPassFilterGet();
                    break;
                case 1:
                    switch (IdArOne)
                    {
                        case 0:
                            switch (comboBoxHPF.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE_DLL.HighPassFiltrSet("0"); break;
                                case 1: ClassLibrary_ARONE_DLL.HighPassFiltrSet("1"); break;
                                case 2: ClassLibrary_ARONE_DLL.HighPassFiltrSet("2"); break;
                                case 3: ClassLibrary_ARONE_DLL.HighPassFiltrSet("3"); break;
                                case 4: ClassLibrary_ARONE_DLL.HighPassFiltrSet("4"); break;
                                default: MessageBox.Show("case not worked!"); break;
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL.HighPassFiltrGet();
                            break;
                        case 1:
                            switch (comboBoxHPF.SelectedIndex)
                            {
                                case 0: ClassLibrary_ARONE_DLL2.HighPassFiltrSet("0"); break;
                                case 1: ClassLibrary_ARONE_DLL2.HighPassFiltrSet("1"); break;
                                case 2: ClassLibrary_ARONE_DLL2.HighPassFiltrSet("2"); break;
                                case 3: ClassLibrary_ARONE_DLL2.HighPassFiltrSet("3"); break;
                                case 4: ClassLibrary_ARONE_DLL2.HighPassFiltrSet("4"); break;
                                default: MessageBox.Show("case not worked!"); break;
                            }
                            Thread.Sleep(20);
                            ClassLibrary_ARONE_DLL2.HighPassFiltrGet();
                            break;
                    }
                    
                    break;
                default: break;
            }
        }

    }
}
