﻿namespace WpfControlLibraryBut
{
    class TableArone
    {
        public TableArone(string Adress, string Frequency, string Mode, string BW, string Time, string Teg)
        {
            this.Adress = Adress;         
            this.Frequency = Frequency;            
            this.BW = BW;            
            this.Mode = Mode;            
            this.Time = Time;
            this.Teg = Teg;
        }

        public string Adress { get; set; }               
        public string Frequency { get; set; }        
        public string BW { get; set; }        
        public string Mode { get; set; }        
        public string Time { get; set; }
        public string Teg { get; set; }
    }
}
