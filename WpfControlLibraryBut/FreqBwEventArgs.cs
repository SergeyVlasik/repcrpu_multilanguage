﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryBut
{
    public class FreqBwEventArgs : EventArgs
    {
        public FreqBwEventArgs(Int64 Freq, Int16 Bw, int lvl)
        {
            Frequency = Freq;
            BW = Bw;
            Lvl = lvl;
        }

        public Int64 Frequency { get; private set; }
        public Int16 BW { get; private set; }
        public int Lvl { get; private set; }
    }
}
