﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryBut
{
    public class ToSupressEventArgs : EventArgs
    {
        public ToSupressEventArgs(Int64 Freq, double Bw, int lvl, Int16 id)
        {
            FrqSupress = Freq;
            BwSupress = Bw;
            LvlSupress = lvl;
            ID = id;
        }
        public Int64 FrqSupress { get; private set; }
        public double BwSupress { get; private set; }
        public int LvlSupress { get; private set; }
        public Int16 ID { get; private set; }
    }
}
