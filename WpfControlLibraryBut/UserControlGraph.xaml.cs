﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using MathNet.Numerics.IntegralTransforms;
using System.Numerics;
using System.IO;
using System.Threading;
using AR6000;

namespace WpfControlLibraryBut
{
    /// <summary>
    /// Логика взаимодействия для UserControlGraph.xaml
    /// </summary>
    public partial class UserControlGraph : UserControl
    {
        //WriteAndRead WriteAndRead;
        public UserControl1 UserControl1;
        public Ar6000Manager AR6000;
        public BufferedWaveProvider bwp;
        public WaveFileWriter writer;
        static double Fs = 44100;               // Частота дискретизвции 
        private int _samplingFrequency = 44100;
        private int m_iFFTWindowLength;
        private int m_iHighFreq;
        static double T = 1.0 / Fs;             // Шаг дискретизации
        static double Fn = Fs / 2;              // Частота Найквиста
        static int N = (int)Fs / 5;
        Complex max = 2400;                         //для корректировки уровня спектра
        Complex[] sig1;
        int iAverage = 0;//Длина сигнала (точек)           
        public float step_frq_MHz = 10f / 1000000f;
        string path;
        static string outputFilename;
        static string outputFn;
        public Int64 freq;

        double qwe = 0;                             //точка сигнала на графике
        double sig_sum = 0;                         //среднее за 5 отсчетов
        double K = N / 4;
        int kol_previshenij_dlia_zapisi = 10;       //колво превышений за 1 набор БПФ
        int kol_previshenij_dlia_zapisi_time_count = 5; //колво превышений подряд
        int kol_previshenij_dlia_zapisi_time_count_i = 0;
        int kol_buf_ostanovka_zapisi = 300;                  //задержка конца записи
        int PorogZapisi = -60;

        double[] SignalX;           //для точек графика БПФ 
        double[] SignalY;           //для точек графика БПФ 
        double[] PorogObnar;           //для точек порог обнаружения
        double[] SignalYAverage;           //для точек графика БПФ        
        double[] SignalYResized;
        int iNumDataMx = 0;
        int iNumDataSigma = 0;
        int count_baund = 0;
        public int delitel;
        public int resolutionX = 1000;
        public int resolutionY = 1000;
        int kol_previshenij = 0;

        double timeStepMs = 20;
        double m_dTimeRangeLengthSec;
        double freqMin = 0;
        double freqMax = Fs / 2;
        double dFFTtimeWinOffset = (double)(512 / 2) / Fs;
        double timeRangeLengthSec = 5;
        double m_dStepTime;
        double m_dCurrentTime = 0;
        int m_iSizeTimeSlots;
        int m_iSizeResolution;
        double[][] m_aFastData;

        public Int64 Frequency
        {
            get;
            set;
        }

        public string LanguageChanged { get; set; }

        public delegate void RoutedEventHandler();
        public event RoutedEventHandler OnCbWriteCheccked;
        public event RoutedEventHandler OnCbWriteUnCheccked;
        public event RoutedEventHandler OnCbPlayCloseChecked;
        public event RoutedEventHandler OnCbPlayCloseUnChecked;
        public event RoutedEventHandler OnCbAverageChecked;
        public event RoutedEventHandler OnCbAverageUnChecced;
        public event RoutedEventHandler OnCbAutoWriteChecked;
        public event RoutedEventHandler OnCbAutoWriteUnChecked;


        public UserControlGraph()
        {
            InitializeComponent();

            InitConnectionAor();

            FrqGraph.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.None;
            FrqGraph.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.None;
            FrqGraph.ViewXY.ZoomPanOptions.MouseWheelZooming = MouseWheelZooming.Off;

            intensityGraph.ViewXY.ZoomPanOptions.RightMouseButtonAction = MouseButtonAction.None;
            intensityGraph.ViewXY.ZoomPanOptions.LeftMouseButtonAction = MouseButtonAction.None;
            intensityGraph.ViewXY.ZoomPanOptions.MouseWheelZooming = MouseWheelZooming.Off;
            intensityGraph.ViewXY.YAxes[0].LabelsVisible = true;
            RecZapis.Visibility = Visibility.Collapsed;
            ColorScale();            
        }

        

        private void ColorScale()
        {                 
            UserControlGraph1.BorderBrush = Brushes.Orange;
            //spectrogram.Clear();
        }

        private void InitConnectionAor()
        {
            UserControl1 = new UserControl1();

            AR6000 = new Ar6000Manager();
            if (cbPlayClose.IsChecked == true)
                cbPlayClose.IsChecked = false;
            cbAverage.IsChecked = false;
            
            int vybordll = UserControl1.vybor_DLL;
            UserControl1.OnReadFreq += UserControl1_Moving;
                        
            sig1 = new Complex[44100];
            SignalX = new double[2205];
            SignalY = new double[2205];
            PorogObnar = new double[2205];
            SignalYAverage = new double[2205];
            m_iFFTWindowLength = 1024 * 2; //1024 * 2 //2205
            m_iHighFreq = 22050;
            _samplingFrequency = 44100;
            ConnectAudio();
            InitializeIntensity();
            //IntensityGraph1Resize();
        }

        private void UserControl1_OnChangedLanguageReceiver(object sender, string ChangedLanguage)
        {
            Dispatcher.Invoke(() => 
            {
                LanguageChanged = ChangedLanguage;
                Language(LanguageChanged);
            });            
        }

        private void UserControl1_Moving(object sender, FrequencyEventArgs e)
        {
            Frequency = e.Frequency;
        }

        private void InitializeIntensity()
        {
            int resolution = m_iFFTWindowLength;
            if (m_iHighFreq <= _samplingFrequency / 2)
            resolution = (int)Math.Round((double)m_iHighFreq / (double)(_samplingFrequency / 2) * m_iFFTWindowLength);
            //int resolution = 2205;
            intensityGraph.BeginUpdate();
            spectrogram.Clear();

            double dAxisTimeScaleMin = timeStepMs / 1000.0 - timeRangeLengthSec;
            double dAxisTimeScaleMax = 0;
            m_dStepTime = timeStepMs / 1000.0;
            m_dTimeRangeLengthSec = timeRangeLengthSec;

            var view = intensityGraph.ViewXY;
            view.XAxes[0].SetRange(freqMin, freqMax);
            view.YAxes[0].SetRange(dAxisTimeScaleMin, dAxisTimeScaleMax);

            m_iSizeTimeSlots = (int)Math.Round(timeRangeLengthSec / (timeStepMs / 1000.0));
            m_iSizeResolution = resolution;

            m_aFastData = new double[m_iSizeTimeSlots][];
            for (int iTimeSlot = 0; iTimeSlot < m_iSizeTimeSlots; iTimeSlot++)
            {
                m_aFastData[iTimeSlot] = new double[m_iSizeResolution];
            }

            spectrogram.SetValuesData(m_aFastData, IntensityGridValuesDataOrder.RowsColumns);
            //spectrogram.Data = null;
            //Graph_AOR.spectrogram.ContourLineType = ContourLineTypeXY.None;
            //Graph_AOR.spectrogram.WireframeType = SurfaceWireframeType.None;
            spectrogram.PixelRendering = true;
            //spectrogram.Fill = IntensityFillStyle.FromSurfacePoints;
            spectrogram.ContourLineStyle.AntiAliasing = LineAntialias.None;
            spectrogram.ContourLineType = ContourLineTypeXY.None;
            
            m_dCurrentTime = dAxisTimeScaleMax;

            spectrogram.SetRangesXY(view.XAxes[0].Minimum, view.XAxes[0].Maximum,
                view.YAxes[0].Minimum, view.YAxes[0].Maximum);
            intensityGraph.EndUpdate();
        }

        public WaveIn waveIn;
        public void ConnectAudio()
        {
            if (WaveIn.DeviceCount != 0)
            {
                try
                {
                    waveIn = new WaveIn();
                    waveIn.DeviceNumber = 0;
                    waveIn.WaveFormat = new WaveFormat((int)Fs, 1);  
                    waveIn.StartRecording();
                }
                catch (Exception ex) { }
            }            
        }

        public void stopRecordButton_Click(object sender, EventArgs e)
        {
            waveIn.StopRecording();
        }

        
        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {            
            OnCbWriteCheccked?.Invoke();
            if (cbWrite.IsChecked == true)
            {
                UserControl1.ReadFrq();    //Запрос Частоты                
                FileDirectoryWrite();
                writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat); //outputFilename, waveIn.WaveFormat
                RecZapis.Visibility = Visibility.Collapsed;                
            }
        }

        private void cbWrite_Unchecked(object sender, RoutedEventArgs e)
        {
            OnCbWriteUnCheccked?.Invoke();
            if (writer != null)
            {
                writer.Close();
                writer = null;
                RecZapis.Visibility = Visibility.Collapsed;
                ResampleAsync();                
            }
        }

        static private void Resample()
        {
            try
            {
                var reader = new WaveFileReader(outputFilename);
                var newFormat = new WaveFormat(8000, 8, 1);
                var convert = new WaveFormatConversionStream(newFormat, reader);
                WaveFileWriter.CreateWaveFile(outputFn + "Jamming.wav", convert);
                convert.Dispose();
                //Thread.Sleep(400);
            }
            catch { /*NAudio.Wave.Compression.AcmStreamHeader.Finalize();*/ }            
        }

        static async void ResampleAsync()
        {
            await Task.Run(() => Resample());
        }

        private void cbPlayClose_Checked(object sender, RoutedEventArgs e)
        {
            OnCbPlayCloseChecked?.Invoke();
            try
            {
                if (cbPlayClose.IsChecked == true)
                {
                    waveIn.DataAvailable += WaveIn_DataAvailable;
                    cbWrite.IsEnabled = true;  
                }
            }
            catch { }
        }

        private void cbPlayClose_Unchecked(object sender, RoutedEventArgs e)
        {
            OnCbPlayCloseUnChecked?.Invoke();
            try
            {
                if (cbPlayClose.IsChecked == false)
                {
                    waveIn.DataAvailable -= WaveIn_DataAvailable;
                    cbWrite.IsEnabled = false;
                }
            }
            catch { }
        }

        private void cbAverage_Checked(object sender, RoutedEventArgs e)
        {
            OnCbAverageChecked?.Invoke();
        }

        private void cbAverage_Unchecked(object sender, RoutedEventArgs e)
        {
            OnCbAverageUnChecced?.Invoke();
        }

        private void cbAutoWrite_Checked(object sender, RoutedEventArgs e)
        {
            OnCbAutoWriteChecked?.Invoke();
        }

        private void cbAutoWrite_Unchecked(object sender, RoutedEventArgs e)
        {
            OnCbAutoWriteUnChecked?.Invoke();
            cbWrite.IsChecked = false;
        }

        //------------------------------------------------------- ПОДКЛЮЧЕНИЕ К ПОТОКУ ЗВУКОВОЙ КАРТЫ С ПРИЁМНИКА----------------------------------------------------

        private void FileDirectoryWrite()
        {
            path = String.Format(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                "\\Wav\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + "\\" + "AR-ONE");

            if (!Directory.Exists(path))
            {
                DirectoryInfo directory = Directory.CreateDirectory(path);
            }

            outputFn = path + "\\" + Frequency.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" +
                DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00");
            outputFilename =outputFn + ".wav";
            //outputFilename = path + "\\" + Frequency.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" +
            //    DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00") + ".wav";            
        }

        public void WaveIn_DataAvailable(object sender, WaveInEventArgs e)
        {

            if (writer != null) writer.WriteData(e.Buffer, 0, e.BytesRecorded);  //запись в файл
            byte[] buffer = e.Buffer;
            int bytesRecorded = e.BytesRecorded;
            Complex[] sig = new Complex[bytesRecorded / 2];

            for (int i = 0, j = 0; i < e.BytesRecorded; i += 2, j++)
            {
                short sample = (short)((buffer[i + 1] << 8) | buffer[i + 0]);
                sig[j] = sample / 32768f;
            }

            Fourier.Inverse(sig, FourierOptions.Matlab);
            foreach (Complex c in sig)
            {
                if (max.Magnitude < c.Magnitude)
                {
                    max = c;
                }
            }

            for (int j = 0; j < bytesRecorded / 2; j++)
            {
                sig1[j + iAverage * bytesRecorded] = sig[j];
            }
            iAverage++;
            if (iAverage == 5) { iAverage = 0; }

            DrawGraphs(sig, max);

        }

        private void DrawGraphs(Complex[] sig, Complex max1)
        {
            try
            {
                if (!Dispatcher.CheckAccess())
                {
                    Dispatcher.Invoke((delegate ()
                    {
                        DrawGraphs(sig, max1);
                    }));
                    return;
                }

                SignalYResized_Clear();
                qwe = 0;                             //точка сигнала на графике
                sig_sum = 0;                         //среднее за 5 отсчетов
                //double K = sig.Length / 2;
                for (int i = 0; i < K; i++)
                {
                    qwe = (Complex.Abs(sig[i]) * 1000000 * 100) / (N * 115 * max1.Magnitude);
                    qwe = 20 * Math.Log10(qwe);
                    try { SignalY[i] = qwe; }
                    catch { }

                    PorogObnar[i] = PorogZapisi;
                    if (iNumDataMx == count_baund) { iNumDataMx = 0; }
                    if (iNumDataSigma == count_baund) { iNumDataSigma = 0; }
                }

                for (int j = 0; j < sig.Length; j++)
                {
                    sig1[j + iAverage * sig.Length] = sig[j];
                }
                iAverage++;
                if (iAverage == 5) { iAverage = 0; }
                for (int i = 0; i < K; i++)
                {
                    sig_sum = ((
                        Complex.Abs(sig1[i + sig.Length * 0]) +
                        Complex.Abs(sig1[i + sig.Length * 1]) +
                        Complex.Abs(sig1[i + sig.Length * 2]) +
                        Complex.Abs(sig1[i + sig.Length * 3]) +
                        Complex.Abs(sig1[i + sig.Length * 4])) * 1000000 * 100) / (5 * N * 115 * max1.Magnitude);
                    sig_sum = 20 * Math.Log10(sig_sum);
                    SignalYAverage[i] = sig_sum;
                    //SignalYAverage[i] = Complex.FromPolarCoordinates(Math.Sqrt(K * K + sig_sum * sig_sum), Math.Atan2(sig_sum, K));
                    //if (i > 10 && i < 1000 && sig_sum > int.Parse(textBox3.Text)) { kol_previshenij++; }    // условие записи по порогу


                    if (i > 10 && i < 1000 && sig_sum > PorogZapisi) { kol_previshenij++; }    // условие записи по порогу
                }
                int ii = 0;
                /*
                for (int i = 0; i < K; i++)
                {
                    ii = i / delitel;
                    if (ii < SignalYResized.Length)
                        if (SignalYResized[ii] < SignalY[i]) SignalYResized[ii] = SignalY[i];

                }
                */

                UpdateGraphs();
            }
            catch { }
        }

        void UpdateGraphs()
        {

            if (SignalY != null)
            {
                var _view = FrqGraph.ViewXY;
                FrqGraph.BeginUpdate();


                _view.SampleDataSeries[0].SamplingFrequency = K / Fn;
                _view.SampleDataSeries[0].InvalidateData();
                _view.SampleDataSeries[0].SamplesDouble = SignalY;
                if (cbAverage.IsChecked == true)
                {
                    _view.SampleDataSeries[1].SamplingFrequency = 1 / Fn * K;
                    _view.SampleDataSeries[1].InvalidateData();
                    _view.SampleDataSeries[1].SamplesDouble = SignalYAverage;
                    _view.SampleDataSeries[1].LineVisible = true;
                }
                else
                {
                    _view.SampleDataSeries[1].LineVisible = false;
                }
                //_view.SampleDataSeries[2].SamplingFrequency = 1 / Fn * K;
                //_view.SampleDataSeries[2].SamplesDouble = PorogObnar;


                FrqGraph.EndUpdate();
                //AutoWriteWave();

                ////////////////////////////////////////////////////////////ПОСТРОЕНИЕ СПЕКТРОГРАММЫ

                intensityGraph.BeginUpdate();

                var view = intensityGraph.ViewXY;

                int rowCount = 6;

                for (int row = 0; row < rowCount; row++)
                {
                    double[] yValues = SignalY;
                    //Only accept resolution count of data points 
                    Array.Resize(ref yValues, m_iSizeResolution);

                    //move the old time slots one step earlier
                    for (int iTimeSlot = 1; iTimeSlot < m_iSizeTimeSlots; iTimeSlot++)
                    {
                        m_aFastData[iTimeSlot - 1] = m_aFastData[iTimeSlot]; //change the reference  
                    }
                    m_aFastData[m_iSizeTimeSlots - 1] = yValues;
                }

                spectrogram.InvalidateValuesDataOnly();

                double dCurrentTimeMin = m_dCurrentTime - m_dTimeRangeLengthSec;
                double dTotalTimeShift = m_dStepTime * rowCount;

                view.YAxes[0].SetRange(dCurrentTimeMin + dTotalTimeShift, dCurrentTimeMin + dTotalTimeShift + m_dTimeRangeLengthSec);

                spectrogram.SetRangesXY(view.XAxes[0].Minimum, view.XAxes[0].Maximum,
                    view.YAxes[0].Minimum, view.YAxes[0].Maximum);

                m_dCurrentTime += dTotalTimeShift;
                //IntensityGraph1Resize();
                intensityGraph.EndUpdate();

            }

        }

        void SignalYResized_Clear()
        {
            try
            {
                for (int j = 0; j < SignalYResized.Length; j++)
                {
                    SignalYResized[j] = -150;
                }
            }
            catch (Exception) { }
        }

        private void IntensityGraph1Resize()
        {
            resolutionX = (int)intensityGraph.Width;
            resolutionY = (int)intensityGraph.Height;
            //delitel = 5;
            delitel = 2205 / resolutionX;
            if (delitel < 2) delitel = 2;
            if (delitel > 10) delitel = 10;
            //intensity = new double[2205 / delitel, WaterflowTime];
            SignalYResized = new double[2205 / delitel];
            SignalYResized_Clear();
            //Intensity_Clear();

        }

        private void DisposeFFTMonitors()
        {
            FrqGraph.ViewXY.AreaSeries[0].Clear();
        }
        /*
        private void AutoWriteWave()
        {            
            if (cbAutoWrite.IsChecked == true)
            {
                if (ObjectReceiver.ArOne.SignalLevel > ObjectReceiver.ArOne.LevelSquelchTreshold * 3.8)
                {
                    if (cbWrite.IsChecked == false)
                    {
                        cbWrite.IsChecked = true;                        //zapisyvat
                    }
                }
                else
                {
                    cbWrite.IsChecked = false;
                }
            }
        }
        */
        private void LegendBoxXY_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            
        }

        private void intensityGraph_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (LegendBoxXY.Visible == false)
            {
                LegendBoxXY.Visible = true;
            }
            else { LegendBoxXY.Visible = false; }
        }
    }
}
