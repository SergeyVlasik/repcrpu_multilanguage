﻿using DllSecondArOne;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using AR6000;


namespace WpfControlLibraryBut
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class UserControl1 : UserControl, IOptions
    {
        public Ar6000Manager AR6000DLL;
        public ClassLibrary_ARONE ClassLibrary_ARONE_DLL;

        public DllSecondArOne.ClassLibrary_ARONESecond ClassLibrary_ARONE_DLL2;

        List<ClassLibrary_ARONE> NumberArOne = new List<ClassLibrary_ARONE>();        
        
        private int idArOne = 0;

        public ClassLibrary_ARONE ObjectReceiver { get; private set; }

        
        public int IdArOne
        {
            get => idArOne;
            set
            {
                idArOne = value;
                switch (value)
                {
                    case 0:
                        OpenArOneDll0();
                        break;
                    case 1:
                        OpenArOneDll1();
                        break;
                }
                OnSendNumberArOne?.Invoke(value);
                //ObjectReceiver = NumberArOne[value];
            }
        }

        DispatcherTimer dispatcherTimer1;
        //public ItemsComBoxArone itemsComBoxArone;        
        #region Properties
        public static int vybor_DLL = 1;  //0 -- подключение AR6000DLL; 1 -- подключение AR-ONE
        
        public string portAdress = "COM6";
        public int baudRate = 19200;
        public string PortAdress
        {
            get =>  portAdress; 
            set { portAdress = value; }
        }
        public int BaudRate
        {
            get => baudRate;
            set { baudRate = value; }
        }
        
        public string FrqHz
        {
            get => Hz;
            set { Hz = value; }
        }

        string dB = "dB";
        public string Hz = "Hz";
        string kHz = "kHz";
        string MHz = " MHz";
        string FrqToAOR = "";
        string UsilVKL = "Amplif ON";
        string vykl = "Off";
        string vkl = "On";
        string Attt = "Atten";
        string Avto = "Auto";
        
        static double Fs = 44100;               // Частота дискретизвции 
        static double T = 1.0 / Fs;             // Шаг дискретизации
        static double Fn = Fs / 2;              // Частота Найквиста
        static int N = (int)Fs / 5;

        int countafterdot = 0;
        double volumeProcents;
        bool DotInFrq = false;
        bool zapolneno = false;
        public float step_frq_MHz = 10f / 1000000f;
        int comOpen;
        double valueMemory = 10;
        #endregion

        public UserControl1()
        {            
            InitializeComponent();
            
            InitConnection();
            //DataContext = new ViewModelComboBox();
            Language(ConvertLang(language));

        }

        public void InitConnection()
        {
            try
            {
                AR6000DLL = new Ar6000Manager();
                //IdArOne = 0;
                gaugeLvl.PrimaryScale.RangeBegin = -120;
                gaugeLvl.PrimaryScale.RangeEnd = 0;

                ClassLibrary_ARONE_DLL = new ClassLibrary_ARONE();
                ClassLibrary_ARONE_DLL2 = new DllSecondArOne.ClassLibrary_ARONESecond();

                ConnectorVisible();

                //ObjectReceiver = NumberArOne[idArOne];

                try
                {
                    switch (IdArOne)
                    {
                        case 0:
                            OpenArOneDll0();
                            break;
                        case 1:
                            OpenArOneDll1();
                            break;
                    }

                    Thread.Sleep(300);                    
                   
                    //НАСТРОЙКА ТАЙМЕРА
                    dispatcherTimer1 = new DispatcherTimer();
                    //dispatcherTimer1.Tick += new EventHandler(DispatcherTimer1_Tick);
                    dispatcherTimer1.Interval = new TimeSpan(0, 0, 1);
                }
                catch { }
            }
            catch { }                        
        }                

        public void ReadFrq()
        {
            switch (vybor_DLL)
            {
                case 0:
                    OnReadFreq?.Invoke(this, new FrequencyEventArgs(Frequency));
                    break;
                case 1:
                    OnReadFreq?.Invoke(this, new FrequencyEventArgs(FrequencyOne));
                    break;
            }        
        }

        public void ReadFrqBwPeleng()
        {
            OnFrqBwPeleng?.Invoke(this, new FreqBwEventArgs(FrequencyOne, BandwidthOne, SignalLvlDbm));
        }

        public void ComIsOpen()
        {
            switch (IdArOne)
            {
                case 0:
                    if (ClassLibrary_ARONE_DLL.ArOne.PortIsOpen)
                    {
                        comOpen = 1;
                        OnComIsOpen?.Invoke(this, new ComIsOpenEventArgs(comOpen));
                    }
                    else
                    {
                        comOpen = 0;
                        OnComIsOpen?.Invoke(this, new ComIsOpenEventArgs(comOpen));
                    }
                    break;
                case 1:
                    if (ClassLibrary_ARONE_DLL2.ArOne.PortIsOpen)
                    {
                        comOpen = 1;
                        OnComIsOpen?.Invoke(this, new ComIsOpenEventArgs(comOpen));
                    }
                    else
                    {
                        comOpen = 0;
                        OnComIsOpen?.Invoke(this, new ComIsOpenEventArgs(comOpen));
                    }
                    break;
            }
            
        }

        public void ReadMemory(object sender, EventArgs e)
        {
            //AR6000DLL.MemoryReadMode();
        }

        public void FrequencyFromPanorama(double Freq)
        {
            switch (IdArOne)
            {
                case 0:
                    ClassLibrary_ARONE.FrequencySet(Freq);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.FrequencySet(Freq);
                    break;
                default:
                    break;
            }
           
        }

        #region ConnectionToReceiver
        public void ConnectionToMain()
        {
            try
            {
                if (SerialPort.GetPortNames().Any(x => x == portAdress))    //Проверка, существует ли COM-порт
                {
                    switch (vybor_DLL)
                    {
                        case 0:
                            if (Ar6000Manager.port == null)             //Проверка, открыт ли COM-порт
                            {
                                AR6000DLL.OpenPort(portAdress);
                                ARPort_Null();
                            }
                            else
                            {
                                ARPort_IsOpen();
                            }
                            break;
                        case 1:
                            switch (IdArOne)
                            {
                                case 0:
                                    if (ClassLibrary_ARONE.port == null)
                                    {
                                        ClassLibrary_ARONE_DLL.OpenPort(portAdress, baudRate);
                                        AROnePort_Null();
                                    }
                                    else
                                    {
                                        AROnePort_IsOpen();
                                    }
                                    break;
                                    break;
                                case 1:
                                    if (ClassLibrary_ARONESecond.port == null)
                                    {
                                        ClassLibrary_ARONE_DLL2.OpenPort(portAdress, baudRate);
                                        AROnePort_Null();
                                    }
                                    else
                                    {
                                        AROnePort_IsOpen();
                                    }
                                    break;                                    
                                default:
                                    break;
                                    
                            }
                            break;
                    }
                }
                else
                {
                    string messRus = "COM-порт отсутствует";
                    string messEng = "No COM-port";
                    switch (langMessage)
                    {
                        case "Rus": MessageBox.Show(messRus);
                            break;
                        case "Eng": MessageBox.Show(messEng);
                            break;
                    }
                }
            }
            catch { }
        }
        private void ARPort_Null()
        {
            if (Ar6000Manager.port.IsOpen)    //Проверка, открылся ли COM-порт
            {
                Ask_Arone();
                isLvlRequest = true;
                TaskManager();
                //dispatcherTimer1.Start();

            }
            else
            {
                isLvlRequest = false;
                //dispatcherTimer1.Stop();

            }
        }
        private void ARPort_IsOpen()
        {
            if (Ar6000Manager.port.IsOpen)   //Если порт открыт, то закрываем
            {
                AR6000DLL.ClosePort();
                BorderBrush = Brushes.Gray;
                isLvlRequest = false;
                //dispatcherTimer1.Stop();

            }
            else
            {
                AR6000DLL.OpenPort(PortAdress);
                Ask_Arone();
                isLvlRequest = true;
                TaskManager();
                //dispatcherTimer1.Start();

            }
        }
        private void AROnePort_Null()
        {
            switch (IdArOne)
            {
                case 0:
                    if (ClassLibrary_ARONE_DLL.ArOne.PortIsOpen)
                    {
                        Ask_Arone();
                        isLvlRequest = true;
                        TaskManager();
                        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Zapolnenie_ComboBox();
                        //dispatcherTimer1.Start();
                    }
                    else { isLvlRequest = false; }
                    break;
                case 1:
                    if (ClassLibrary_ARONE_DLL2.ArOne.PortIsOpen)
                    {
                        Ask_Arone();
                        isLvlRequest = true;
                        TaskManager();
                        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Zapolnenie_ComboBox();
                        //dispatcherTimer1.Start();
                    }
                    else { isLvlRequest = false; }
                    break;
            }            
        }
        private void AROnePort_IsOpen()
        {
            switch (IdArOne)
            {
                case 0:
                    if (ClassLibrary_ARONE_DLL.ArOne.PortIsOpen)
                    {
                        ClassLibrary_ARONE_DLL.ClosePort();
                        isLvlRequest = false;
                        //dispatcherTimer1.Stop();
                    }
                    else
                    {
                        ClassLibrary_ARONE_DLL.OpenPort(PortAdress, BaudRate);
                        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Zapolnenie_ComboBox();                
                        Ask_Arone();
                        //dispatcherTimer1.Start();      
                        isLvlRequest = true;
                        TaskManager();
                    }
                    break;
                case 1:
                    if (ClassLibrary_ARONE_DLL2.ArOne.PortIsOpen)
                    {
                        ClassLibrary_ARONE_DLL2.ClosePort();
                        isLvlRequest = false;
                        //dispatcherTimer1.Stop();
                    }
                    else
                    {
                        ClassLibrary_ARONE_DLL2.OpenPort(PortAdress, BaudRate);
                        //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Zapolnenie_ComboBox();                
                        Ask_Arone();
                        //dispatcherTimer1.Start();      
                        isLvlRequest = true;
                        TaskManager();
                    }
                    break;
            }
        }
        #endregion

        // ПОЛУЧЕНИЕ ЧАСТОТЫ ПРИЁМНИКА
        private void DecodedFrq()
        {
            switch (vybor_DLL)
            {
                case 0:
                    textBoxFrq.Dispatcher.Invoke((delegate ()
                    {
                        textBoxFrq.Text = (Frequency / 1000000).ToString() + "," + (Frequency % 1000000).ToString("000") + MHz;
                        
                    }));
                    //ReadFrq(Frequency);
                    break;
                case 1:
                    textBoxFrq.Dispatcher.Invoke((delegate ()
                    {
                        textBoxFrq.Text = (FrequencyOne / 1000000).ToString() + "," + (FrequencyOne % 1000000).ToString("000").PadLeft(6, '0') + MHz;
                        FrequencyOneChanged = FrequencyOne;
                        OnLvlSignal?.Invoke(this, new LevelOfSignalsEventArgs(PSigLevel.Value, PSqlLevel.Value, FrequencyOne));
                    }));
                    break;
                    // default: break;
            }
        }
        
        private void textBoxFrq_LostFocus(object sender, RoutedEventArgs e)
        {
            OnTextBoxFrqLostFocus?.Invoke();
        }

        private void textBoxFrq_LostKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {
            OnTextBoxFrqKeyboardEventHandler?.Invoke(this, e);
        }
               
        private void DecodedMemoryChannelDataRead()
        {
            //ReceiverTableControl.textBoxData.Dispatcher.Invoke((delegate ()
            //{
            //    ReceiverTableControl.textBoxData.Text = AR6000DLL.AORstruct.MemoryData.ToString();
            //}));
        }

        //Отправка на панораму
        protected virtual void OnFreqChanged(Int64 Frequency, double ChangedBw)
        {
            FrequencyChanged?.Invoke(this, Frequency, ChangedBw);
        }

        private double DecodeBwPanoram(Int16 bw)
        {
            switch (bw)
            {
                case 0: return 0.5;                          
                case 1: return 3.0;
                case 2: return 6.0;
                case 3: return 8.5;
                case 4: return 16;
                case 5: return 30;
                case 6: return 100;
                case 7: return 200;
                case 8: return 300;
                default: return 200;
            }
        }

        private void bMute_Checked(object sender, RoutedEventArgs e)
        {
            ResourceDictionary dict = new ResourceDictionary
            {
                Source = new Uri(String.Format("/WpfControlLibraryBut;component/Icons/Icon.Mute.xaml"), UriKind.Relative)
            };

            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                          where d.Source != null && d.Source.OriginalString.StartsWith("/WpfControlLibraryBut;component/Icons/Icon.")
                                          select d).First();

            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);
                Resources.MergedDictionaries.Insert(ind, dict);

            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }
            try
            {
                valueMemory = SAudioGain.Value;
                ClassLibrary_ARONE_DLL?.AFGainSet(0);
                SAudioGain.Value = 0;
            }
            catch { }
        }

        private void bMute_Unchecked(object sender, RoutedEventArgs e)
        {
            ResourceDictionary dict = new ResourceDictionary
            {
                Source = new Uri(String.Format("/WpfControlLibraryBut;component/Icons/Icon.Play.xaml"), UriKind.Relative)
            };

            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                          where d.Source != null && d.Source.OriginalString.StartsWith("/WpfControlLibraryBut;component/Icons/Icon.")
                                          select d).First();

            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);
                Resources.MergedDictionaries.Insert(ind, dict);

            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }

            try
            {
                if (valueMemory != 0)
                {
                    SAudioGain.Value = valueMemory;
                }
                else {  }
                
                ClassLibrary_ARONE_DLL?.AFGainSet(SAudioGain.Value);
            }
            catch { }
        }

        public void UpdateDataMemory()
        {
            //ClassLibrary_ARONE.ARONE.memTwo.Clear();
            //ClassLibrary_ARONE.ARONE.MBank = "1"; //Bank.ToString();
            //ClassLibrary_ARONE_DLL.MemoryChannelDataReadGet();    /* Чтение данных банков памяти */
            //Thread.Sleep(20);
            //ClassLibrary_ARONE_DLL.FreeScanGet();
            //Thread.Sleep(5);
            //ClassLibrary_ARONE_DLL.DelayTime_ScanDelayGet();
            //Thread.Sleep(5);
        }
    }
}
