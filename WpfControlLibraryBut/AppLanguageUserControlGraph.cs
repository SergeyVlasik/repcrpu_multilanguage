﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace WpfControlLibraryBut
{
    public partial class UserControlGraph : UserControl
    {
        private Int16 language = 1;
        public Int16 ChangeLang
        {
            get { return language; }
            set
            {
                language = value;                
                Language(ConvertLang(value));                
            }
        }

        private string ConvertLang(Int16 Lan)
        {
            switch (Lan)
            {
                case 0: return "ru-RU";
                case 1: return "en-US";
                default: return "ru-RU";
            }
        }

        public new void Language(string value)
        {
            //1. Создаём ResourceDictionary для новой культуры
            ResourceDictionary dict = new ResourceDictionary();
            switch (value)
            {
                case "Rus":
                    dict.Source = new Uri(String.Format("/WpfControlLibraryBut;component/ResourcesLang/lang.ru-RU.xaml", value), UriKind.Relative);
                    break;
                case "Eng":
                    dict.Source = new Uri(String.Format("/WpfControlLibraryBut;component/ResourcesLang/lang.en-US.xaml", value), UriKind.Relative);
                    break;
                case "Azr":
                    dict.Source = new Uri(String.Format("/WpfControlLibraryBut;component/ResourcesLang/lang.az-AZ.xaml", value), UriKind.Relative);
                    break;
                default:
                    dict.Source = new Uri("/WpfControlLibraryBut;component/ResourcesLang/lang.en-US.xaml", UriKind.Relative);
                    break;
            }


            //2. Находим старую ResourceDictionary и удаляем его и добавляем новую ResourceDictionary
            ResourceDictionary oldDict = (from d in Resources.MergedDictionaries
                                          where d.Source != null && d.Source.OriginalString.StartsWith("/WpfControlLibraryBut;component/ResourcesLang/lang.")
                                          select d).First();

            if (oldDict != null)
            {
                int ind = Resources.MergedDictionaries.IndexOf(oldDict);
                Resources.MergedDictionaries.Remove(oldDict);
                Resources.MergedDictionaries.Insert(ind, dict);

            }
            else
            {
                Resources.MergedDictionaries.Add(dict);
            }            
        }
    }
}
