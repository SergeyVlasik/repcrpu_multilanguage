﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryBut
{
    public class FrequencyEventArgs : EventArgs
    {
        public FrequencyEventArgs(Int64 Freq)
        {
            Frequency = Freq;
        }

        public Int64 Frequency { get; private set; }
    }
}
