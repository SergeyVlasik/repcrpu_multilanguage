﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlLibraryBut
{
    public class MemoryEventArgs : EventArgs
    {
        public MemoryEventArgs(string bank, string channel)
        {
            Bank = bank;
            Channel = channel;
        }

        public string Bank { get; private set; }
        public string Channel { get; private set; }
    }
}
