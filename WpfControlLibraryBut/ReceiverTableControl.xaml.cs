﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;
using Microsoft.Win32;
using System.Reflection;
using AR6000;
using Microsoft.Office.Interop.Word;
using DllSecondArOne;

namespace WpfControlLibraryBut
{
    /// <summary>
    /// Логика взаимодействия для ReceiverTableControl.xaml
    /// </summary>
    public partial class ReceiverTableControl : UserControl
    {
        public Ar6000Manager AR6000DLL;
        public UserControl1 UserControl1;

        public ClassLibrary_ARONE ClassLibrary_ARONE_DLL;
        public ClassLibrary_ARONESecond ClassLibrary_ARONE_DLL2;

        public event EventHandler<MemoryEventArgs> OnMemory;
        public delegate void MyControlEventHandler();
        public event MyControlEventHandler OnUpdate;
        public event MyControlEventHandler OnUpdateFileType;

        public event EventHandler<ToSupressEventArgs> OnFreqSupress;
        

        DispatcherTimer dispatcherTimer1 = new DispatcherTimer();
        DispatcherTimer dispatcherTimer2 = new DispatcherTimer();
        ObservableCollection<TableArone> Data;
        List<TableArone> list;

        //public string MemoryData
        //{
        //    get => AR6000Manager.AOR.MemoryData;
        //    set { }
        //}

        //public string MemoryDataOne
        //{
        //    get => ClassLibrary_ARONE.ARONE.MemoryData;
        //}

        public int Bank
        {
            get => bank;
            set { bank = value; }
        }
        int bank = 1;
        string formatFile = "Word";
        public string FormatFile
        {
            get => formatFile;
            set { formatFile = value; }
        }
        public Int64 Frequency
        {
            get;
            set;
        }
        public Int16 BW { get; set; }

        private double sLvl = -120;
        public double SignalLvl 
        {
            get => sLvl;
            set { sLvl = value; }
        }

        private double sqlLvl = -120;
        public double SqlLvl 
        {
            get => sqlLvl;
            set { sqlLvl = value; }
        }
        
        int vyborDll = UserControl1.vybor_DLL;
        int dllArOne = 0;
        public int DllArOne
        {
            get => dllArOne;
            set
            {               
                dllArOne = value;
                UserControl1 = new UserControl1();
                switch (value)
                {
                    case 0:
                        ClassLibrary_ARONE.OnDecodedMemoryChannelDataRead += new ClassLibrary_ARONE.ByteEventHandler(DecodedMemoryChannelDataRead);
                        ClassLibrary_ARONE.OnDecodedScanMode += new ClassLibrary_ARONE.ByteEventHandler(DecodedScanMode);
                        ClassLibrary_ARONE.OnFreeScan += new ClassLibrary_ARONE.ByteEventHandler(DecodedFreeScan);
                        ClassLibrary_ARONE.OnDelayScan += new ClassLibrary_ARONE.ByteEventHandler(DelayScan);
                        break;
                    case 1:
                        ClassLibrary_ARONESecond.OnDecodedMemoryChannelDataRead += new ClassLibrary_ARONESecond.ByteEventHandler(DecodedMemoryChannelDataRead);
                        ClassLibrary_ARONESecond.OnDecodedScanMode += new ClassLibrary_ARONESecond.ByteEventHandler(DecodedScanMode);
                        ClassLibrary_ARONESecond.OnFreeScan += new ClassLibrary_ARONESecond.ByteEventHandler(DecodedFreeScan);
                        ClassLibrary_ARONESecond.OnDelayScan += new ClassLibrary_ARONESecond.ByteEventHandler(DelayScan);
                        break;
                }
            }
        }

        public ReceiverTableControl()
        {
            InitializeComponent();

            AR6000DLL = new Ar6000Manager();
            UserControl1 = new UserControl1();

            ClassLibrary_ARONE_DLL = new ClassLibrary_ARONE();
            ClassLibrary_ARONE_DLL2 = new ClassLibrary_ARONESecond();

            UserControl1.OnSendNumberArOne += UserControl1_OnSendNumberArOne;
            UserControl1.OnComboboxMode += new UserControl1.SelectionChangedEventHandler(ComboboxMode);
            UserControl1.OnFrqBwPeleng += ReadFreqBwToPeleng;

            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.OnDecodedMemoryChannelDataRead += new ClassLibrary_ARONE.ByteEventHandler(DecodedMemoryChannelDataRead);
                    ClassLibrary_ARONE.OnDecodedScanMode += new ClassLibrary_ARONE.ByteEventHandler(DecodedScanMode);
                    ClassLibrary_ARONE.OnFreeScan += new ClassLibrary_ARONE.ByteEventHandler(DecodedFreeScan);
                    ClassLibrary_ARONE.OnDelayScan += new ClassLibrary_ARONE.ByteEventHandler(DelayScan);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.OnDecodedMemoryChannelDataRead += new ClassLibrary_ARONESecond.ByteEventHandler(DecodedMemoryChannelDataRead);
                    ClassLibrary_ARONESecond.OnDecodedScanMode += new ClassLibrary_ARONESecond.ByteEventHandler(DecodedScanMode);
                    ClassLibrary_ARONESecond.OnFreeScan += new ClassLibrary_ARONESecond.ByteEventHandler(DecodedFreeScan);
                    ClassLibrary_ARONESecond.OnDelayScan += new ClassLibrary_ARONESecond.ByteEventHandler(DelayScan);
                    break;
            }
            

            Data = new ObservableCollection<TableArone>();
            for (int i = 0; i < 10; i++)
            {
                Data.Add(new TableArone(null, null, null, null, null, null));                
            }
            TableArone.ItemsSource = Data;
            list = new List<TableArone>();

            SaveChanges.Visibility = Visibility.Collapsed;
            tBoxFree.Text = "0,1";
            tBoxDelay.Text = "0,1";
            //UserControl1.FrequencyChanged += OnFreqChanged;
            ConnectAudio();
        }

        private void UserControl1_OnSendNumberArOne(int number)
        {
            DllArOne = number;
        }

        static void OnFreqChanged(object sender, EventArgs e)
        {
            
        }

        public void Update_Click(object sender, RoutedEventArgs e)
        {
            switch (vyborDll)
            {
                case 0:                    
                    //AR6000Manager.MemoryChannelDataReadGet();
                    Thread.Sleep(20);
                    
                    break;
                case 1:
                    OnUpdate?.Invoke();

                    switch (dllArOne)
                    {
                        case 0:
                            //UserControl1.UpdateDataMemory();
                            ClassLibrary_ARONE.ARONE.memTwo.Clear();
                            ClassLibrary_ARONE.ARONE.MBank = Bank.ToString();
                            ClassLibrary_ARONE.MemoryChannelDataReadGet();    /* Чтение данных банков памяти */
                            Thread.Sleep(20);
                            ClassLibrary_ARONE.FreeScanGet();
                            Thread.Sleep(5);
                            ClassLibrary_ARONE.DelayTime_ScanDelayGet();
                            Thread.Sleep(5);
                            break;
                        case 1:
                            ClassLibrary_ARONESecond.ARONE.memTwo.Clear();
                            ClassLibrary_ARONESecond.ARONE.MBank = Bank.ToString();
                            ClassLibrary_ARONESecond.MemoryChannelDataReadGet();    /* Чтение данных банков памяти */
                            Thread.Sleep(20);
                            ClassLibrary_ARONESecond.FreeScanGet();
                            Thread.Sleep(5);
                            ClassLibrary_ARONESecond.DelayTime_ScanDelayGet();
                            Thread.Sleep(5);
                            break;                           
                    }
                    
                    break;
            }            
        }

        private void ReadFreqBwToPeleng(object sender, FreqBwEventArgs e)
        {
            Frequency = e.Frequency;
            BW = e.BW;
            SignalLvl = e.Lvl;
        }

        private void DecodedMemoryChannelDataRead()
        {
            BuildTable();
        }

        private void BuildTable()
        {
            TableArone.Dispatcher.Invoke(delegate 
            {             
                list.Clear();
                Data.Clear();

                switch (dllArOne)
                {
                    case 0:
                        foreach (string element in ClassLibrary_ARONE.ARONE.memthree)
                        {
                            try
                            {
                                if (element.Length > 44)
                                {
                                    if (element.Substring(0, 2) == "MX")
                                    {
                                        string adress = element.Substring(2, 3);
                                        string time = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00");
                                        string high = "123";
                                        string test = element.Substring(45, 12);
                                        string mode = null;
                                        int.TryParse(element.Substring(29, 1), out int modeIndex);
                                        switch (modeIndex)
                                        {
                                            case 0: mode = "FM"; break;
                                            case 1: mode = "AM"; break;
                                            case 2: mode = "CW"; break;
                                            case 3: mode = "USB"; break;
                                            case 4: mode = "LSB"; break;
                                            case 5: mode = "WFM"; break;
                                            case 6: mode = "NFM"; break;
                                        }
                                        string bw = null;
                                        int.TryParse(element.Substring(33, 1), out int bwIndex);
                                        switch (bwIndex)
                                        {
                                            case 0: bw = "0.5"; break;// кГц"                                
                                            case 1: bw = "3.0"; break; //кГц";
                                            case 2: bw = "6.0"; break; //кГц";
                                            case 3: bw = "8.5"; break;
                                            case 4: bw = "16"; break;
                                            case 5: bw = "30"; break; //кГц"; 
                                            case 6: bw = "100"; break; //кГц"; 
                                            case 7: bw = "200"; break; //кГц"; 
                                            case 8: bw = "300"; break; //кГц"; 
                                        }

                                        Int64.TryParse(element.Substring(12, 10), out Int64 Freq);
                                        string FrequencyOne = (Freq / 1000000).ToString() + "," + (Freq % 1000000).ToString("000").PadLeft(6, '0');
                                        Data.Add(new TableArone(adress, FrequencyOne, mode, bw, time, test));
                                    }
                                }
                                else { }
                            }
                            catch (Exception ex) { }
                        }
                        break;
                    case 1:
                        foreach (string element in ClassLibrary_ARONESecond.ARONE.memthree)
                        {
                            try
                            {
                                if (element.Length > 44)
                                {
                                    if (element.Substring(0, 2) == "MX")
                                    {
                                        string adress = element.Substring(2, 3);
                                        string time = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00");
                                        string high = "123";
                                        string test = element.Substring(45, 12);
                                        string mode = null;
                                        int.TryParse(element.Substring(29, 1), out int modeIndex);
                                        switch (modeIndex)
                                        {
                                            case 0: mode = "FM"; break;
                                            case 1: mode = "AM"; break;
                                            case 2: mode = "CW"; break;
                                            case 3: mode = "USB"; break;
                                            case 4: mode = "LSB"; break;
                                            case 5: mode = "WFM"; break;
                                            case 6: mode = "NFM"; break;
                                        }
                                        string bw = null;
                                        int.TryParse(element.Substring(33, 1), out int bwIndex);
                                        switch (bwIndex)
                                        {
                                            case 0: bw = "0.5"; break;// кГц"                                
                                            case 1: bw = "3.0"; break; //кГц";
                                            case 2: bw = "6.0"; break; //кГц";
                                            case 3: bw = "8.5"; break;
                                            case 4: bw = "16"; break;
                                            case 5: bw = "30"; break; //кГц"; 
                                            case 6: bw = "100"; break; //кГц"; 
                                            case 7: bw = "200"; break; //кГц"; 
                                            case 8: bw = "300"; break; //кГц"; 
                                        }

                                        Int64.TryParse(element.Substring(12, 10), out Int64 Freq);
                                        string FrequencyOne = (Freq / 1000000).ToString() + "," + (Freq % 1000000).ToString("000").PadLeft(6, '0');
                                        Data.Add(new TableArone(adress, FrequencyOne, mode, bw, time, test));
                                    }
                                }
                                else { }
                            }
                            catch (Exception ex) { }
                        }
                        break;
                }
                
                TableArone.ItemsSource = Data;                
            });            
        }

        public IEnumerable<DataGridRow> GetDataGridRows(DataGrid grid)
        {
            var itemsSource = grid.ItemsSource as IEnumerable;
            if (null == itemsSource) yield return null;
            foreach (var item in itemsSource)
            {
                if (grid.ItemContainerGenerator.ContainerFromItem(item) is DataGridRow row) yield return row;
            }
        }

        private void DecodedScanMode()
        {
            TableArone.Dispatcher.Invoke(delegate () 
            {
                switch (dllArOne)
                {
                    case 0:
                        var modeScan = ClassLibrary_ARONE.ARONE.ModeScan;
                        var row = GetDataGridRows(TableArone);
                        foreach (DataGridRow r in row)
                        {
                            if (TableArone.Columns[0].GetCellContent(r) is TextBlock)
                            {
                                TextBlock cellContent = TableArone.Columns[0].GetCellContent(r) as TextBlock;
                                if (cellContent.Text == modeScan)
                                {
                                    TableArone.SelectedItem = r.Item;
                                    TableArone.ScrollIntoView(r.Item);
                                    break;
                                }
                            }
                        }
                        break;
                    case 1:
                        var modeScan2 = ClassLibrary_ARONESecond.ARONE.ModeScan;
                        var row2 = GetDataGridRows(TableArone);
                        foreach (DataGridRow r in row2)
                        {
                            if (TableArone.Columns[0].GetCellContent(r) is TextBlock)
                            {
                                TextBlock cellContent = TableArone.Columns[0].GetCellContent(r) as TextBlock;
                                if (cellContent.Text == modeScan2)
                                {
                                    TableArone.SelectedItem = r.Item;
                                    TableArone.ScrollIntoView(r.Item);
                                    break;
                                }
                            }
                        }
                        break;
                }
                
                
            });            
        }

        private void DecodeMemoryData()
        {
            
        }

        private void TableArone_Loaded(object sender, RoutedEventArgs e)
        {
            /*
            DateTime dataTime = new DateTime();
            string high = "high";
            string test = "test";
            string time = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00");
            List<TableArone> data = new List<TableArone>(9);
            data.Add(new TableArone(1, 1, AR6000Manager.AOR.frequency, 1, -50, time, AR6000DLL.AORstruct.Mode.ToString(), high, test));
            data.Add(new TableArone(2, 1, AR6000Manager.AOR.frequency, 1, -50, time, AR6000DLL.AORstruct.Mode.ToString(), high, test));
            TableArone.Items.Clear();
            TableArone.ItemsSource = data;
            */
        }
        
        public void LoadTableArone()
        {
            //ClassLibrary_ARONE.MemoryChannelDataReadGet();
            Thread.Sleep(20);
        }

        private void SaveData_Click(object sender, RoutedEventArgs e)
        {
            SaveDataToMemory();
        }

        private void SaveDataToMemory()
        {
            switch (dllArOne)
            {
                case 0:
                    SaveDataToMemoryDllOne();
                    break;
                case 1:
                    SaveDataToMemoryDllTwo();
                    break;
            }
        }

        private void SaveDataToMemoryDllOne()
        {
            try
            {
                List<string> channels = new List<string>();
                var first = 0;
                var second = 0;

                if (TableArone.Items.Count == 0)
                {
                    var adressFirst = Bank.ToString() + "00";
                    ClassLibrary_ARONE.MemoryDataSettingSet(adressFirst.ToString());
                    Thread.Sleep(20);
                }
                else
                {
                    var row = GetDataGridRows(TableArone);

                    foreach (DataGridRow r in row)
                    {
                        if (TableArone.Columns[0].GetCellContent(r) is TextBlock)
                        {
                            TextBlock cellContent = TableArone.Columns[0].GetCellContent(r) as TextBlock;
                            channels.Add(cellContent.Text);
                        }
                    }

                    first = int.Parse(channels[0].Substring(1, 2));
                    bool isSaved = false;
                    int nullAddress = 0;

                    if (first != nullAddress)
                    {
                        //сохраняем по адресу 00
                        var adressFirst = Bank.ToString() + "00";
                        ClassLibrary_ARONE.MemoryDataSettingSet(adressFirst);
                    }
                    else
                    {
                        if (channels.Count == 1)
                        {
                            var adressFirst = Bank.ToString() + "01";
                            ClassLibrary_ARONE.MemoryDataSettingSet(adressFirst);
                        }
                        else
                        {
                            for (int i = 1; i < channels.Count; i++)
                            {
                                if (!isSaved)
                                {
                                    second = int.Parse(channels[i].Substring(1, 2)) - first;
                                    if (second == 1)
                                    {
                                        if (i == channels.Count - 1)
                                        {
                                            var channel1 = (int.Parse(channels[i].Substring(1, 2)) + 1).ToString().PadLeft(2, '0');
                                            var address = Bank.ToString() + channel1;
                                            ClassLibrary_ARONE.MemoryDataSettingSet(address);
                                            isSaved = true;
                                        }
                                    }
                                    else
                                    {
                                        var nextAddress = (first + 1).ToString().PadLeft(2, '0');
                                        var address = Bank.ToString() + nextAddress;
                                        ClassLibrary_ARONE.MemoryDataSettingSet(address);
                                        isSaved = true;
                                        break;
                                    }
                                    first = int.Parse(channels[i].Substring(1, 2));
                                }
                            }
                        }
                    }
                }
            }
            catch
            { }

            ClassLibrary_ARONE.ARONE.memTwo.Clear();
            ClassLibrary_ARONE.MemoryChannelDataReadGet();
        }

        private void SaveDataToMemoryDllTwo()
        {
            try
            {
                List<string> channels = new List<string>();
                var first = 0;
                var second = 0;

                if (TableArone.Items.Count == 0)
                {
                    var adressFirst = Bank.ToString() + "00";
                    ClassLibrary_ARONESecond.MemoryDataSettingSet(adressFirst.ToString());
                    Thread.Sleep(20);
                }
                else
                {
                    var row = GetDataGridRows(TableArone);

                    foreach (DataGridRow r in row)
                    {
                        if (TableArone.Columns[0].GetCellContent(r) is TextBlock)
                        {
                            TextBlock cellContent = TableArone.Columns[0].GetCellContent(r) as TextBlock;
                            channels.Add(cellContent.Text);
                        }
                    }

                    first = int.Parse(channels[0].Substring(1, 2));
                    bool isSaved = false;
                    int nullAddress = 0;

                    if (first != nullAddress)
                    {
                        //сохраняем по адресу 00
                        var adressFirst = Bank.ToString() + "00";
                        ClassLibrary_ARONESecond.MemoryDataSettingSet(adressFirst);
                    }
                    else
                    {
                        if (channels.Count == 1)
                        {
                            var adressFirst = Bank.ToString() + "01";
                            ClassLibrary_ARONESecond.MemoryDataSettingSet(adressFirst);
                        }
                        else
                        {
                            for (int i = 1; i < channels.Count; i++)
                            {
                                if (!isSaved)
                                {
                                    second = int.Parse(channels[i].Substring(1, 2)) - first;
                                    if (second == 1)
                                    {
                                        if (i == channels.Count - 1)
                                        {
                                            var channel1 = (int.Parse(channels[i].Substring(1, 2)) + 1).ToString().PadLeft(2, '0');
                                            var address = Bank.ToString() + channel1;
                                            ClassLibrary_ARONESecond.MemoryDataSettingSet(address);
                                            isSaved = true;
                                        }
                                    }
                                    else
                                    {
                                        var nextAddress = (first + 1).ToString().PadLeft(2, '0');
                                        var address = Bank.ToString() + nextAddress;
                                        ClassLibrary_ARONESecond.MemoryDataSettingSet(address);
                                        isSaved = true;
                                        break;
                                    }
                                    first = int.Parse(channels[i].Substring(1, 2));
                                }
                            }
                        }
                    }
                }
            }
            catch
            { }

            ClassLibrary_ARONESecond.ARONE.memTwo.Clear();
            ClassLibrary_ARONESecond.MemoryChannelDataReadGet();
        }

        private void BScan_Checked(object sender, RoutedEventArgs e)
        {
            //dispatcherTimer2.Tick -= new EventHandler(DispatcherTimer2_Tick);
            //dispatcherTimer2.Stop();
            double delayFree = 1.5;  //0.0 -- free scan выключен, 0.1-9.9 -- free scan Вкл
            double delayTime = 0.1;  // FF (hold)  --  остановиться на сканировании

            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.ScanRestartSet(1);  //0-- автоскан не работает, 1-- автоскан работает через T= delayTime

                    Thread.Sleep(20);
                    ClassLibrary_ARONE.ModeScanSet(Bank);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.ScanRestartSet(1);  //0-- автоскан не работает, 1-- автоскан работает через T= delayTime

                    Thread.Sleep(20);
                    ClassLibrary_ARONESecond.ModeScanSet(Bank);
                    break;
            }
            
            Thread.Sleep(20);
            dispatcherTimer1.Tick += new EventHandler(DispatcherTimer1_Tick);
            dispatcherTimer1.Interval = new TimeSpan(0, 0, 1 / 10);
            dispatcherTimer1.Start();
            Thread.Sleep(20);
        }

        private void BScan_Unchecked(object sender, RoutedEventArgs e)
        {
            dispatcherTimer1.Tick -= new EventHandler(DispatcherTimer1_Tick);            
            dispatcherTimer1.Stop();
            //dispatcherTimer2.Tick -= new EventHandler(DispatcherTimer2_Tick);
            //dispatcherTimer2.Interval = new TimeSpan(0, 0, 1);
            //dispatcherTimer2.Start();
            string Vfo = "A";

            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.ModeVFOSet(Vfo);
                    ClassLibrary_ARONE.FrequencyGet();
                    break;
                case 1:
                    ClassLibrary_ARONESecond.ModeVFOSet(Vfo);
                    ClassLibrary_ARONESecond.FrequencyGet();
                    break;
            }
            
            TableArone.SelectedItem = null;            
        }

        private void DispatcherTimer1_Tick(object sender, EventArgs e)
        {
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.FrequencyGet();
                    ClassLibrary_ARONE.RXModeGet();
                    break;
                case 1:
                    ClassLibrary_ARONESecond.FrequencyGet();
                    ClassLibrary_ARONESecond.RXModeGet();
                    break;
            }   
        }

        private void LoadFile_Click(object sender, RoutedEventArgs e)
        {
            switch (dllArOne)
            {
                case 0:
                    LoadFileOneDll();
                    break;
                case 1:
                    LoadFileTwoDll();
                    break;
            }
        }

        private void LoadFileOneDll()
        {
            Data.Clear();
            list.Clear();
            ClassLibrary_ARONE.ARONE.memTwo.Clear();

            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Filter = "Text file (*.txt)|*.txt"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                StreamReader reader = new StreamReader(fileInfo.Open(FileMode.Open, FileAccess.Read), Encoding.GetEncoding("windows-1251"));
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var parsed = line.Split('\t');
                    list.Add(new TableArone(parsed[0], parsed[1], parsed[2], parsed[3], parsed[4], parsed[5]));
                    Thread.Sleep(20);

                    string freq = parsed[1];
                    double.TryParse(freq, out double frqq);
                    if ((frqq > 0.09) && (frqq < 3300))
                    {
                        ClassLibrary_ARONE.FrequencySet(frqq);
                    }
                    Thread.Sleep(30);

                    string mod = parsed[2];
                    switch (mod)
                    {
                        case "FM": ClassLibrary_ARONE.ModeSet(0); break;
                        case "AM": ClassLibrary_ARONE.ModeSet(1); break;
                        case "CW": ClassLibrary_ARONE.ModeSet(2); break;
                        case "USB": ClassLibrary_ARONE.ModeSet(3); break;
                        case "LSB": ClassLibrary_ARONE.ModeSet(4); break;
                        case "WFM": ClassLibrary_ARONE.ModeSet(5); break;
                        case "NFM": ClassLibrary_ARONE.ModeSet(6); break;
                    }
                    Thread.Sleep(30);

                    string bw = parsed[3];
                    switch (bw)
                    {
                        case "0.5": ClassLibrary_ARONE.BandWidthSet(0); break;
                        case "3.0": ClassLibrary_ARONE.BandWidthSet(1); break;
                        case "6.0": ClassLibrary_ARONE.BandWidthSet(2); break;
                        case "8.5": ClassLibrary_ARONE.BandWidthSet(3); break;
                        case "16": ClassLibrary_ARONE.BandWidthSet(4); break;
                        case "30": ClassLibrary_ARONE.BandWidthSet(5); break;
                        case "100": ClassLibrary_ARONE.BandWidthSet(6); break;
                        case "200": ClassLibrary_ARONE.BandWidthSet(7); break;
                        case "300": ClassLibrary_ARONE.BandWidthSet(8); break;
                    }
                    Thread.Sleep(30);

                    string adress = parsed[0];
                    ClassLibrary_ARONE.MemoryDataSettingSet(adress.ToString());
                    ClassLibrary_ARONE.FrequencyGet();
                    Thread.Sleep(500);
                }
                TableArone.ItemsSource = list;
            }
        }

        private void LoadFileTwoDll()
        {
            Data.Clear();
            list.Clear();
            ClassLibrary_ARONESecond.ARONE.memTwo.Clear();

            OpenFileDialog openFileDialog = new OpenFileDialog()
            {
                Filter = "Text file (*.txt)|*.txt"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                StreamReader reader = new StreamReader(fileInfo.Open(FileMode.Open, FileAccess.Read), Encoding.GetEncoding("windows-1251"));
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    var parsed = line.Split('\t');
                    list.Add(new TableArone(parsed[0], parsed[1], parsed[2], parsed[3], parsed[4], parsed[5]));
                    Thread.Sleep(20);

                    string freq = parsed[1];
                    double.TryParse(freq, out double frqq);
                    if ((frqq > 0.09) && (frqq < 3300))
                    {
                        ClassLibrary_ARONESecond.FrequencySet(frqq);
                    }
                    Thread.Sleep(30);

                    string mod = parsed[2];
                    switch (mod)
                    {
                        case "FM": ClassLibrary_ARONESecond.ModeSet(0); break;
                        case "AM": ClassLibrary_ARONESecond.ModeSet(1); break;
                        case "CW": ClassLibrary_ARONESecond.ModeSet(2); break;
                        case "USB": ClassLibrary_ARONESecond.ModeSet(3); break;
                        case "LSB": ClassLibrary_ARONESecond.ModeSet(4); break;
                        case "WFM": ClassLibrary_ARONESecond.ModeSet(5); break;
                        case "NFM": ClassLibrary_ARONESecond.ModeSet(6); break;
                    }
                    Thread.Sleep(30);

                    string bw = parsed[3];
                    switch (bw)
                    {
                        case "0.5": ClassLibrary_ARONESecond.BandWidthSet(0); break;
                        case "3.0": ClassLibrary_ARONESecond.BandWidthSet(1); break;
                        case "6.0": ClassLibrary_ARONESecond.BandWidthSet(2); break;
                        case "8.5": ClassLibrary_ARONESecond.BandWidthSet(3); break;
                        case "16": ClassLibrary_ARONESecond.BandWidthSet(4); break;
                        case "30": ClassLibrary_ARONESecond.BandWidthSet(5); break;
                        case "100": ClassLibrary_ARONESecond.BandWidthSet(6); break;
                        case "200": ClassLibrary_ARONESecond.BandWidthSet(7); break;
                        case "300": ClassLibrary_ARONESecond.BandWidthSet(8); break;
                    }
                    Thread.Sleep(30);

                    string adress = parsed[0];
                    ClassLibrary_ARONESecond.MemoryDataSettingSet(adress.ToString());
                    ClassLibrary_ARONESecond.FrequencyGet();
                    Thread.Sleep(500);
                }
                TableArone.ItemsSource = list;
            }
        }

        private void FileDirectory()
        {
            string path = String.Format(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                "\\Documents\\" + "Receiver\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00"));
            if (!Directory.Exists(path))
            {
                DirectoryInfo directory = Directory.CreateDirectory(path);
            }

            outputFn = path + "\\" + "Bank №" + Bank.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" +
                DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00");            
        }

        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnUpdateFileType?.Invoke();
                switch (formatFile)
                {
                    case "Txt":
                        SaveToTxt();
                        break;
                    case "Word":
                        SaveToDocx();
                        break;
                    case "Excel":                        
                        SaveToExcel(ConvertToDataTable.ToDataTable(Data));
                        break;
                }
            }
            catch { }
        }

        public void TakeFormatFile(string type)
        {
            FormatFile = type;
        }

        private void SaveToTxt()
        {
            StringBuilder stringBuilder = new StringBuilder();            
            TableArone.SelectAllCells();
            TableArone.ClipboardCopyMode = DataGridClipboardCopyMode.ExcludeHeader;
            ApplicationCommands.Copy.Execute(null, TableArone);
            TableArone.UnselectAllCells();
            string result = (string)Clipboard.GetData(DataFormats.Text);
            Clipboard.Clear();
            FileDirectory();
            string outputFilename = outputFn + "." + "txt";
            stringBuilder.Append(result.ToString());
            File.WriteAllText(outputFilename, result.ToString());
        }

        public void SaveToDocx()
        {
            try
            {
                //Create an instance for word app
                Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();
                
                //Set animation status for word application
                //winword.ShowAnimation = false;
                
                //Set status for word application is to be visible or not.
                winword.Visible = false;

                //Create a missing variable for missing value
                object missing = Missing.Value;

                //Create a new document
                Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

                string[,] Table = new string[TableArone.Items.Count + 1, TableArone.Columns.Count];

                //Add header into the document
                string header = "Table Receiver";
                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {

                    //Get the header range and add the header details.
                    Range headerRange = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;//.wdAlignParagraphCenter;
                    headerRange.Font.ColorIndex = WdColorIndex.wdBlue;
                    headerRange.Font.Size = 14;
                    headerRange.Text = "";
                }
                
                //Add paragraph with Heading 1 style
                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                para1.Range.Font.Size = 14;
                //string curTime = "Время               " + DateTime.Now.ToString(@"HH:mm:ss");
                string curTime = DateTime.Today.ToLongDateString() + "   " + DateTime.Now.ToString(@"HH:mm:ss");
                para1.Range.Text = curTime;
                para1.Range.InsertParagraphAfter();

                //Create table and insert some dummy record
                Microsoft.Office.Interop.Word.Table firstTable = document.Tables.Add(para1.Range, Table.GetLength(0), Table.GetLength(1), ref missing, ref missing);
                firstTable.Borders.Enable = 1;
                
                foreach (Row row in firstTable.Rows)
                {
                    
                    foreach (Cell cell in row.Cells)
                    {
                        //Header row
                        if (cell.RowIndex == 1)
                        {
                            //TextBlock cellContent = TableArone.Columns[cell.ColumnIndex - 1].GetCellContent(TableArone.Items[0]) as TextBlock;
                            TextBlock cellHeader = TableArone.Columns[cell.ColumnIndex - 1].Header as TextBlock;
                            cell.Range.Text = cell.Range.Text.Replace("\r", "");
                            cell.Range.Text = cell.Range.Text.Replace("\a", "");
                            cell.Range.Text = cellHeader.Text;
                            cell.Range.Font.Bold = 1;

                            //other format properties goes here
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 10;
                            cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;

                            //Center alignment for the Header cells
                            cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        }
                        //Data row
                        else
                        {
                            try
                            {
                                if (TableArone.Columns[cell.ColumnIndex - 1].GetCellContent(TableArone.Items[cell.RowIndex - 2]) is TextBlock)
                                {
                                    TextBlock cellContent = TableArone.Columns[cell.ColumnIndex - 1].GetCellContent(TableArone.Items[cell.RowIndex - 2]) as TextBlock;
                                    cell.Range.Text = cell.Range.Text.Replace("\r", "");
                                    cell.Range.Text = cell.Range.Text.Replace("\a", "");
                                    cell.Range.Text = cellContent.Text;
                                }                                
                                //Thread.Sleep(100);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            
                        }
                    }
                }

                // Сохранить документ
                FileDirectory();
                string outputFilename = outputFn + "." + "docx";
                object filename = outputFilename;
                document.SaveAs2(ref filename);
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void SaveToExcel(System.Data.DataTable data)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                Microsoft.Office.Interop.Excel.Workbook workBook = excel.Workbooks.Add(Type.Missing);
                //object missing = Type.Missing;
                Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Test";

                System.Data.DataTable dataTable = data;
                TableArone.ItemsSource = dataTable.DefaultView;
                workSheet.Cells.Font.Size = 14;

                int rowCount = 1;
                for (int i = 1; i < dataTable.Columns.Count; i++)
                {
                    workSheet.Cells[1, i] = dataTable.Columns[i - 1].ColumnName;
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    rowCount += 1;
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        workSheet.Cells[rowCount, i + 1] = row[i].ToString();
                    }
                }

                Microsoft.Office.Interop.Excel.Range cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowCount, dataTable.Columns.Count]];
                cellRange.EntireColumn.AutoFit();

                FileDirectory();
                string outputFilename = outputFn + "." + "xlsx";
                
                workBook.SaveAs(outputFilename, Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value,
                    Missing.Value, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                    Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlUserResolution, true,
                    Missing.Value, Missing.Value, Missing.Value);
                
                workBook.Close();
                excel.Quit();
            }
            catch { }
        }

        private void DeleteData_Click(object sender, RoutedEventArgs e)
        {
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.DeleteMemory(Bank);
                    ClassLibrary_ARONE.ARONE.memTwo.Clear();
                    break;
                case 1:
                    ClassLibrary_ARONESecond.DeleteMemory(Bank);
                    ClassLibrary_ARONESecond.ARONE.memTwo.Clear();
                    break;
            }

            Data.Clear();
            list.Clear();
            Thread.Sleep(10);            
        }

        private void TableArone_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            switch (dllArOne)
            {
                case 0:
                    DataToReceiverOne();
                    break;
                case 1:
                    DataToReceiverTwo();
                    break;
            }
        }

        private void DataToReceiverOne()
        {
            //SaveChanges.Visibility = Visibility.Visible;
            if (TableArone.SelectedItem != null)
            {
                TableArone path = TableArone.SelectedItem as TableArone;
                string freq = path.Frequency;
                double.TryParse(freq, out double frqq);
                if ((frqq > 0.09) && (frqq < 3300))
                {
                    ClassLibrary_ARONE.FrequencySet(frqq);
                }
                Thread.Sleep(20);
                string mod = path.Mode;
                switch (mod)
                {
                    case "FM": ClassLibrary_ARONE.ModeSet(0); break;
                    case "AM": ClassLibrary_ARONE.ModeSet(1); break;
                    case "CW": ClassLibrary_ARONE.ModeSet(2); break;
                    case "USB": ClassLibrary_ARONE.ModeSet(3); break;
                    case "LSB": ClassLibrary_ARONE.ModeSet(4); break;
                    case "WFM": ClassLibrary_ARONE.ModeSet(5); break;
                    case "NFM": ClassLibrary_ARONE.ModeSet(6); break;
                }
                Thread.Sleep(5);

                string bw = path.BW;
                switch (bw)
                {
                    case "0.5": ClassLibrary_ARONE.BandWidthSet(0); break;
                    case "3.0": ClassLibrary_ARONE.BandWidthSet(1); break;
                    case "6.0": ClassLibrary_ARONE.BandWidthSet(2); break;
                    case "8.5": ClassLibrary_ARONE.BandWidthSet(3); break;
                    case "16": ClassLibrary_ARONE.BandWidthSet(4); break;
                    case "30": ClassLibrary_ARONE.BandWidthSet(5); break;
                    case "100": ClassLibrary_ARONE.BandWidthSet(6); break;
                    case "200": ClassLibrary_ARONE.BandWidthSet(7); break;
                    case "300": ClassLibrary_ARONE.BandWidthSet(8); break;
                }
                Thread.Sleep(50);
            }            
        }

        private void DataToReceiverTwo()
        {
            //SaveChanges.Visibility = Visibility.Visible;
            TableArone path = TableArone.SelectedItem as TableArone;
            string freq = path.Frequency;
            double.TryParse(freq, out double frqq);
            if ((frqq > 0.09) && (frqq < 3300))
            {
                ClassLibrary_ARONESecond.FrequencySet(frqq);
            }
            Thread.Sleep(20);
            string mod = path.Mode;
            switch (mod)
            {
                case "FM": ClassLibrary_ARONESecond.ModeSet(0); break;
                case "AM": ClassLibrary_ARONESecond.ModeSet(1); break;
                case "CW": ClassLibrary_ARONESecond.ModeSet(2); break;
                case "USB": ClassLibrary_ARONESecond.ModeSet(3); break;
                case "LSB": ClassLibrary_ARONESecond.ModeSet(4); break;
                case "WFM": ClassLibrary_ARONESecond.ModeSet(5); break;
                case "NFM": ClassLibrary_ARONESecond.ModeSet(6); break;
            }
            Thread.Sleep(5);

            string bw = path.BW;
            switch (bw)
            {
                case "0.5": ClassLibrary_ARONESecond.BandWidthSet(0); break;
                case "3.0": ClassLibrary_ARONESecond.BandWidthSet(1); break;
                case "6.0": ClassLibrary_ARONESecond.BandWidthSet(2); break;
                case "8.5": ClassLibrary_ARONESecond.BandWidthSet(3); break;
                case "16": ClassLibrary_ARONESecond.BandWidthSet(4); break;
                case "30": ClassLibrary_ARONESecond.BandWidthSet(5); break;
                case "100": ClassLibrary_ARONESecond.BandWidthSet(6); break;
                case "200": ClassLibrary_ARONESecond.BandWidthSet(7); break;
                case "300": ClassLibrary_ARONESecond.BandWidthSet(8); break;
            }
            Thread.Sleep(50);
        }

        private void TableArone_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {
            //if (e.Column.DisplayIndex != 6)
            //{
            //    e.Cancel = true;                
            //}
        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {            
            TableArone path = TableArone.SelectedItem as TableArone;
            
            string adress = path?.Adress;
            string md = path?.Mode;
            int msIndex = 0;
            switch (md)
            {
                case "FM": msIndex = 0; break;
                case "AM": msIndex = 1; break;
                case "CW": msIndex = 2; break;
                case "USB": msIndex = 3; break;
                case "LSB": msIndex = 4; break;
                case "WFM": msIndex = 5; break;
                case "NFM": msIndex = 6; break;
            }
            string bw = path?.BW;
            int bwIndex = 0;
            switch (bw)
            {
                case "0.5 ": bwIndex = 0; break;
                case "3.0 ": bwIndex = 1; break;
                case "6.0 ": bwIndex = 2; break;
                case "8.5 ": bwIndex = 3; break;
                case "16 ": bwIndex = 4; break;
                case "30 ": bwIndex = 5; break;
                case "100 ": bwIndex = 6; break;
                case "200 ": bwIndex = 7; break;
                case "300 ": bwIndex = 8; break;
            }
            //if ((msIndex != ClassLibrary_ARONE.ARONE.Mode) ||(bwIndex != ClassLibrary_ARONE.ARONE.Bandwidth))
            //{
            //    ClassLibrary_ARONE.MemoryDataSettingSet(adress?.ToString());
            //}
            SaveChanges.Visibility = Visibility.Collapsed;
        }

        private void ComboboxMode()
        {
            
        }

        private void ChBoxFree_Checked(object sender, RoutedEventArgs e)
        {
            tBoxFree.IsEnabled = true;
            ButFreeUp.IsEnabled = true;
            ButFreeDown.IsEnabled = true;
            double delayF = 0;
            double.TryParse(tBoxFree.Text, out delayF);
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.FreeScanSet(delayF);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.FreeScanSet(delayF);
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void ChBoxFree_Unchecked(object sender, RoutedEventArgs e)
        {
            tBoxFree.IsEnabled = false;
            ButFreeUp.IsEnabled = false;
            ButFreeDown.IsEnabled = false;
            double delayFreeOff = 0.0;
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.FreeScanSet(delayFreeOff);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.FreeScanSet(delayFreeOff);
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void DecodedFreeScan()
        {
            switch (dllArOne)
            {
                case 0:
                    if (ClassLibrary_ARONE.ARONE.FreeScan == 0)
                    {
                        chBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            chBoxFree.IsChecked = false;
                        });
                        tBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            tBoxFree.IsEnabled = false;
                            ButFreeUp.IsEnabled = false;
                            ButFreeDown.IsEnabled = false;
                        });
                        Thread.Sleep(20);
                    }
                    else
                    {
                        chBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            chBoxFree.IsChecked = true;
                        });
                        tBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            tBoxFree.IsEnabled = true;
                            string textFree = ClassLibrary_ARONE.ARONE.FreeScan.ToString();
                            if (textFree.Length == 1) textFree = textFree + ",0";
                            tBoxFree.Text = textFree;
                            ButFreeUp.IsEnabled = true;
                            ButFreeDown.IsEnabled = true;
                        });
                        Thread.Sleep(20);
                    }
                    break;
                case 1:
                    if (ClassLibrary_ARONESecond.ARONE.FreeScan == 0)
                    {
                        chBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            chBoxFree.IsChecked = false;
                        });
                        tBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            tBoxFree.IsEnabled = false;
                            ButFreeUp.IsEnabled = false;
                            ButFreeDown.IsEnabled = false;
                        });
                        Thread.Sleep(20);
                    }
                    else
                    {
                        chBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            chBoxFree.IsChecked = true;
                        });
                        tBoxFree.Dispatcher.Invoke(delegate ()
                        {
                            tBoxFree.IsEnabled = true;
                            string textFree = ClassLibrary_ARONESecond.ARONE.FreeScan.ToString();
                            if (textFree.Length == 1) textFree = textFree + ",0";
                            tBoxFree.Text = textFree;
                            ButFreeUp.IsEnabled = true;
                            ButFreeDown.IsEnabled = true;
                        });
                        Thread.Sleep(20);
                    }
                    break;
            }
           
        }

        private void tBoxFree_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string num1 = tBoxFree.Text.Substring(0, 1);
                string num2 = tBoxFree.Text.Substring(2, 1);
                string result = num1 + "." + num2;
                double.TryParse(tBoxFree.Text, out double delayF);
                //double delayF = Convert.ToDouble(tBoxFree.Text);

                switch (dllArOne)
                {
                    case 0:
                        ClassLibrary_ARONE.FreeScanSet(delayF);
                        break;
                    case 1:
                        ClassLibrary_ARONESecond.FreeScanSet(delayF);
                        break;
                }
                
                Thread.Sleep(20);
            }
        }

        private void tBoxFree_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
               && (!tBoxFree.Text.Contains(",")
               && tBoxFree.Text.Length != 0)))
            {
                e.Handled = true;
                //Keyboard.ClearFocus();
            }
        }

        private void chBoxHold_Checked(object sender, RoutedEventArgs e)
        {
            tBoxDelay.IsEnabled = false;
            ButDelayUp.IsEnabled = false;
            ButDelayDown.IsEnabled = false;
            string delay = "FF";
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.DelayTime_ScanDelaySet(delay);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.DelayTime_ScanDelaySet(delay);
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void chBoxHold_Unchecked(object sender, RoutedEventArgs e)
        {
            tBoxDelay.IsEnabled = true;
            ButDelayUp.IsEnabled = true;
            ButDelayDown.IsEnabled = true;
            string d1 = tBoxDelay.Text.Substring(0, 1);
            string d2 = tBoxDelay.Text.Substring(2, 1);
            string tData = d1 + "." + d2;
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.DelayTime_ScanDelaySet(tData);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.DelayTime_ScanDelaySet(tData);
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void tBoxDelay_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
               && (!tBoxDelay.Text.Contains(",")
               && tBoxDelay.Text.Length != 0)))
            {
                e.Handled = true;
                //Keyboard.ClearFocus();
            }
        }

        private void tBoxDelay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string d1 = tBoxDelay.Text.Substring(0, 1);
                string d2 = tBoxDelay.Text.Substring(2, 1);
                string tData = d1 + "." + d2;
                switch (dllArOne)
                {
                    case 0:
                        ClassLibrary_ARONE.DelayTime_ScanDelaySet(tData);
                        break;
                    case 1:
                        ClassLibrary_ARONESecond.DelayTime_ScanDelaySet(tData);
                        break;
                }
                
                Thread.Sleep(20);
            }
        }

        private void DelayScan()
        {
            switch (dllArOne)
            {
                case 0:
                    if (ClassLibrary_ARONE.ARONE.DelayTime == "FF")
                    {
                        chBoxHold.Dispatcher.Invoke(delegate ()
                        {
                            chBoxHold.IsChecked = true;
                        });
                        tBoxDelay.Dispatcher.Invoke(delegate ()
                        {
                            tBoxDelay.IsEnabled = false;
                            ButDelayUp.IsEnabled = false;
                            ButDelayDown.IsEnabled = false;
                        });
                    }
                    else
                    {
                        chBoxHold.Dispatcher.Invoke(delegate ()
                        {
                            chBoxHold.IsChecked = false;
                        });
                        tBoxDelay.Dispatcher.Invoke(delegate ()
                        {
                            tBoxDelay.IsEnabled = true;
                            ButDelayUp.IsEnabled = true;
                            ButDelayDown.IsEnabled = true;
                        });
                    }
                    break;
                case 1:
                    if (ClassLibrary_ARONESecond.ARONE.DelayTime == "FF")
                    {
                        chBoxHold.Dispatcher.Invoke(delegate ()
                        {
                            chBoxHold.IsChecked = true;
                        });
                        tBoxDelay.Dispatcher.Invoke(delegate ()
                        {
                            tBoxDelay.IsEnabled = false;
                            ButDelayUp.IsEnabled = false;
                            ButDelayDown.IsEnabled = false;
                        });
                    }
                    else
                    {
                        chBoxHold.Dispatcher.Invoke(delegate ()
                        {
                            chBoxHold.IsChecked = false;
                        });
                        tBoxDelay.Dispatcher.Invoke(delegate ()
                        {
                            tBoxDelay.IsEnabled = true;
                            ButDelayUp.IsEnabled = true;
                            ButDelayDown.IsEnabled = true;
                        });
                    }
                    break;
            }
        }

        private void bPeleng_Click(object sender, RoutedEventArgs e)
        {
            UserControl1.ReadFrqBwPeleng();
            Int16 Id = 2;
            double width = 0.0;
            switch (BW)
            {
                case 0: width = 0.5 / 1000.0; break;// МГц"; 
                case 1: width = 3.0 / 1000.0; break; //МГц"; 
                case 2: width = 6.0 / 1000.0; break; //МГц"; 
                case 3: width = 8.5 / 1000.0; break; //МГц";
                case 4: width = 16 / 1000.0; break; //МГц";
                case 5: width = 30.0 / 1000.0; break; //МГц"; 
                case 6: width = 100.0 / 1000.0; break; //МГц"; 
                case 7: width = 200.0 / 1000.0; break; //МГц"; 
                case 8: width = 300.0 / 1000.0; break; //МГц";                 
            }
            OnFreqSupress?.Invoke(this, new ToSupressEventArgs(Frequency, width, (int)SignalLvl, Id));
        }

        private void bPodavleniye_Click(object sender, RoutedEventArgs e)
        {
            UserControl1.ReadFrqBwPeleng();
            Int16 Id = 0;
            OnFreqSupress?.Invoke(this, new ToSupressEventArgs(Frequency, BW, (int)SignalLvl, Id));
        }

        private void bCR_Click(object sender, RoutedEventArgs e)
        {
            UserControl1.ReadFrqBwPeleng();
            Int16 Id = 1;
            double width = 0.0;
            switch (BW)
            {
                case 0: width = 0.5 / 1000.0; break;// МГц"; 
                case 1: width = 3.0 / 1000.0; break; //МГц"; 
                case 2: width = 6.0 / 1000.0; break; //МГц"; 
                case 3: width = 8.5 / 1000.0; break; //МГц";
                case 4: width = 16 / 1000.0; break; //МГц";
                case 5: width = 30.0 / 1000.0; break; //МГц"; 
                case 6: width = 100.0 / 1000.0; break; //МГц"; 
                case 7: width = 200.0 / 1000.0; break; //МГц"; 
                case 8: width = 300.0 / 1000.0; break; //МГц";                 
            }
            OnFreqSupress?.Invoke(this, new ToSupressEventArgs(Frequency, width, (int)SignalLvl, Id));
        }

        private void ButDelayUp_Click(object sender, RoutedEventArgs e)
        {            
            double.TryParse(tBoxDelay.Text, out double delayUp);
            delayUp = delayUp + 0.1;
            if (delayUp > 9.9) delayUp = 9.9;            
            string dUp = delayUp.ToString();
            if (dUp.Length == 1)
            {
                dUp = dUp + ",0";
            }
            tBoxDelay.Text = null;
            tBoxDelay.Text = dUp.Substring(0, 1) + "," + dUp.Substring(2, 1);
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.DelayTime_ScanDelaySet(dUp.Substring(0, 1) + "." + dUp.Substring(2, 1));
                    break;
                case 1:
                    ClassLibrary_ARONESecond.DelayTime_ScanDelaySet(dUp.Substring(0, 1) + "." + dUp.Substring(2, 1));
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void ButDelayDown_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxDelay.Text, out double delayDown);
            delayDown = delayDown - 0.1;
            if (delayDown < 0.0) delayDown = 0.0;
            string dDown = delayDown.ToString();
            if (dDown.Length == 1)
            {
                dDown = dDown + ",0";
            }
            tBoxDelay.Text = null;
            tBoxDelay.Text = dDown.Substring(0, 1) + "," + dDown.Substring(2, 1);
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.DelayTime_ScanDelaySet(dDown.Substring(0, 1) + "." + dDown.Substring(2, 1));
                    break;
                case 1:
                    ClassLibrary_ARONESecond.DelayTime_ScanDelaySet(dDown.Substring(0, 1) + "." + dDown.Substring(2, 1));
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void ButFreeUp_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxFree.Text, out double freeUp);
            freeUp = freeUp + 0.1;
            if (freeUp > 9.9) freeUp = 9.9;
            string fUp = freeUp.ToString();
            if (fUp.Length == 1) fUp = fUp + ",0";
            tBoxFree.Text = null;
            tBoxFree.Text = fUp.Substring(0, 1) + "," + fUp.Substring(2, 1);
            double.TryParse(tBoxFree.Text, out double fU);
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.FreeScanSet(fU);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.FreeScanSet(fU);
                    break;
            }
            
            Thread.Sleep(20);
        }

        private void ButFreeDown_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxFree.Text, out double freeDown);
            freeDown = freeDown - 0.1;
            if (freeDown < 0.1) freeDown = 0.1;
            string fDown = freeDown.ToString();
            if (fDown.Length == 1) fDown = fDown + ",0";
            tBoxFree.Text = null;
            tBoxFree.Text = fDown.Substring(0, 1) + "," + fDown.Substring(2, 1);
            double.TryParse(tBoxFree.Text, out double fd);
            switch (dllArOne)
            {
                case 0:
                    ClassLibrary_ARONE.FreeScanSet(fd);
                    break;
                case 1:
                    ClassLibrary_ARONESecond.FreeScanSet(fd);
                    break;
            }
            
            Thread.Sleep(20);
        }

        
    }
}
