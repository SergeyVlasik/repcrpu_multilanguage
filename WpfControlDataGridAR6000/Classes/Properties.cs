﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        public int Bank
        {
            get => bank;
            set { bank = value; }
        }
        int bank = 1;

        Int64 freq = 102500;
        public Int64 Frequency
        {
            get => freq;
            set { freq = value; }
        }

        private int Bw = 0;
        public int Bandwidth 
        {
            get;
            set; 
        }

        private int mod = 0;
        public int Modulation 
        {
            get => mod;
            set { mod = value; }
        }

        private int att = 0;
        public int Attenuator 
        {
            get => att;
            set { att = value; }
        }
        private double sLvl = -120;
        public double SignalLvl 
        {
            get => sLvl;
            set { sLvl = value; }
        }

        private double sqlLvl = -120;
        public double SqlLvl 
        {
            get => sqlLvl;
            set { sqlLvl = value; }
        }

        string dB = "dB";       
        string UsilVKL = "RF ON";       
        string Attt = "Att";
        string Avto = "Auto";

        string langMessage = "Rus";
        public string LangMessage
        {
            get { return langMessage; }
            set { langMessage = value; }
        }

        string scanDelay = "0.0";
        public string ScanDelayTime 
        {
            get { return scanDelay; }
            set 
            {
                scanDelay = value;
                UpdateScanData();
            }
        }

        string scanPause = "0.0";
        public string ScanPauseTime 
        {
            get { return scanPause; }
            set 
            {
                scanPause = value;
                UpdateScanData();
            }
        }
    }
}
