﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlDataGridAR6000
{
    public class ScanInformation : EventArgs
    {
        public ScanInformation(string parameter) 
        {
            Parameter = parameter;
        }

        public string Parameter { get; set; }
    }
}
