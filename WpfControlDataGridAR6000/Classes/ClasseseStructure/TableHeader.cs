﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfControlDataGridAR6000
{
    class TableHeader
    {
        public TableHeader(string Adress, string Frequency, string Mode, string Att, string Time, string Teg)
        {
            this.Adress = Adress;
            this.Frequency = Frequency;
            this.Attenuator = Att;
            this.Mode = Mode;
            this.Time = Time;
            this.Teg = Teg;
        }

        public string Adress { get; set; }
        public string Frequency { get; set; }
        public string Attenuator { get; set; }
        public string Mode { get; set; }
        public string Time { get; set; }
        public string Teg { get; set; }
    }
}
