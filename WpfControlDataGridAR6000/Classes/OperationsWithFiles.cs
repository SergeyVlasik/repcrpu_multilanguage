﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AR6000;
using Microsoft.Office.Interop;
using Microsoft.Office.Interop.Word;
using Microsoft.Win32;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        string formatFile = "Word";
        public string FormatFile
        {
            get => formatFile;
            set { formatFile = value; }
        }

        public void TakeFormatFile(string type)
        {
            FormatFile = type;
        }
        private void LoadFile_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void LoadTxtFormat() 
        {
            try
            {
                GridCollection.Clear();

                Ar6000Manager.AOR.memthree.Clear();

                OpenFileDialog openFileDialog = new OpenFileDialog()
                {
                    Filter = "Text file (*.txt)|*.txt"
                };

                if (openFileDialog.ShowDialog() == true)
                {
                    FileInfo fileInfo = new FileInfo(openFileDialog.FileName);
                    StreamReader reader = new StreamReader(fileInfo.Open(FileMode.Open, FileAccess.Read), Encoding.GetEncoding("windows-1251"));
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        var parsed = line.Split('\t');
                        GridCollection.Add(new TableHeader(parsed[0], parsed[1], parsed[2], parsed[3], parsed[4], parsed[5]));
                        Thread.Sleep(20);

                        string freq = parsed[1];
                        double.TryParse(freq, out double frqq);
                        if ((frqq > 0.09) && (frqq < 6000))
                        {
                            //ClassLibrary_ARONE.FrequencySet(frqq);
                        }
                        Thread.Sleep(30);

                        string mod = parsed[2];
                        

                        string att = parsed[3];
                        

                        string adress = parsed[0];
                        /*
                        ClassLibrary_ARONE.MemoryDataSettingSet(adress.ToString());
                        ClassLibrary_ARONE.FrequencyGet();
                        Thread.Sleep(500);
                        */
                    }
                    TableReceiver.ItemsSource = GridCollection;
                }
            }
            catch { }
        }

        private void SaveFile_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnUpdateFileType?.Invoke();
                switch (formatFile)
                {
                    case "Txt":
                        SaveToTxt();
                        break;
                    case "Word":
                        SaveToDocx();
                        break;
                    case "Excel":
                        SaveToExcel(ConvertToDataTable.ToDataTable(GridCollection));
                        break;
                }
            }
            catch { }
        }

        private void FileDirectory()
        {
            string path = String.Format(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                "\\Documents\\" + "ReceiverAR6000\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00"));
            if (!Directory.Exists(path))
            {
                DirectoryInfo directory = Directory.CreateDirectory(path);
            }

            outputFn = path + "\\" + "Bank №" + Bank.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" +
                DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00");
        }

        private void SaveToTxt()
        {
            try 
            {
                StringBuilder stringBuilder = new StringBuilder();

                TableReceiver.SelectAllCells();
                TableReceiver.ClipboardCopyMode = DataGridClipboardCopyMode.ExcludeHeader;
                ApplicationCommands.Copy.Execute(null, TableReceiver);
                TableReceiver.UnselectAllCells();

                string result = (string)Clipboard.GetData(DataFormats.Text);
                Clipboard.Clear();
                FileDirectory();
                string outputFilename = outputFn + "." + "txt";
                stringBuilder.Append(result.ToString());
                File.WriteAllText(outputFilename, result.ToString());
            }
            catch { }            
        }
           
        private void SaveToDocx()
        {
            try
            {
                //Create an instance for word app
                Microsoft.Office.Interop.Word.Application winword = new Microsoft.Office.Interop.Word.Application();

                //Set animation status for word application
                //winword.ShowAnimation = false;

                //Set status for word application is to be visible or not.
                winword.Visible = false;

                //Create a missing variable for missing value
                object missing = Missing.Value;

                //Create a new document
                Document document = winword.Documents.Add(ref missing, ref missing, ref missing, ref missing);

                string[,] Table = new string[TableReceiver.Items.Count + 1, TableReceiver.Columns.Count];

                //Add header into the document
                string header = "Table Receiver";
                foreach (Microsoft.Office.Interop.Word.Section section in document.Sections)
                {

                    //Get the header range and add the header details.
                    Range headerRange = section.Headers[WdHeaderFooterIndex.wdHeaderFooterPrimary].Range;
                    headerRange.Fields.Add(headerRange, WdFieldType.wdFieldPage);
                    headerRange.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphLeft;//.wdAlignParagraphCenter;
                    headerRange.Font.ColorIndex = WdColorIndex.wdBlue;
                    headerRange.Font.Size = 14;
                    headerRange.Text = "";
                }

                //Add paragraph with Heading 1 style
                Microsoft.Office.Interop.Word.Paragraph para1 = document.Content.Paragraphs.Add(ref missing);
                para1.Range.Font.Size = 14;
                //string curTime = "Время               " + DateTime.Now.ToString(@"HH:mm:ss");
                string curTime = DateTime.Today.ToLongDateString() + "   " + DateTime.Now.ToString(@"HH:mm:ss");
                para1.Range.Text = curTime;
                para1.Range.InsertParagraphAfter();

                //Create table and insert some dummy record
                Microsoft.Office.Interop.Word.Table firstTable = document.Tables.Add(para1.Range, Table.GetLength(0), Table.GetLength(1), ref missing, ref missing);
                firstTable.Borders.Enable = 1;

                foreach (Row row in firstTable.Rows)
                {

                    foreach (Cell cell in row.Cells)
                    {
                        //Header row
                        if (cell.RowIndex == 1)
                        {
                            //TextBlock cellContent = TableArone.Columns[cell.ColumnIndex - 1].GetCellContent(TableArone.Items[0]) as TextBlock;
                            TextBlock cellHeader = TableReceiver.Columns[cell.ColumnIndex - 1].Header as TextBlock;
                            cell.Range.Text = cell.Range.Text.Replace("\r", "");
                            cell.Range.Text = cell.Range.Text.Replace("\a", "");
                            cell.Range.Text = cellHeader.Text;
                            cell.Range.Font.Bold = 1;

                            //other format properties goes here
                            cell.Range.Font.Name = "verdana";
                            cell.Range.Font.Size = 10;
                            cell.Shading.BackgroundPatternColor = WdColor.wdColorGray25;

                            //Center alignment for the Header cells
                            cell.VerticalAlignment = WdCellVerticalAlignment.wdCellAlignVerticalCenter;
                            cell.Range.ParagraphFormat.Alignment = WdParagraphAlignment.wdAlignParagraphCenter;
                        }
                        //Data row
                        else
                        {
                            try
                            {
                                if (TableReceiver.Columns[cell.ColumnIndex - 1].GetCellContent(TableReceiver.Items[cell.RowIndex - 2]) is TextBlock)
                                {
                                    TextBlock cellContent = TableReceiver.Columns[cell.ColumnIndex - 1].GetCellContent(TableReceiver.Items[cell.RowIndex - 2]) as TextBlock;
                                    cell.Range.Text = cell.Range.Text.Replace("\r", "");
                                    cell.Range.Text = cell.Range.Text.Replace("\a", "");
                                    cell.Range.Text = cellContent.Text;
                                }
                                //Thread.Sleep(100);
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }

                        }
                    }
                }

                // Сохранить документ
                FileDirectory();
                string outputFilename = outputFn + "." + "docx";
                object filename = outputFilename;
                document.SaveAs2(ref filename);
                document.Close(ref missing, ref missing, ref missing);
                document = null;
                winword.Quit(ref missing, ref missing, ref missing);
                winword = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void SaveToExcel(System.Data.DataTable data) 
        {
            try
            {
                Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
                excel.DisplayAlerts = false;
                excel.Visible = false;
                Microsoft.Office.Interop.Excel.Workbook workBook = excel.Workbooks.Add(Type.Missing);
                //object missing = Type.Missing;
                Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)workBook.ActiveSheet;
                workSheet.Name = "Test";

                System.Data.DataTable dataTable = data;
                TableReceiver.ItemsSource = dataTable.DefaultView;
                workSheet.Cells.Font.Size = 14;

                int rowCount = 1;
                for (int i = 1; i < dataTable.Columns.Count; i++)
                {
                    workSheet.Cells[1, i] = dataTable.Columns[i - 1].ColumnName;
                }
                foreach (DataRow row in dataTable.Rows)
                {
                    rowCount += 1;
                    for (int i = 0; i < dataTable.Columns.Count; i++)
                    {
                        workSheet.Cells[rowCount, i + 1] = row[i].ToString();
                    }
                }

                Microsoft.Office.Interop.Excel.Range cellRange = workSheet.Range[workSheet.Cells[1, 1], workSheet.Cells[rowCount, dataTable.Columns.Count]];
                cellRange.EntireColumn.AutoFit();

                FileDirectory();
                string outputFilename = outputFn + "." + "xlsx";

                workBook.SaveAs(outputFilename, Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook, Missing.Value,
                    Missing.Value, false, false, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlNoChange,
                    Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlUserResolution, true,
                    Missing.Value, Missing.Value, Missing.Value);

                workBook.Close();
                excel.Quit();
            }
            catch { }
        }
    }
}
