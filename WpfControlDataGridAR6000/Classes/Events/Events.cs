﻿using System;
using System.Windows.Input;
using System.Windows.Controls;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        public event EventHandler<ToSupressEventArgs> OnFreqSupress;

        public delegate void MyControlEventHandler();
        public event MyControlEventHandler OnUpdateBank;
        public event MyControlEventHandler OnUpdateFileType;

        public delegate void SendScanInformation(string data);
        //public event SendScanInformation OnSendScanPauseTime = (data) => { };
        //public event SendScanInformation OnSendScanDelayTime = (data) => { };

        public event EventHandler<ScanInformation> OnSendScanPauseTime;
        public event EventHandler<ScanInformation> OnSendScanDelayTime;

        public delegate void RequestReceiverStatus(bool value);
        public event RequestReceiverStatus OnRequestReceiverStatus = (value) => { };
    }
}
