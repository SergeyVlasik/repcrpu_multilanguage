﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        private void UpdateScanData() 
        {
            DispatchIfNecessary(() => 
            {
                tBoxDelay.Text = ScanDelayTime.Replace('.', ',');
                tBoxFree.Text = ScanPauseTime;
            });
        }

        #region DELAY Properies
        private void ButDelayUp_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxDelay.Text, out double delayUp);
            delayUp = delayUp + 0.1;
            if (delayUp > 9.9) delayUp = 9.9;
            string dUp = delayUp.ToString();
            if (dUp.Length == 1)
            {
                dUp = dUp + ",0";
            }
            tBoxDelay.Text = null;
            tBoxDelay.Text = dUp.Substring(0, 1) + "," + dUp.Substring(2, 1);
            var message = (delayUp * 10).ToString().PadLeft(2, '0');
            OnSendScanDelayTime?.Invoke(this, new ScanInformation(message));
            Thread.Sleep(20);
        }

        private void ButDelayDown_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxDelay.Text.Replace('.', ','), out double delayDown);
            delayDown = delayDown - 0.1;
            if (delayDown < 0.0) delayDown = 0.0;
            string dDown = delayDown.ToString();
            if (dDown.Length == 1)
            {
                dDown = dDown + ",0";
            }
            tBoxDelay.Text = null;
            tBoxDelay.Text = dDown.Substring(0, 1) + "," + dDown.Substring(2, 1);
            var message = (delayDown * 10).ToString().PadLeft(2, '0');
            OnSendScanDelayTime?.Invoke(this, new ScanInformation(message));
            Thread.Sleep(20);
        }

        private void tBoxDelay_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
               && (!tBoxDelay.Text.Contains(",")
               && tBoxDelay.Text.Length != 0)))
            {
                e.Handled = true;
            }
        }

        private void tBoxDelay_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string d1 = tBoxDelay.Text.Substring(0, 1);
                string d2 = tBoxDelay.Text.Substring(2, 1);
                string tData = d1 + d2;
                OnSendScanDelayTime?.Invoke(this, new ScanInformation(tData));
                Thread.Sleep(20);
            }
        }

        private void chBoxHold_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void chBoxHold_Unchecked(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region PAUSE Properties
        private void ButFreeUp_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxFree.Text, out double freeUp);
            freeUp = freeUp + 1;
            if (freeUp > 60) freeUp = 60;
            string fUp = freeUp.ToString();
            
            tBoxFree.Text = null;
            tBoxFree.Text = fUp.PadLeft(2, '0');

            OnSendScanPauseTime?.Invoke(this, new ScanInformation(tBoxFree.Text));
            Thread.Sleep(20);
        }

        private void ButFreeDown_Click(object sender, RoutedEventArgs e)
        {
            double.TryParse(tBoxFree.Text, out double freeDown);
            freeDown = freeDown - 1;
            if (freeDown < 0) freeDown = 0;
            string fDown = freeDown.ToString();
            
            tBoxFree.Text = null;
            tBoxFree.Text = fDown.PadLeft(2, '0');

            OnSendScanPauseTime?.Invoke(this, new ScanInformation(tBoxFree.Text));
            Thread.Sleep(20);
        }



        private void tBoxFree_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                OnSendScanPauseTime?.Invoke(this, new ScanInformation(tBoxFree.Text.Substring(0, 2)));
                Thread.Sleep(20);
            }
        }

        private void tBoxFree_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
              && (!tBoxFree.Text.Contains(",")
              && tBoxFree.Text.Length != 0)))
            {
                e.Handled = true;
                //Keyboard.ClearFocus();
            }
        }

        private void ChBoxFree_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void ChBoxFree_Unchecked(object sender, RoutedEventArgs e)
        {

        }
        #endregion
    }
}
