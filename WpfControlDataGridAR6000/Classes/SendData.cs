﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        private void bPodavleniye_Click(object sender, RoutedEventArgs e)
        {
            short Id = 0;
            OnFreqSupress?.Invoke(this, new ToSupressEventArgs(Frequency, Bandwidth, (int)SignalLvl, Id));
        }

        private void bCR_Click(object sender, RoutedEventArgs e)
        {
            short Id = 1;
            double width = 0.0;
            switch (Bandwidth)
            {
                case 0: width = 0.2 / 1000.0; break;// МГц"; 
                case 1: width = 0.5 / 1000.0; break; //МГц"; 
                case 2: width = 1.0 / 1000.0; break; //МГц"; 
                case 3: width = 3.0 / 1000.0; break; //МГц";
                case 4: width = 6.0 / 1000.0; break; //МГц";
                case 5: width = 15.0 / 1000.0; break; //МГц"; 
                case 6: width = 30.0 / 1000.0; break; //МГц"; 
                case 7: width = 100.0 / 1000.0; break; //МГц";    
                case 8: width = 200.0 / 1000.0; break; //МГц"; 
                case 9: width = 300.0 / 1000.0; break; //МГц";      
            }
            OnFreqSupress?.Invoke(this, new ToSupressEventArgs(Frequency, width, (int)SignalLvl, Id));
        }

        private void bPeleng_Click(object sender, RoutedEventArgs e)
        {
            short Id = 2;
            double width = 0.0;
            switch (Bandwidth)
            {
                case 0: width = 0.2 / 1000.0; break;// МГц"; 
                case 1: width = 0.5 / 1000.0; break; //МГц"; 
                case 2: width = 1.0 / 1000.0; break; //МГц"; 
                case 3: width = 3.0 / 1000.0; break; //МГц";
                case 4: width = 6.0 / 1000.0; break; //МГц";
                case 5: width = 15.0 / 1000.0; break; //МГц"; 
                case 6: width = 30.0 / 1000.0; break; //МГц"; 
                case 7: width = 100.0 / 1000.0; break; //МГц";    
                case 8: width = 200.0 / 1000.0; break; //МГц"; 
                case 9: width = 300.0 / 1000.0; break; //МГц";                  
            }
            OnFreqSupress?.Invoke(this, new ToSupressEventArgs(Frequency, width, (int)SignalLvl, Id));
        }
    }
}
