﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        private WaveFileWriter writer;
        private WaveIn waveIn;

        string path;
        static string outputFilename = "";
        static string outputFn;
        static int Fs = 44100;

        private int _deviceNumber = 0;
        public int DeviceNumber 
        {
            get => _deviceNumber;
            set
            {
                _deviceNumber = value;
                waveIn.DeviceNumber = _deviceNumber;
                ConnectAudio();
            }
        }

        public void ConnectAudio()
        {
            if (WaveIn.DeviceCount != 0)
            {                
                try
                {
                    waveIn = new WaveIn();
                    var devicescount = WaveIn.DeviceCount;
                    for(int i=0; i < devicescount; i++) 
                    {
                        WaveInCapabilities inCapabilities = WaveIn.GetCapabilities(i); 
                    }
                  
                    waveIn.DeviceNumber = _deviceNumber;
                    waveIn.WaveFormat = new WaveFormat(Fs, 16, 1);
                    waveIn.DataAvailable += WaveIn_DataAvailable;                    

                    waveIn.StartRecording();   
                }
                catch (Exception ex) {}
            }
        }

        private void WaveIn_RecordingStopped(object sender, StoppedEventArgs e)
        {

        }

        public void DisconnectAudio() 
        {
            waveIn.StopRecording();
            waveIn.DataAvailable -= WaveIn_DataAvailable;
            writer.Close();
            writer.Dispose();
        }

        private void FileDirectoryWrite()
        {
            long freqPath;
            if (bScan.IsChecked == true)
            {
                freqPath = _ScanFreq;
            }
            else { freqPath = Frequency; }
            
            path = String.Format(Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location) +
                "\\Wav\\" + DateTime.Now.Year.ToString("0000") + "-" + DateTime.Now.Month.ToString("00") + "-" + DateTime.Now.Day.ToString("00") + "\\" + "AR6000Manager");

            if (!Directory.Exists(path))
            {
                DirectoryInfo directory = Directory.CreateDirectory(path);
            }

            outputFn = path + "\\" + freqPath.ToString() + "_" + DateTime.Now.Hour.ToString("00") + "-" +
                DateTime.Now.Minute.ToString("00") + "-" + DateTime.Now.Second.ToString("00");
            outputFilename = outputFn + ".wav";        
        }

        private void TBWrite_Checked(object sender, RoutedEventArgs e) 
        {
            FileDirectoryWrite();
            
            writer = new WaveFileWriter(outputFilename, waveIn.WaveFormat);
        }

        private void TBWrite_Unchecked(object sender, RoutedEventArgs e) 
        {
            if (writer != null)
            {
                writer.Close();
                writer = null;

                ResampleAsync();
                //DisconnectAudio();
            }
        }

        private void WaveIn_DataAvailable(object sender, WaveInEventArgs e)
        {
            if (writer != null) writer.WriteData(e.Buffer, 0, e.BytesRecorded);
            if (TBAutoWrite.IsChecked == true) { AutoWrite(); }            
        }

        private void TBAutoWrite_Checked(object sender, RoutedEventArgs e) 
        {
            try 
            {
                //waveIn.DataAvailable += WaveIn_DataAvailable;
            }
            catch { }
        }

        private void TBAutoWrite_Unchecked(object sender, RoutedEventArgs e) 
        {
            try 
            {
                TBWrite.IsChecked = false;
                //waveIn.DataAvailable -= WaveIn_DataAvailable;
            }
            catch { }
        }

        private void AutoWrite() 
        {
            try
            {
                if (TBAutoWrite.IsChecked == true) 
                {
                    if (SignalLvl > SqlLvl)
                    {
                        if (TBWrite.IsChecked == false) { TBWrite.IsChecked = true; }
                    }
                    else
                    {
                        if (TBWrite.IsChecked == true) { TBWrite.IsChecked = false; }
                    }
                }
                else { }
            }
            catch { }
        }

        static async void ResampleAsync()
        {
            await Task.Run(() => Resample());
        }

        static private void Resample()
        {
            try
            {
                var reader = new WaveFileReader(outputFilename);
                var newFormat = new WaveFormat(8000, 8, 1);
                var convert = new WaveFormatConversionStream(newFormat, reader);
                WaveFileWriter.CreateWaveFile(outputFn + "Jamming.wav", convert);
                convert.Dispose();
            }
            catch { /*NAudio.Wave.Compression.AcmStreamHeader.Finalize();*/ }
        }

    }
}
