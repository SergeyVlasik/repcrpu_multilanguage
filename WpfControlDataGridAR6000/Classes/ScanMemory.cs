﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        private Int64 _ScanFreq = (long)100.5;
        public Int64 ScanFrequency 
        {
            get => _ScanFreq;
            set
            {
                _ScanFreq = value;
            }
        }
        #region asyncScan
        private bool isStatus = false;
        CancellationTokenSource cts = new CancellationTokenSource();
        private int _CancellationTokenDelay = 100;
        public int CancellationTokenDelay
        {
            get { return _CancellationTokenDelay; }
            set
            {
                if (_CancellationTokenDelay != value)
                {
                    _CancellationTokenDelay = value;
                }
            }
        }
        private int _RequestLvlTimer = 10;
        public int RequestLvlTimer
        {
            get { return _RequestLvlTimer; }
            set
            {
                if (_RequestLvlTimer != value)
                {
                    _RequestLvlTimer = value;
                }
            }
        }

        private void TaskManager()
        {
            Task.Run(() => ReceiveModeStatus());
        }

        private async void ReceiveModeStatus() 
        {
            try
            {
                while (isStatus)
                {
                    cts = new CancellationTokenSource();
                    CancellationToken token = cts.Token;

                    OnRequestReceiverStatus?.Invoke(isStatus);

                    var T = await Task.Delay(_CancellationTokenDelay, token).ContinueWith(_ => { return 42; });
                    await Task.Delay(_RequestLvlTimer);
                }
            }
            catch { }
        }
        #endregion

        DispatcherTimer dispatcherTimer1 = new DispatcherTimer();

        private void BScan_Checked(object sender, RoutedEventArgs e)
        {
            isStatus = true;
            TaskManager();

            Ar6000.MemoryReadMode(Bank);
            Thread.Sleep(100);
            Ar6000.StartScanSet(Bank.ToString());
        }

        private void BScan_Unchecked(object sender, RoutedEventArgs e)
        {
            isStatus = false;
            
            Ar6000.StopScanSet();
            OnRequestReceiverStatus?.Invoke(isStatus);
            TableReceiver.SelectedItem = null;
        }

        private void DispatcherTimer1_Tick(object sender, EventArgs e)
        {
            OnRequestReceiverStatus?.Invoke(isStatus);
        }
        
        public void DecodedScanMode(string address) 
        {
            DispatchIfNecessary(() => 
            {
                var rows = GetDataGridRows(TableReceiver);
                foreach (DataGridRow r in rows)
                {
                    if (TableReceiver.Columns[0].GetCellContent(r) is TextBlock)
                    {
                        TextBlock cellContent = TableReceiver.Columns[0].GetCellContent(r) as TextBlock;
                        if (cellContent.Text == address)
                        {
                            TableReceiver.SelectedItem = r.Item;
                            TableReceiver.ScrollIntoView(r.Item);
                            break;
                        }
                    }
                }
            });
        }
        
    }
}
