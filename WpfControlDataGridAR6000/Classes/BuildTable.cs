﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AR6000;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        private void AR6000_OnDecodedMemoryChannelDataRead()
        {
            BuildTable();
        }

        private void BuildTable()
        {
            try 
            {
                DispatchIfNecessary(() =>
                {
                    //list.Clear();

                    GridCollection.Clear();

                    foreach (var element in Ar6000Manager.AOR.memthree)
                    {
                        if (element.Length > 12)
                        {
                            if (element.Substring(0, 2) == "MX")
                            {
                                string adress = element.Substring(2, 4);
                                string time = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00") + ":" + DateTime.Now.Second.ToString("00");
                                string test = element.Substring(45, 12);

                                string mode = null;
                                int.TryParse(element.Substring(30, 2), out int modeIndex);
                                mode = DecMod(modeIndex);

                                string att = null;
                                int.TryParse(element.Substring(35, 2), out int attIndex);
                                att = DecAtt(attIndex);

                                Int64.TryParse(element.Substring(17, 10), out Int64 Freq);
                                string FrequencyAor = (Freq / 1000000).ToString() + "," + (Freq % 1000000).ToString("000").PadLeft(6, '0');
                                GridCollection.Add(new TableHeader(adress, FrequencyAor, mode, att, time, test));
                            }
                        }
                    }
                    TableReceiver.ItemsSource = GridCollection;
                });
            }
            catch { }            
        }

        string DecMod(int mod) 
        {
            string modulation = "";
            switch (mod) 
            {
                case 00: modulation = "FM"; break;
                case 01: modulation = "FMST"; break;
                case 02: modulation = "AM"; break;
                case 03: modulation = "SAM"; break;
                case 04: modulation = "USB"; break;
                case 05: modulation = "LSB"; break;
                case 06: modulation = "CW"; break;
                case 07: modulation = "ISB"; break;
                case 08: modulation = "AIQ"; break;
                case 21: modulation = "WFM"; break;
                case 22: modulation = "WFM"; break;
                case 23: modulation = "FMST"; break;
                case 24: modulation = "NFM"; break;
                case 25: modulation = "SFM"; break;
                case 26: modulation = "WAM"; break;
                case 27: modulation = "AM"; break;
                case 28: modulation = "NAM"; break;
                case 29: modulation = "SAM"; break;
                case 30: modulation = "USB"; break;
                case 31: modulation = "LSB"; break;
                case 32: modulation = "CW1"; break;
                case 33: modulation = "CW2"; break;
                case 34: modulation = "ISB"; break;
                case 35: modulation = "AIQ"; break;
            }
            return modulation;
        }

        string DecAtt(int att) 
        {
            string attenuator = "";
            switch (att)
            {
                case 0: attenuator = UsilVKL; break;// "Усил ВКЛ"; break;
                case 1: attenuator = "0" + dB; break;// "Атт 0дБ"; break;
                case 2: attenuator = "-10" + dB; break;// "Атт -10дБ"; break;
                case 3: attenuator = "-20" + dB; break;//"Атт -20дБ"; break;
                case 4: attenuator = Avto; break;// "Атт Авто"; break;
                case 10: attenuator = Attt + " " + Avto; break;// "Атт Авто"; break;
                case 11: attenuator = Attt + " " + Avto; break;// "Атт Авто"; break;
                case 12: attenuator = Attt + " " + Avto; break; //"Атт Авто"; break;
                case 13: attenuator = Attt + " " + Avto; break; //"Атт Авто"; break;
                case 14: attenuator = Attt + " " + Avto; break; //"Атт Авто"; break;
            }
            return attenuator;
        }
    }
}
