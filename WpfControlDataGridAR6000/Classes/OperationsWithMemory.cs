﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using AR6000;

namespace WpfControlDataGridAR6000
{
    public partial class ControlTable
    {
        private void Update_Click(object sender, RoutedEventArgs e)
        {
            OnUpdateBank?.Invoke();

            Ar6000Manager.AOR.memthree.Clear();
            Ar6000Manager.AOR.MBank = Bank.ToString();

            Ar6000.MemoryChannelDataRead(Bank.ToString().PadLeft(2, '0'));
        }

        private void SaveData_Click(object sender, RoutedEventArgs e)
        {
            try 
            {
                List<string> channels = new List<string>();
                var first = 0;
                var second = 0;

                if(TableReceiver.Items.Count == 0) 
                {
                    var adressFirst = Bank.ToString().PadLeft(2, '0') + "00";
                    Ar6000Manager.MemoryChannelSetting(adressFirst, "00", Frequency, 0, 0, 12.5, 0, Bandwidth, Modulation, Attenuator, 1, " ");
                }
                else 
                {
                    var row = GetDataGridRows(TableReceiver);

                    foreach(DataGridRow r in row) 
                    {
                        if (TableReceiver.Columns[0].GetCellContent(r) is TextBlock) 
                        {
                            TextBlock cellContent = TableReceiver.Columns[0].GetCellContent(r) as TextBlock;
                            channels.Add(cellContent.Text);
                        }
                    }

                    first = int.Parse(channels[0].Substring(2, 2));
                    bool isSaved = false;
                    int nullAddress = 0;

                    if (first != nullAddress) 
                    {
                        //сохраняем по адресу 00
                        var adressFirst = Bank.ToString().PadLeft(2, '0') + "00";
                        Ar6000Manager.MemoryChannelSetting(adressFirst, "00", Frequency, 0, 0, 12.5, 0, Bandwidth, Modulation, Attenuator, 1, " ");
                    }
                    else 
                    {
                        if (channels.Count == 1) 
                        {
                            var adressFirst = Bank.ToString().PadLeft(2, '0') + "01";
                            Ar6000Manager.MemoryChannelSetting(adressFirst, "00", Frequency, 0, 0, 12.5, 0, Bandwidth, Modulation, Attenuator, 1, " ");
                        }
                        else 
                        {
                            for (int i = 1; i < channels.Count; i++)
                            {
                                if (!isSaved)
                                {
                                    second = int.Parse(channels[i].Substring(2, 2)) - first;
                                    if (second == 1)
                                    {
                                        if (i == channels.Count - 1)
                                        {
                                            var channel1 = (int.Parse(channels[i].Substring(2, 2)) + 1).ToString().PadLeft(2, '0');
                                            Ar6000Manager.MemoryChannelSetting(Bank.ToString().PadLeft(2, '0'), channel1, Frequency, 0, 0, 12.5, 0, Bandwidth, Modulation, Attenuator, 1, " ");
                                            isSaved = true;
                                        }
                                    }
                                    else
                                    {
                                        var nextAddress = (first + 1).ToString().PadLeft(2, '0');
                                        Ar6000Manager.MemoryChannelSetting(Bank.ToString().PadLeft(2, '0'), nextAddress, Frequency, 0, 0, 12.5, 0, Bandwidth, Modulation, Attenuator, 1, " ");
                                        isSaved = true;
                                        break;
                                    }
                                    first = int.Parse(channels[i].Substring(2, 2));
                                }
                            }
                        }                        
                    }
                }
                Ar6000Manager.AOR.memthree.Clear();
                Ar6000.MemoryChannelDataRead(Bank.ToString().PadLeft(2, '0'));
            }
            catch { }      
        }

        private void DeleteData_Click(object sender, RoutedEventArgs e)
        {
            if(TableReceiver.SelectedItem != null)
            {
                TableHeader dataGrid = TableReceiver.SelectedItem as TableHeader;
                if (dataGrid.Adress != null)
                {
                    string address = dataGrid.Adress;
                    Ar6000Manager.DeleteMemoryChannel(address); //.Substring(2, 2)
                    Ar6000Manager.AOR.memthree.Clear();
                    Ar6000.MemoryChannelDataRead(Bank.ToString().PadLeft(2, '0'));
                }
            }            
        }

        private void TableReceiver_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed) 
            {
                //TableHeader dataGrid = TableReceiver.SelectedCells as TableHeader;
               // string address = dataGrid.Adress;
            }
        }

        public IEnumerable<DataGridRow> GetDataGridRows(DataGrid grid)
        {
            var itemsSource = grid.ItemsSource as IEnumerable;
            if (null == itemsSource) yield return null;
            foreach (var item in itemsSource)
            {
                if (grid.ItemContainerGenerator.ContainerFromItem(item) is DataGridRow row) yield return row;
            }
        }
    }
}
