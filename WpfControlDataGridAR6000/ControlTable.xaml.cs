﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Win32;
using System.Reflection;
using AR6000;

namespace WpfControlDataGridAR6000
{
    /// <summary>
    /// Логика взаимодействия для UserControl1.xaml
    /// </summary>
    public partial class ControlTable : UserControl
    {
        private Ar6000Manager Ar6000;
        static ObservableCollection<TableHeader> GridCollection { get; set; }
        

        public ControlTable()
        {
            InitializeComponent();
            DataContext = this;

            Ar6000 = new Ar6000Manager();
            GridCollection = new ObservableCollection<TableHeader>();

            for (int i = 0; i < 20; i++) { GridCollection.Add(new TableHeader(null, null, null, null, null, null)); }
            TableReceiver.ItemsSource = GridCollection;

            InterviewReceiver();

            SaveChanges.Visibility = Visibility.Collapsed;
            //UpdateScanData();

            ConnectAudio();
        }

        private void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void InterviewReceiver() 
        {
            Ar6000Manager.OnDecodedMemoryChannelDataRead += AR6000_OnDecodedMemoryChannelDataRead;
        }

        private void TableArone_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {        
            if(TableReceiver.SelectedItem != null)
            {
                TableHeader path = TableReceiver.SelectedItem as TableHeader;

                string freq = path.Frequency;
                double.TryParse(freq, out double frqq);
                if ((frqq > 0.09) && (frqq < 6000))
                {
                    Ar6000.FrequencySet(frqq);
                }
                Thread.Sleep(20);
                string mod = path.Mode;
                switch (mod)
                {
                    case "FM": Ar6000.ModeSet(0); break;
                    case "AM": Ar6000.ModeSet(2); break;
                    case "USB": Ar6000.ModeSet(4); break;
                    case "LSB": Ar6000.ModeSet(5); break;
                    case "CW": Ar6000.ModeSet(6); break;
                    case "ISB": Ar6000.ModeSet(7); break;
                    case "WFM": Ar6000.ModeSet(21); break;
                    case "FMST": Ar6000.ModeSet(23); break;
                    case "NFM": Ar6000.ModeSet(24); break;
                    case "SFM": Ar6000.ModeSet(25); break;
                    case "WAM": Ar6000.ModeSet(26); break;
                    case "NAM": Ar6000.ModeSet(28); break;
                }
                Thread.Sleep(5);
            }
        }

        private void TableArone_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
        {

        }

        private void SaveChanges_Click(object sender, RoutedEventArgs e)
        {

        }

        


    }
}
